import rule_engine
import json
from jsonpath_ng import jsonpath, parse

# f = {"up":{"down":{
#   'first_name': 'Luke',
#   'last_name': 'Skywalker',
#   'email': 'luke@rebels.org'
# }}}
with open('/home/xpms/data.json') as f:
  inp_data = json.load(f)
rule = rule_engine.Rule(
    'name == "patient_first_name" and value == "Linda"'
)

jsonpath_expression = parse('$.domain.[0].children[*].children[*].children[*]')
match = jsonpath_expression.find(inp_data)
for m in match:
    if rule.matches(m.value):
        print(m.value)
# print(match[0].value)

