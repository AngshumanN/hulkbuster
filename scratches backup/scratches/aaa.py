[
  {
    "Woiqtx_qRf-fH4zi3gbV2g": [
      {
        "sec_errors": [],
        "section_id": "47dddc05-c559-4ad9-acd0-70e7d7edb4ed",
        "extracted_entities": [],
        "failed_elements": {}
      }
    ],
    "Js4mtXE3Tuqw_wQLThyv-g": [
      {
        "sec_errors": [],
        "section_id": "bdef241e-3325-475f-8855-afec63321fe8",
        "extracted_entities": [],
        "failed_elements": {}
      }
    ],
    "AOUGQzqDRMqOerJ1znvnLA": [
      {
        "sec_errors": [],
        "section_id": "5e650304-f36f-45c4-9b60-b69f2e0d1528",
        "extracted_entities": [],
        "failed_elements": {}
      }
    ],
    "yoAfETDhRJuBfSHqq7xAOA": [
      {
        "sec_errors": [],
        "section_id": "63357fed-550f-4e12-ab0a-89d873fcb64a",
        "extracted_entities": [],
        "failed_elements": {}
      }
    ],
    "E5G8BDD9Sbm6JJNN7VWs3w": [
      {
        "sec_errors": [],
        "section_id": "44fa1f0c-db00-4607-8bd8-cabcc9a6f636",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "case_information.entity"
                ],
                "entity_value": [
                  "personcompletingthisform",
                  "8f9254ff-8434-4d5b-81eb-56ef37a18421",
                  0.1888,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.59
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym",
                "multi_source": False
              }
            ],
            "heading"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "TAj61pwoSVKM2dyBK-HuXg": [
      {
        "sec_errors": [],
        "section_id": "b4799ecb-793e-4f7f-9d52-cac791657dbe",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "case_information.source_case_form_sender_tel"
                ],
                "entity_value": [
                  "7831587383",
                  "9de1ee23-65a7-4ed3-86d7-6fc37502f18d",
                  0.10880000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "EN0aeNLQTSCG-CCzao31jA": [
      {
        "sec_errors": [],
        "section_id": "96d031be-b7d0-48da-bee4-07a15c8e987a",
        "extracted_entities": [],
        "failed_elements": {
          "96d031be-b7d0-48da-bee4-07a15c8e987a": {
            "0d9ddc81-db48-406b-b8cd-8846fd68c2f6": [
              "PSP NameNumber or MR Project NameNumber"
            ]
          }
        }
      }
    ],
    "cyLCORGQTima16PKgK7uSA": [
      {
        "sec_errors": [],
        "section_id": "6a03c9ff-9b8b-4464-a372-268bd5a5c6a5",
        "extracted_entities": [],
        "failed_elements": {}
      }
    ],
    "XBIfNgixQKqvqB5WToofcg": [
      {
        "sec_errors": [],
        "section_id": "ac50f5b5-3f8b-40e7-8e88-f5c89f5c42f6",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "case_information.first_receive_datetime"
                ],
                "entity_value": [
                  "25-Aug-2017",
                  "59f3fd43-4fe2-4554-84ca-3ec231700b2c",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "1Sbmlee7SL-lLaFAium1hg": [
      {
        "sec_errors": [],
        "section_id": "561d32ee-17f5-4a5a-a34a-72cc2026c92a",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "patient_information.entity"
                ],
                "entity_value": [
                  "patientinformationcompleteanyknowninformation",
                  "088308e5-bfd7-4157-8d21-e9dae0bc871a",
                  0.1888,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.59
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym",
                "multi_source": False
              }
            ],
            "heading"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "l8quKfCNTRSZdbyB6PmlmA": [
      {
        "sec_errors": [],
        "section_id": "f91abec0-2e24-4fa9-bdca-5fc3625bd24c",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_last_name"
                ],
                "entity_value": [
                  "Freddie",
                  "5ab1c13b-6972-4484-a7b0-c0ae292701f9",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "COszzLXyRoWz27dAHoNAlA": [
      {
        "sec_errors": [],
        "section_id": "5de6e72d-cfda-4c23-8c51-d8bf920ca75d",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_first_name"
                ],
                "entity_value": [
                  "Craig",
                  "ab104caf-6637-49d0-8d6b-3ca23f3d3acf",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_sex"
                ],
                "entity_value": [
                  "Male",
                  "47973eb3-0268-4aca-aff7-ac033f5f861e",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_birth_date"
                ],
                "entity_value": [
                  "14-Oct-1960",
                  "04f4e0ce-6ea5-466c-938a-2a75bdf0bdc6",
                  0.055999999999999994,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "QWcnfu77QDCkGVHNycvkvQ": [
      {
        "sec_errors": [],
        "section_id": "8fca67ae-220b-4e7c-aca9-c3a5c16facf1",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_investigation_number"
                ],
                "entity_value": [
                  "P NT-00017133",
                  "20af622f-2b77-43e3-b7c5-a168154f780c",
                  0.10880000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_onset_age_text"
                ],
                "entity_value": [
                  "56 Years 9 Months",
                  "eaeeb008-596e-4517-b390-191ab63411f4",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "ctXDyYbmQuyTWwsBhccjBA": [
      {
        "sec_errors": [],
        "section_id": "f6e406c1-910c-42bd-996a-e6ace947e460",
        "extracted_entities": [],
        "failed_elements": {}
      }
    ],
    "L7zlL-HwSryUjiH2HFF7KQ": [
      {
        "sec_errors": [],
        "section_id": "cca66ad6-7b7c-408e-b90a-61853786c7a2",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "drug_information.medicinal_product"
                ],
                "entity_value": [
                  "Avaxim",
                  "1fbcdf31-a983-4f61-82ef-66a6497ac4d8",
                  0.10880000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "drug_information.drug_interval_dosage_text"
                ],
                "entity_value": [
                  "10 minutes",
                  "15b1b05f-57d2-45f5-82c7-cff24b3544c2",
                  0.08640000000000002,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.27
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "DUokkJsOR8KT_KIOOvJsFA": [
      {
        "sec_errors": [],
        "section_id": "1e7c5f8d-3712-4fe2-871a-ca0bbd4a0ed1",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "drug_information.drug_dosage_text"
                ],
                "entity_value": [
                  "1750.00",
                  "3b3e1056-8f4a-4868-905c-0d48eba692ab",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "drug_information.drug_administration_route_text"
                ],
                "entity_value": [
                  "{ Nail use",
                  "0104cb6f-e676-4c6d-8d96-40842b153312",
                  0.0928,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.29
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "drug_information.drug_end_date"
                ],
                "entity_value": [
                  "19-) an-2020",
                  "ee94643b-3017-4666-a779-d6e96e8b8476",
                  0.0672,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.96,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.96,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "dd0_GBo5Rcq4RAIyAL0GRw": [
      {
        "sec_errors": [],
        "section_id": "f20c52b4-dbfb-47e3-8fa3-b00a756211cb",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "drug_information.drug_start_date"
                ],
                "entity_value": [
                  "09-Apr-2013",
                  "34fc09eb-5b4d-4714-af33-76a23ec9aae0",
                  0.1344,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.96,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.96,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "drug_information.drug_indication_verbatim"
                ],
                "entity_value": [
                  "Type I",
                  "f6880d4b-d711-4198-9e40-2020f279307b",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "-R0dTv8_T-y7EJAJfnHmug": [
      {
        "sec_errors": [],
        "section_id": "af22b544-b251-4275-81dc-cc0dbdd9acd9",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "drug_information.ongoing"
                ],
                "entity_value": [
                  "Yes",
                  "01525c00-87df-467d-bfb5-a277b38dd94e",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "30hZwebxSyOz18nkziLmoA": [
      {
        "sec_errors": [],
        "section_id": "0a929a47-fc05-4b5f-b4a3-1adb3eb324e6",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "drug_information.entity"
                ],
                "entity_value": [
                  "adverseeventinformationcompleteanyknowninformation",
                  "4f075623-f530-4f2d-bb8c-8a5518547a28",
                  0.20352000000000003,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.96,
                    "element_confidence",
                    0.53
                  ]
                ],
                "entity_score": 0.96,
                "entity_source": "synonym",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "event_information.entity"
                ],
                "entity_value": [
                  "adverseeventinformationcompleteanyknowninformation",
                  "4f075623-f530-4f2d-bb8c-8a5518547a28",
                  0.20352000000000003,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.96,
                    "element_confidence",
                    0.53
                  ]
                ],
                "entity_score": 0.96,
                "entity_source": "synonym",
                "multi_source": True
              }
            ],
            "heading"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "g77-XFooT5GcSFDeMQyhjw": [
      {
        "sec_errors": [],
        "section_id": "ec6e8879-6d0d-4c37-ad06-a92ed2c4f8b5",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "event_information.event_start_date"
                ],
                "entity_value": [
                  "26-Oct-2017",
                  "f998afbc-c557-42b9-9041-2222f44fba0b",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "Describe",
                  "9f3927f4-e025-425d-8a5e-5ce574a019a4",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "event corrective treatment patients",
                  "9f3927f4-e025-425d-8a5e-5ce574a019a4",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "concomitant treatment",
                  "9f3927f4-e025-425d-8a5e-5ce574a019a4",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "the adverse events",
                  "9f3927f4-e025-425d-8a5e-5ce574a019a4",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "Batch Number",
                  "9f3927f4-e025-425d-8a5e-5ce574a019a4",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "the Suspect Product",
                  "9f3927f4-e025-425d-8a5e-5ce574a019a4",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "Batch Number",
                  "9f3927f4-e025-425d-8a5e-5ce574a019a4",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              }
            ],
            "sentence"
          ],
          [
            [
              {
                "entity_candidates": [
                  "event_information.event_outcome"
                ],
                "entity_value": [
                  "Unknown",
                  "1071a32d-f3e1-4135-a248-f2fecdaf46b5",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "IpvMI5mXTcafEsLeuAaxoA": [
      {
        "sec_errors": [],
        "section_id": "98361039-ccb5-4510-afe5-9cd0bb27c6b0",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "PERSON"
                ],
                "entity_value": [
                  "Renvela",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "named_entity",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "named_entity",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "Renvela",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "our patient assistance program",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "unknown buthe",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "our program",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "DATE"
                ],
                "entity_value": [
                  "April 2016",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "named_entity",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "named_entity",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "GPE"
                ],
                "entity_value": [
                  "Patientt",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "named_entity",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "named_entity",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "Patientt",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "hospital",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              },
              {
                "entity_candidates": [
                  "NP"
                ],
                "entity_value": [
                  "deceasedC ause",
                  "ea95017e-fa9e-41a8-8cde-992f2bf45bb7",
                  0.16000000000000003,
                  [
                    "heading",
                    "0.4",
                    "NP",
                    0.5,
                    "element_confidence",
                    0.8
                  ]
                ],
                "entity_score": 0.5,
                "entity_source": "NP",
                "multi_source": True
              }
            ],
            "sentence"
          ],
          [
            [
              {
                "entity_candidates": [
                  "drug_information.drug_assessment_result"
                ],
                "entity_value": [
                  "No",
                  "9d138a9d-8346-4f31-bede-a87f5f37b25f",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "2CKm49RdTlSWlbEAeoK4Lg": [
      {
        "sec_errors": [],
        "section_id": "d25bd971-9c49-4550-b9eb-e66805fd22d1",
        "extracted_entities": [],
        "failed_elements": {}
      }
    ],
    "zd4qXJlOQ4CG5qMeYSWhZg": [
      {
        "sec_errors": [],
        "section_id": "c7df4e8d-13b8-4c07-b935-961f97ebcd42",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "event_information.entity"
                ],
                "entity_value": [
                  "",
                  "5f0bcae1-03f3-48c9-8600-f6fafb7f4bf6",
                  0.10560000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.33
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_death_date"
                ],
                "entity_value": [
                  "01-03-2020",
                  "3c72ab8e-c86c-45ba-b18f-398c13410460",
                  0.11199999999999999,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "patient_information.patient_death_report_text"
                ],
                "entity_value": [
                  "headache",
                  "037d768b-2a1d-448b-af43-122bec717099",
                  0.054400000000000004,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "1ChvLEi9RfCBZ7KoJf8-XQ": [
      {
        "sec_errors": [],
        "section_id": "29fef5c5-a176-456f-94b3-b3e3b9d64163",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "treating_physician_information.entity"
                ],
                "entity_value": [
                  "treatingphysicianinformation",
                  "61831e7a-738a-4743-a376-9ee721462c18",
                  0.1888,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.59
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym",
                "multi_source": False
              }
            ],
            "heading"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "Y8M5UnC4T-ulTaVIDL119g": [
      {
        "sec_errors": [],
        "section_id": "7890791d-6808-4db9-97e6-ae8cb3167232",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "treating_physician_information.entity"
                ],
                "entity_value": [
                  "",
                  "0f519b48-772f-43b6-b3f6-c185255ea5c4",
                  0.10880000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "reporter_information.reporter_address"
                ],
                "entity_value": [
                  "70 Wakehurst Drive Staten Island, NY 10306",
                  "01d847f2-124b-4939-844d-1f4b29141ff9",
                  0.1064,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "treating_physician_information.reporter_address"
                ],
                "entity_value": [
                  "70 Wakehurst Drive Staten Island, NY 10306",
                  "01d847f2-124b-4939-844d-1f4b29141ff9",
                  0.1064,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "H1ADCWXXSYSfgcszzhWjiw": [
      {
        "sec_errors": [],
        "section_id": "2fc56bfe-6582-46a0-a383-11b86c4d6573",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "reporter_information.reporter_full_name"
                ],
                "entity_value": [
                  "Craig Harold",
                  "3aa74b42-de07-480b-a55b-7d207c2fb493",
                  0.08400000000000002,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.6000000000000001,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.6000000000000001,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "case_information.source_case_form_sender_name"
                ],
                "entity_value": [
                  "Craig Harold",
                  "3aa74b42-de07-480b-a55b-7d207c2fb493",
                  0.08400000000000002,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.6000000000000001,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.6000000000000001,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "treating_physician_information.reporter_full_name"
                ],
                "entity_value": [
                  "Craig Harold",
                  "3aa74b42-de07-480b-a55b-7d207c2fb493",
                  0.08400000000000002,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.6000000000000001,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.6000000000000001,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "treating_physician_information.reporter_telephone"
                ],
                "entity_value": [
                  "7867827300",
                  "53292fab-dead-40a8-9cd4-6eb4acef286a",
                  0.10336000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "reporter_information.reporter_telephone"
                ],
                "entity_value": [
                  "7867827300",
                  "53292fab-dead-40a8-9cd4-6eb4acef286a",
                  0.10336000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "reporter_information.reporter_email"
                ],
                "entity_value": [
                  "Craig.Harold@ email.com",
                  "2f16c5a0-eb8c-4c0a-9aa1-ac6f833df43d",
                  0.0532,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "treating_physician_information.reporter_email"
                ],
                "entity_value": [
                  "Craig.Harold@ email.com",
                  "2f16c5a0-eb8c-4c0a-9aa1-ac6f833df43d",
                  0.0532,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "dF42sv1XQvmQu_5i7kB4RQ": [
      {
        "sec_errors": [],
        "section_id": "b41d18cf-2c1f-441c-aa2b-bd7c3cb7b87a",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "reporter_information.entity"
                ],
                "entity_value": [
                  "reporterinformationwhotoldyouaboutthisadverseevent",
                  "3e341aea-633c-4212-9494-09c82aea4191",
                  0.1856,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.58
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym",
                "multi_source": False
              }
            ],
            "heading"
          ]
        ],
        "failed_elements": {}
      }
    ],
    "UFqnuSypQQib6F6tWl-WNg": [
      {
        "sec_errors": [],
        "section_id": "7ac11302-4b35-4487-a331-62f693a38908",
        "extracted_entities": [],
        "failed_elements": {
          "7ac11302-4b35-4487-a331-62f693a38908": {
            "43c5d8d8-d898-4dc0-a185-b78cc59941b0": [
              "Address"
            ]
          }
        }
      }
    ],
    "QL1eudOIRaiI1fC9P6jpsg": [
      {
        "sec_errors": [],
        "section_id": "1f9c6edc-31d4-4e2e-b44f-351f8319dba2",
        "extracted_entities": [
          [
            [
              {
                "entity_candidates": [
                  "reporter_information.reporter_full_name"
                ],
                "entity_value": [
                  "Harold Craig",
                  "7f70ff81-9870-40f2-87a1-723fd92437a9",
                  0.08400000000000002,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.6000000000000001,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.6000000000000001,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "case_information.source_case_form_sender_name"
                ],
                "entity_value": [
                  "Harold Craig",
                  "7f70ff81-9870-40f2-87a1-723fd92437a9",
                  0.08400000000000002,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.6000000000000001,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.6000000000000001,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "treating_physician_information.reporter_full_name"
                ],
                "entity_value": [
                  "Harold Craig",
                  "7f70ff81-9870-40f2-87a1-723fd92437a9",
                  0.08400000000000002,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.6000000000000001,
                    "element_confidence",
                    0.35
                  ]
                ],
                "entity_score": 0.6000000000000001,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "reporter_information.reporter_hcp"
                ],
                "entity_value": [
                  "Yes",
                  "4cdcfe32-f63d-42e7-a6a1-0601767c3484",
                  0.10560000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.8,
                    "element_confidence",
                    0.33
                  ]
                ],
                "entity_score": 0.8,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "treating_physician_information.reporter_telephone"
                ],
                "entity_value": [
                  "7831587383",
                  "d0c35c57-4b94-40e2-a7d6-398d308c01fe",
                  0.10336000000000001,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "reporter_information.reporter_telephone"
                ],
                "entity_value": [
                  "7831587383",
                  "d0c35c57-4b94-40e2-a7d6-398d308c01fe",
                  0.051680000000000004,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              }
            ],
            "field"
          ],
          [
            [
              {
                "entity_candidates": [
                  "reporter_information.reporter_email"
                ],
                "entity_value": [
                  "Craig.F reddie@ email.com",
                  "67554c0e-b34b-4a8b-a25f-872896c10e88",
                  0.06890666666666667,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              },
              {
                "entity_candidates": [
                  "treating_physician_information.reporter_email"
                ],
                "entity_value": [
                  "Craig.F reddie@ email.com",
                  "67554c0e-b34b-4a8b-a25f-872896c10e88",
                  0.06890666666666667,
                  [
                    "heading",
                    "0.4",
                    "synonym",
                    0.76,
                    "element_confidence",
                    0.34
                  ]
                ],
                "entity_score": 0.76,
                "entity_source": "synonym"
              }
            ],
            "field"
          ]
        ],
        "failed_elements": {}
      }
    ]
  }
]
