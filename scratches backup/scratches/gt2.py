def canSum(n, l, memo={}):
    if n == 0:
        return True
    elif n < 0:
        return False

    elif n in memo:
        return memo[n]

    else:
        for i in l:
            target = n - i
            memo[target] = canSum(target, l, memo)
            if memo[target]:
                return memo[target]

    return False


if __name__ == "__main__":
    # print(canSum(300, [7, 14]))
    # print(canSum(7, [5,3,4,7]))
    # print(canSum(7, [2, 4]))
    print(canSum(8, [2,3,5]))
