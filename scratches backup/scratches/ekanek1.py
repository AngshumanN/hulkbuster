class Student:
    name = ""
    age = 0
    marks = 0
    rollNumber = 0

    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.age = kwargs.get('age')
        self.marks = kwargs.get('marks')
        self.rollNumber = kwargs.get('rollNumber')


# print([(i.name, i.age, i.marks, i.rollNumber) for i in l])
# # newlist = sorted(l, key=lambda x: x.age, reverse=False)
# l.sort(key=lambda x: x.name, reverse=False)
# l.sort(key=lambda x: x.age, reverse=False)
# l.sort(key=lambda x: x.marks, reverse=False)
# l.sort(key=lambda x: x.rollNumber, reverse=False)
# print([(i.name, i.age, i.marks, i.rollNumber) for i in l])
#

# print([i.age for i in newlist])

def sort_student_object(students: list, criteria: list, asc=False):
    for item in criteria:
        students.sort(key=lambda x: getattr(x,item), reverse=asc)
    return students




if __name__ == "__main__":
    s = Student(name="abec", age=23, marks=44, rollNumber=34)
    s1 = Student(name="nmjk", age=25, marks=44, rollNumber=34)
    s2 = Student(name="lpou", age=24, marks=44, rollNumber=34)
    s3 = Student(name="werh", age=26, marks=44, rollNumber=34)
    s4 = Student(name="vvvv", age=21, marks=44, rollNumber=34)

    c = ['name','age']

    l = [s, s1, s2, s3, s4]
    print(sort_student_object(l,c))
