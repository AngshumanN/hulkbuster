class Graph:
    vertices = {}
    num_vertex = None
    vertices_list = None
    adjMatrix = []

    def __init__(self, num_vertex):
        self.num_vertex = num_vertex
        self.vertices_list = [None] * self.num_vertex
        self.adjMatrix = [[-1] * num_vertex for x in range(num_vertex)]

    def add_vertex(self, vtx, id):
        if 0 <= vtx <= self.num_vertex:
            self.vertices[id] = vtx
            self.vertices_list[vtx] = id

    def add_edge(self, from_v, to_v, cost=0):
        if from_v in self.vertices_list and to_v in self.vertices_list:
            v_f = self.vertices[from_v]
            t_f = self.vertices[to_v]

            self.adjMatrix[v_f][t_f] = cost
            # self.adjMatrix[t_f][v_f] = cost

    def get_vertex(self):
        return self.vertices_list

    def get_edges(self):
        edges = []
        min = 1000
        min_pair = []
        max = -1
        max_pair = []
        for i in range(self.num_vertex):
            for j in range(self.num_vertex):
                if self.adjMatrix[i][j] != -1:
                    if min > self.adjMatrix[i][j]:
                        min = self.adjMatrix[i][j]
                        min_pair = [self.vertices_list[i], self.vertices_list[j]]
                    if max < self.adjMatrix[i][j]:
                        max = self.adjMatrix[i][j]
                        max_pair = [self.vertices_list[i], self.vertices_list[j]]
                    edges.append((self.vertices_list[i], self.vertices_list[j], self.adjMatrix[i][j]))
        return edges, min_pair, max_pair

    def get_matrix(self):
        return self.adjMatrix


if __name__ == "__main__":
    G = Graph(6)
    G.add_vertex(0, 'a')
    G.add_vertex(1, 'b')
    G.add_vertex(2, 'c')
    G.add_vertex(3, 'd')
    G.add_vertex(4, 'e')
    G.add_vertex(5, 'f')
    G.add_edge('a', 'e', 10)
    G.add_edge('a', 'c', 20)
    G.add_edge('c', 'b', 30)
    G.add_edge('b', 'e', 40)
    G.add_edge('e', 'd', 50)
    G.add_edge('f', 'e', 60)
    G.add_edge('e', 'f', 1000)
    print("Vertices of Graph")
    print(G.get_vertex())
    print("Edges of Graph")
    ed, min_p, max_p = G.get_edges()
    print(ed)
    print("Adjacency Matrix of Graph")
    print(G.get_matrix())

    print("least costly edge:")
    print(min_p)

    print("most costly edge:")
    print(max_p)
