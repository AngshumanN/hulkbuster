import json
q = {
    'configuration': {
        'transactional_commit': False
    },
    'TDivIiIsSbertQjJgLuO1g': [
        {
            'section_id': 'fcab1427-86d9-4bdb-84ab-34b546109232',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'CYkjzBbAQyabG0fg0L7nVg': [
        {
            'section_id': '40d0445b-d011-4554-830b-9ed86ae9ad91',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    '1R3gNxmVTMeEpPQC6g_vcw': [
        {
            'section_id': 'd912afd0-c28e-4648-9e04-7f088d8c01ca',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    '1ZEl7YuESXCAfMPsB1PtNg': [
        {
            'section_id': '67a0b8a3-86ce-46b6-aa82-1579f237f6af',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'hoALqv1gQwmMkyotKb-JSA': [
        {
            'section_id': '02ef2de8-a1a2-4e10-b265-34c3b4de8c80',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'En9R8ySLSIKQTH0J_28eAg': [
        {
            'section_id': 'c17dbb2c-2c7e-4b39-a150-3964ae10cc1c',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'case_information.source_case_form_sender_tel'
                            ],
                            'entity_value': [
                                '7831587383',
                                '6435cd0b-aede-4088-9cd3-7cb9645621e9',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'NvuGOFDhSc28qXKnOmTDng': [
        {
            'section_id': '109b3331-e1bf-4d4d-97e5-9b6c651d10b4',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'case_information.source_case_form_sender_name'
                            ],
                            'entity_value': [
                                'Harold Craig, Patient Education Liaison',
                                'cf88c196-ecf7-41af-9d80-53db4bc50500',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'case_information.source_case_form_sender_organization'
                            ],
                            'entity_value': [
                                'MCKESSON',
                                'b5ca26d2-1757-408c-a828-321b8c213690',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'case_information.sponsor_study_number'
                            ],
                            'entity_value': [
                                'The Oak Clinic',
                                '2ad5ffd2-82d7-4f23-bf68-3c12aea99320',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'S8bRWtLbT72pe4PNsnawQQ': [
        {
            'section_id': 'bc99d33b-d6f9-44a6-ae81-077d69c7c4c3',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'oRGn8pgrRrGH--R0_T1lXw': [
        {
            'section_id': 'c0d87b70-5084-416c-abb6-6624a5387c00',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'case_information.first_receive_datetime'
                            ],
                            'entity_value': [
                                '25-Aug-2017',
                                '35061fb1-8f06-4485-abc3-057ed9a4b24f',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'J7FAVMK9QNWqW-nD1XPxGQ': [
        {
            'section_id': '175d843d-29a8-40a3-bb25-19b0d5576f98',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'msjsy138RkCSxTwXqujDgw': [
        {
            'section_id': '02c26a10-a5ef-4225-9b67-56cfd22baabc',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_last_name'
                            ],
                            'entity_value': [
                                'Freddie',
                                '901330fe-f57d-4266-bb6a-883cb12074d5',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'lTyEHs2eTNioc4ZhkZG9QA': [
        {
            'section_id': '5e886d4d-dc3b-4cac-8041-aa1e5aec3070',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_first_name'
                            ],
                            'entity_value': [
                                'Craig',
                                '002d0c49-ce9b-4e2a-8380-bd568b401aad',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_sex'
                            ],
                            'entity_value': [
                                'Male',
                                'b5fc0389-8b1b-4bf1-8c2f-da1567c01711',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_birth_date'
                            ],
                            'entity_value': [
                                '14-Oct-1960',
                                'b418049f-6944-4c99-8dfb-bea905e55a09',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'Kz4trdzmSwu8YXrgjSQ_7w': [
        {
            'section_id': '355f8b59-7c71-4f18-b8ae-7829ead49888',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_investigation_number'
                            ],
                            'entity_value': [
                                'P NT-00017133',
                                '522f95ca-1ed1-485b-87cb-c5f2ba71cc3b',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_onset_age_text'
                            ],
                            'entity_value': [
                                '56 Years 9 Months',
                                '69172d2f-5876-4138-bc30-af43cd97d242',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    '8SiHySgUQpShXGV5uGmiRA': [
        {
            'section_id': '8f91811f-2c67-43bb-92d4-0d0d96d5c0c2',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'KM4GJf9yS4KfVCjJye1WmQ': [
        {
            'section_id': '64d3c6fa-27a2-4bc3-b766-55d8ccb7d53e',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'bQk2Ql-cTp6VQ3Ldqus95g': [
        {
            'section_id': '83d6ad06-5cd7-4c01-a996-4019bc44af77',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.medicinal_product'
                            ],
                            'entity_value': [
                                'Avaxim',
                                '8256427f-ed18-45c8-ae86-141038c463b0',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.drug_interval_dosage_text'
                            ],
                            'entity_value': [
                                '10 minutes',
                                '1d4e95ea-e617-49d4-bc9b-994634a3d5ac',
                                0.10368,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.27
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'vsRDKBJARRuMM4qKXOrC_w': [
        {
            'section_id': '9dcc560a-2716-4775-aa2f-7720de264f2a',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.drug_dosage_text'
                            ],
                            'entity_value': [
                                '1750.00',
                                'b60d8241-d361-4cf9-98c6-dece808ba3b5',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.drug_administration_route_text'
                            ],
                            'entity_value': [
                                '{ Nail use',
                                '1f784d20-c54c-4d12-b399-d30099ec452c',
                                0.11136,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.29
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.drug_end_date'
                            ],
                            'entity_value': [
                                '19-) an-2020',
                                '28e27cd3-a420-4caf-bab6-21a23244c5aa',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    '7d6JzJniQJy5SEnGhVAgXg': [
        {
            'section_id': '955a9ba6-68ab-4ffb-b4a2-b291be6109ed',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.drug_start_date'
                            ],
                            'entity_value': [
                                '09-Apr-2013',
                                '9c46920b-80cc-4e9f-9a73-024b3ed13630',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.drug_indication_verbatim'
                            ],
                            'entity_value': [
                                'Type I',
                                '6fba07ce-cd2f-4807-89a3-b5541984e3cc',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'C1uZCr5ZQrmQXvAFFQniNg': [
        {
            'section_id': 'b063cc60-4395-4ed5-9c6c-4874d6d4a3b9',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.ongoing'
                            ],
                            'entity_value': [
                                'Yes',
                                '38ffd257-48c1-4dcd-b3a4-48ef1a454de4',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    '-onCUT7lRS-43Vb5vhpB6w': [
        {
            'section_id': 'c8696202-aa3e-43ed-bc9b-0f0d4ded4dbc',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'f4YbKCSHQhC4c6ybvzJxhw': [
        {
            'section_id': '8cbfa7ee-f1c6-45c6-a68d-7a83ad96a0af',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'event_information.event_start_date'
                            ],
                            'entity_value': [
                                '26-Oct-2017',
                                'dee65459-d89b-4ed2-9d94-0675cea30189',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'event_information.event_outcome'
                            ],
                            'entity_value': [
                                'Unknown',
                                '56acd48b-d629-4a49-beb8-82fdea389aa5',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'IdCm6YwPR52JYTsk6DuRGA': [
        {
            'section_id': 'da37a3d4-3414-4467-b291-2c804555d25b',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'drug_information.drug_assessment_result'
                            ],
                            'entity_value': [
                                'No',
                                '3c40a3dd-a29e-424b-8084-550aecf82385',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'ccAmACV2Qwmivf1ATYEIWQ': [
        {
            'section_id': 'cdccf547-c549-439d-9b00-fe43532c774b',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'vEwS-1LSSrmG6pgNpfR8rg': [
        {
            'section_id': '8ba7a8eb-5193-45f5-bc21-c2830564360b',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'event_information.entity'
                            ],
                            'entity_value': [
                                '',
                                '77afd168-fe28-451c-b75c-e6ff64fce824',
                                0.12672000000000003,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.33
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_death_date'
                            ],
                            'entity_value': [
                                '01-03-2020',
                                '732ef856-8a09-4ba3-9b33-068c339a3c9e',
                                0.11199999999999999,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'patient_information.patient_death_report_text'
                            ],
                            'entity_value': [
                                'headache',
                                'c48b1a83-e83b-4bf6-9090-624aeaefd531',
                                0.10880000000000001,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    '8m6zXLpcSa2oYkX67bIBLQ': [
        {
            'section_id': 'baa38b33-318c-4d7f-8860-7e0c06846560',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'fIiDWeZ1R0uhKReYtlGL5Q': [
        {
            'section_id': 'fd939acf-5d81-41d1-8e55-7bc736ec3847',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'treating_physician_information.entity'
                            ],
                            'entity_value': [
                                '',
                                '229cd1b8-860b-430d-8dc7-a05e7fbaa6cf',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'reporter_information.reporter_address'
                            ],
                            'entity_value': [
                                '70 Wakehurst Drive Staten Island, NY 10306',
                                '4483fa17-dd90-4a13-958e-3b383c47a8b9',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'Eu5yOQZJR-yo4D3njq386A': [
        {
            'section_id': '2249aaa1-7598-41ea-857f-be93c492a36e',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'reporter_information.reporter_full_name'
                            ],
                            'entity_value': [
                                'Craig Harold',
                                'ec9132b2-45ff-4ed7-b368-0caf456c2f41',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'treating_physician_information.reporter_telephone'
                            ],
                            'entity_value': [
                                '7867827300',
                                '25bfac71-f2ad-4e15-bf8c-76cf7576dc09',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'treating_physician_information.reporter_email'
                            ],
                            'entity_value': [
                                'Craig.Harold@ email.com',
                                '69cf4152-22c9-427f-9a05-b57587bb8678',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'FRPOrG_1SY6u39Glvsg9qg': [
        {
            'section_id': 'aae8f978-e202-4b9c-b85c-942518f68487',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'iPpxlvphSByAIaL_F5kRfA': [
        {
            'section_id': '3f64ce08-8891-48fa-8c41-3625ca9c9b39',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'reporter_information.entity'
                            ],
                            'entity_value': [
                                ')',
                                '4ed11dee-a98c-4e2b-8577-5bc7ac4dd5d8',
                                0.10880000000000001,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'reporter_information.entity'
                            ],
                            'entity_value': [
                                '',
                                '8868ae7e-98c9-4ad3-aa34-d70dd166dbfb',
                                0.10880000000000001,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'reporter_information.reporter_address'
                            ],
                            'entity_value': [
                                '820 Gartner Road Brooklyn, NY 11224',
                                'b00d15c5-2147-4dcc-aa08-bf8e27bf200b',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'nm9kIKEpQjmYAe_Jq3JtSg': [
        {
            'section_id': '150fcead-9479-4753-9e9b-4a6b2948a9c2',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'reporter_information.reporter_full_name'
                            ],
                            'entity_value': [
                                'Harold Craig',
                                'c3d15064-209a-4a53-8dc5-c086e7db44b1',
                                0.1344,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.35
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'QqJsD4plR_2AzGSgq4_fzg': [
        {
            'section_id': '6b41a9eb-e986-4f74-9ae7-e7e9fdde2ec2',
            'extracted_entities': [

            ],
            'failed_elements': {

            }
        }
    ],
    'QTPgResmRFiWGt7UHIbHmw': [
        {
            'section_id': '416ad878-019a-497a-b780-824d8af4552c',
            'extracted_entities': [
                [
                    [
                        {
                            'entity_candidates': [
                                'reporter_information.reporter_hcp'
                            ],
                            'entity_value': [
                                'Yes',
                                '32956fe1-4e0f-4b29-8ecb-520dc64d920b',
                                0.10560000000000001,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.8,
                                    'element_confidence',
                                    0.33
                                ]
                            ],
                            'entity_score': 0.8,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'treating_physician_information.reporter_telephone'
                            ],
                            'entity_value': [
                                '7831587383',
                                '37e3e5aa-32da-44f3-858d-6415435a8cba',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ],
                [
                    [
                        {
                            'entity_candidates': [
                                'treating_physician_information.reporter_email'
                            ],
                            'entity_value': [
                                'Craig.F reddie@ email.com',
                                '59a46ddd-3531-4538-8739-32e9109ff8b6',
                                0.13056,
                                [
                                    'heading',
                                    '0.4',
                                    'corpus',
                                    0.96,
                                    'element_confidence',
                                    0.34
                                ]
                            ],
                            'entity_score': 0.96,
                            'entity_source': 'corpus'
                        }
                    ],
                    'field'
                ]
            ],
            'failed_elements': {

            }
        }
    ],
    'data_bucket': {
        'keys': [
            'mq_state',
            'mq_context'
        ]
    }
}
sec_data = {}
for k,v in q.items():
    v = v[0] if isinstance(v,list) else v
    if "section_id" in v:
        sec_data.update({v["section_id"]:v})

print(sec_data)