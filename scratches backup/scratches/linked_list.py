class Node():

    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None


class LinkedList():
    start = None
    end = None

    def __init__(self, key):
        self.start = self.end = Node(key) if key else None

    def insert(self, key):
        if self.end:
            self.end.next = Node(key)
            self.end = self.end.next

    def insert_at_key(self, key_loc, key):
        node = self.get_node(key_loc)
        newnode = Node(key)
        newnode.next = node.next
        node.next = newnode

    def get_node(self, key):
        node = self.start
        while node.next is not None:
            if node.data == key:
                return node
            node = node.next

    def print_list(self, reverse=False, key=None):
        if not key:
            node = self.start
        else:
            node = self.get_node(key)
        while node:
            print(node.data)
            node = node.next


l = LinkedList(1)
l.insert(23)
l.insert(433)
l.insert_at_key(1,908)
l.print_list()
