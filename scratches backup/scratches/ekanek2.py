delhi_to_mumbai = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30]
mumbai_to_delhi = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75]

chart_map = []

for fare1 in delhi_to_mumbai:
    for fare2 in mumbai_to_delhi:
        chart_map.append({"fare": fare1 + fare2, "pair": [fare1, fare2]})
chart_map.sort(key=lambda x:x["fare"], reverse=False)
[print(a["pair"][0],",",a["pair"][1]) for a in chart_map[0:10]]