# # def fib(n, l: dict):
# #     if n <= 2:
# #         return 1
# #     elif n in l:
# #         return l[n]
# #     else:
# #         l[n] = (fib((n - 1), l) + fib((n - 2), l))
# #         return l[n]
#
#
# if __name__ == "__main__":
#     print(fib(7, {}))
#     print(fib(8, {}))
#     print(fib(9, {}))
#     print(fib(100, {}))


def fibo(n, l: dict):
    if n <= 2:
        return 1
    elif n in l:
        return l[n]
    else:
        l[n] = fibo((n - 1), l) + fibo((n - 2), l)
        return l[n]


if __name__ == "__main__":
    print(fibo(7,{}))
    print(fibo(8,{}))
    print(fibo(9,{}))
    print(fibo(100,{}))
