# Google Mock Interview question

Blocks = [
    {
        "gym": False,
        "school": True,
        "store": False,
    },
    {
        "gym": True,
        "school": False,
        "store": False,
    },
    {
        "gym": True,
        "school": True,
        "store": False,
    },
    {
        "gym": False,
        "school": True,
        "store": False,
    },
    {
        "gym": False,
        "school": True,
        "store": True,
    }
]

req = ["gym", "school", "store", "office"]


def find_distance(index, b_list, req):
    if index < 0:
        return None

    else:
        dist_dict = {}
        for r in req:
            dist_dict.update({r: 99999})
            if r in b_list[index]:
                if b_list[index][r]:
                    dist_dict.update({r: 0})
                else:
                    for l_block in b_list[0:index]:
                        if r in l_block:
                            if l_block[r] and (index - b_list.index(l_block)) >= 0:
                                dist_dict.update({r: min(dist_dict[r], (index - b_list.index(l_block)))})
                    for r_block in b_list[index + 1:len(b_list)]:
                        if r in r_block:
                            if r_block[r] and (b_list.index(r_block) - index) >= 0:
                                dist_dict.update({r: min(dist_dict[r], b_list.index(r_block) - index)})
    return dist_dict


def find_suitable_block():
    Blocks = [
        {
            "gym": False,
            "school": True,
            "store": False,
        },
        {
            "gym": True,
            "school": False,
            "store": False,
        },
        {
            "gym": True,
            "school": True,
            "store": False,
        },
        {
            "gym": False,
            "school": True,
            "store": False,
        },
        {
            "gym": False,
            "school": True,
            "store": True,
        }
    ]

    req = ["gym", "school", "store"]

    dist_vectors = []
    max_d = []
    for i, b in enumerate(Blocks):
        dist = find_distance(i, Blocks, req)
        dist_vectors.append(dist)
        max_d.append({"index": i, "max": max(list(dist.values()))})

    print(max_d)
    max_d.sort(key=lambda x: x['max'], reverse=False)

    print("index : ", max_d[0]['index'], ", block : ", Blocks[max_d[0]['index']])


if __name__ == "__main__":
    find_suitable_block()
