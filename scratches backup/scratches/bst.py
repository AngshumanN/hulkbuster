class Node():

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BST():
    root = None

    def __init__(self, root_key=None):
        self.root = Node(root_key) if root_key else None

    def insert(self, root, key):
        if root is None:
            return Node(key)
        else:
            if root.data == key:
                return root
            elif root.data < key:
                root.right = self.insert(root.right, key)
            else:
                root.left = self.insert(root.left, key)
        return root

    def inorder(self, root=None):
        if root:
            self.inorder(root.left)
            print(root.data)
            self.inorder(root.right)

    def store_inorder(self, root=None, elements=[]):
        if root:
            self.store_inorder(root.left, elements)
            elements.append(root.data)
            self.store_inorder(root.right, elements)

    def invert_tree(self, node):
        if not node:
            return None
        else:
            l = node.left
            node.left = node.right
            node.right = l
            self.invert_tree(node.left)
            self.invert_tree(node.right)

    def count_subtree(self, node):
        if node is None:
            return 1
        if node.left is None and node.right is None:
            return 1
        else:
            return self.count_subtree(node.left) + self.count_subtree(node.right)

    def print_inorder(self):
        self.inorder(self.root)

    def insert_node(self, key):
        if isinstance(key, list):
            if not self.root:
                self.root = Node(key[0])
            for l in key[1:]:
                self.insert(self.root, l)
        else:
            if not self.root:
                self.root = Node(key)
            self.insert(self.root, key)

    def get_node(self, key, node=None):
        if not node:
            node = self.root
        if key == node.data:
            return node
        elif key < node.data:
            return self.get_node(key, node.left) if node.left else None
        else:
            return self.get_node(key, node.right) if node.right else None

    def get_smaller_count(self, key):
        node = self.get_node(key)
        c = self.count_subtree(node)
        return c

    def height(self, node):
        if node is None:
            return 0
        else:
            # Compute the height of each subtree
            lheight = self.height(node.left)
            rheight = self.height(node.right)

            # Use the larger one
            if lheight > rheight:
                return lheight + 1
            else:
                return rheight + 1

    def printLevelOrder(self):
        h = self.height(self.root)
        for i in range(1, h + 1):
            self.printCurrentLevel(self.root, i)

    # Print nodes at a current level
    def printCurrentLevel(self, root, level):
        if root is None:
            return
        if level == 1:
            print(root.data, end=" ")
        elif level > 1:
            self.printCurrentLevel(root.left, level - 1)
            self.printCurrentLevel(root.right, level - 1)


root = Node(10)
root.left = Node(30)
root.right = Node(15)
root.left.left = Node(20)
root.right.right = Node(5)
t = BST()
ele_list = []
t.store_inorder(root, ele_list)
t.insert_node(ele_list)
# t.printLevelOrder()
# print("\n")
t.print_inorder()
# n = t.invert_tree(t.root)
# # print(n)
# # t.print_inorder()
# t.printLevelOrder()
