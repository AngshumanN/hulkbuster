import json
import os
import traceback
import re

from domain_sync_lib.utils.entity_cache import set_cache_data
from entity_lib.utils.config import EntityConfig
from entity_lib.utils.entity_cfg import entity_exists_in_domain, get_entity_attribute_ner_type, get_entity_cardinality
from entity_lib.utils.entity_util import get_domain_entities
from xpms_common.trace import Tracer
import operator

from domain_sync_lib.utils.entity_cfg import get_entity_cfg_ep
from entity_lib.utils.entity_enrichment import extract_entities, processs_nlp_text, infer_ner_type
from entity_lib.utils.graph_util import SVO
from xpms_lib_dag.constants import TaskType
from xpms_objects.models.elements import *

from xpms_common.mq_endpoint import TriggerHandler
from xpms_common.errors import InvalidUsageError, XpmsError, EntityNotFoundError, InternalError
from xpms_common.xpms_logger import XPMSLogger
import editdistance
from entity_lib.constants import NERS, DEFAULT_HEADING_CONFIDENCE
from xpms_storage.db_handler import DBProvider
from xpmsnlp.utils.rake_keyword_extractor import transform_key_phrases, RakeKeywordExtractor
from xpmsnlp.utils.word_util import WordSemantics
from xpmsnlp.services.handle_extract_link_entities import extract_link_entities

logger = XPMSLogger.get_instance()
tracer = Tracer.get_instance(service_name=os.getenv("PROJECT_NAME", "entity_lib"), log_level="INFO")


class ProcessedText(object):
    name = "Section Entity Candidates"
    task_type = TaskType.INTERNAL.value
    description = "service to receive processed text"

    request_trigger = "processed_text"
    response_trigger = "{0}_response".format(request_trigger)
    raw_document_entity = {}

    # def __init__(self, context, message):
    #     # super().__init__(context, message)
    #     self.response.metadata = {}

    def run(self, payload, raw_ent):
        # self.initialize_response(self.response_trigger)
        try:
            self.raw_document_entity = raw_ent
            self.handle_processed_section_elements(payload)

        except Exception as e:
            # logger.log(self.request.context, message=str(e), obj=self.request.data, error_obj=e)
            self.response.status_code = 500
            self.response.metadata = "error_message: {} traceback: {}".format(str(e), traceback.format_exc())
            self.response.success = False

    def handle_processed_section_elements(self, payload):
       # payload = self.request.data
        # self.context.update_context(method_name='ProcessedText.handle_processed_section_elements')

        elements = payload['raw_text']
        if elements:
            response = self.handle_section_elements(elements)

            assert response
        return True

    def handle_section_elements(self, elements):
        # self.context.update_context(method_name='ProcessedText.handle_section_elements')

        entities = []
        raw_elements_data = []
        element_heading_confidence = 1

        section_id = elements[0]['section_id']
        doc_id = elements[0]['doc_id']

        section_redis_key = "{0}_{1}".format(section_id, "test_req_id")
        doc_redis_key = "{0}_{1}".format(doc_id, "test_req_id")

        # fetching the document object from state
        doc_object = self.raw_document_entity

        if doc_object:
            entity_keys = doc_object['entities_look_for']
            domain_entity_ner_types = doc_object['domain_ners']

            solution_domain_objects = doc_object['solution_domain_objects']
            synonyms = [re.sub('[^0-9a-z A-Z]+', '', s).lower() for s in doc_object['synonyms']]
            heading_entity, heading_attribute = None, None
            failed_elements_dict = {}
            enable_svo_linking = doc_object.get('enable_svo_linking', "false")
            remove_field_synonym = doc_object.get("remove_field_synonym", "no")

            graph = {}
            if enable_svo_linking == "true":
                graph_path = "{0}/entity/assisted_linking/{1}/{1}.json".format("elinking2",
                                                                               doc_object['doc_class_name'])
                graph = SVO.get_instance(path=graph_path, doc_class_name=doc_object['doc_class_name'])

            heading_tagged_entities, tagged_entities_index_dict, heading_tag = self.get_tagged_entities(section_id,
                                                                                                        elements)
            if heading_tag is not None and len(heading_tag.split('.')) == 2:
                heading_attribute = heading_tag

            for elem_index, element in enumerate(elements):
                if element['type'] == "omr_field":
                    element['type'] = 'field'
                try:
                    extracted_entities = []
                    self.fetch_failed_element_text(element, section_id, failed_elements_dict)
                    if element['type'] in ['heading'] and element['entities'][0]:
                        extracted_entities = self.handle_heading_element(element, graph, entity_keys)
                        raw_elements_data.append([element['id'], element['type'], element['text'][0]])

                    elif element['type'] in ['paragraph', 'sentence'] and element['entities'][0]:
                        data = dict()
                        data["heading_attribute"] = heading_attribute
                        data["heading_tagged_entities"] = heading_tagged_entities
                        data["heading_tag"] = heading_tag
                        data["element_heading_confidence"] = element_heading_confidence
                        data["tagged_entities_index_dict"] = tagged_entities_index_dict
                        data["elem_index"] = elem_index
                        extracted_entities, heading_tagged_entities, heading_tag = \
                            self.handle_paragraph_sentence(element, graph, data, entity_keys)
                        raw_elements_data.append([element['id'], element['type'], element['text'][0]])

                    elif element['type'] in ['field', 'omr_field'] and element['entities'][0]:
                        enable_similarity_key_match = True if doc_object.get('enable_similarity_key_match',
                                                                             "false").lower() == "true" else False
                        extracted_entities = self.handle_field_element(element, entity_keys, solution_domain_objects,
                                                                       heading_tagged_entities=heading_tagged_entities,
                                                                       tagged_entities_index_dict=tagged_entities_index_dict,
                                                                       elem_index=elem_index,
                                                                       heading_entity=heading_tag,
                                                                       enable_similarity_key_match=enable_similarity_key_match)
                        raw_elements_data.append([element['id'], element['type'], element['text'][0]])

                    elif element['type'] == 'list':
                        extracted_entities = self.handle_list_element(element, graph)
                        raw_text = " ".join(element['text'])
                        raw_elements_data.append([element['id'], element['type'], raw_text])

                    elif element['type'] == 'table':
                        extracted_entities = self.handle_table_element(element, solution_domain_objects,
                                                                       domain_entity_ner_types,
                                                                       element_heading_confidence)

                    new_extracted_entities = []

                    if extracted_entities:
                        if element['type'] == 'table':
                            new_extracted_entities = extracted_entities
                        else:
                            for row_index, entity in enumerate(extracted_entities):
                                to_be_added = True
                                entity_value = re.sub('[^0-9a-z A-Z]+', '', entity['entity_value'][0]).lower()
                                if not element['type'] in ['table', 'field']:
                                    synonym_check = [s for s in synonyms if s and s.lower().strip() in entity_value]
                                    value_synonym_check = [s for s in synonyms if entity_value in s]
                                    if entity_value in synonyms or synonym_check or value_synonym_check:
                                        to_be_added = False
                                elif element['type'] in ['field']:
                                    if entity_value in synonyms:
                                        to_be_added = False

                                    if remove_field_synonym.lower() == "yes":
                                        original_ent_value = entity['entity_value'][0]
                                        field_synoms = [original_ent_value.index(synonym) for synonym in synonyms if
                                                        synonym in original_ent_value]
                                        if field_synoms:
                                            new_entity_value = list(entity['entity_value'])
                                            new_entity_value[0] = original_ent_value[:min(field_synoms)]
                                            entity['entity_value'] = tuple(new_entity_value)

                                if to_be_added:
                                    new_extracted_entities.append(entity)

                    if new_extracted_entities:
                        entities.append((new_extracted_entities, element['type']))

                    if raw_elements_data:
                        response = self.batch_insert_element_data(raw_elements_data)
                        assert response == "Successfully inserted"

                except Exception as e:
                    logger.log(self.context, message=str(e), obj=elements)
                    error_details = {'section_id': section_id, 'error': str(e), 'element': element}
                    doc_object['failed_section_elements'].append(error_details)
                    set_cache_data(section_redis_key + "_error", error_details)
                    set_cache_data(doc_redis_key, doc_object)

            # setting the section output in redis
            self.response.metadata = {}
            # set_cache_data(section_redis_key, entities)

            if failed_elements_dict:
                doc_object['failed_section_elements'].append(failed_elements_dict)
                # set_cache_data(doc_redis_key, doc_object)

            self.response.metadata.update(
                {"section_id": section_id, "extracted_entities": entities, "failed_elements": failed_elements_dict})
            # if is_all_element_sections_processed(doc_object['element_sections'], request_id):
            #     process_unstructured_document(context, solution_id, request_id, doc_object, execution_id=execution_id,
            #                                   retry_id=retry_id, user_id=user_id)
        else:
            error_message = "document object with ID {0} not found".format(doc_id)
            raise InvalidUsageError(error_message)

        return True

    def fetch_failed_element_text(self, element, section_id, failed_elements_dict):
        text_failed_list = []
        for elem in element['entities']:
            if not elem:
                index_number = element['entities'].index(elem)
                if 'error' in element and element['error'][index_number]:
                    text_failed_list.append(element['text'][index_number])

        if text_failed_list:
            failed_elements_dict[section_id] = {element['id']: text_failed_list}

    def get_tagged_entities(self, section_id, elements):
        heading_tagged_entities = {}
        tagged_entities_index_dict = {}
        section_redis_key = "{0}_{1}".format(section_id, "test_req_id")
        heading_entity, heading_attribute = None, None
        try:
            heading_tag = self.fetch_heading_ent_details(elements)
            if heading_tag and len(heading_tag.split('.')) == 2:
                heading_attribute = heading_tag
            else:
                heading_entity = heading_tag

            element_heading_confidence = self.fetch_heading_confidence(
                elements) if heading_entity or heading_attribute else 1

            heading_tagged_entities[heading_tag] = element_heading_confidence
            tagged_entities_index_dict[heading_tag] = {'index': 1}

        except Exception as e:
            logger.log(self.context, message=str(e), obj=elements)
            error_details = {'section_id': section_id, 'error': str(e), 'element': 'fetch_heading'}
            set_cache_data(section_redis_key + "_headingerror", error_details)
        return heading_tagged_entities, tagged_entities_index_dict, heading_tag

    def fetch_heading_ent_details(self, elements):
        extracted_entities = [elem['entities'][0] for elem in elements if
                              elem['type'] == 'heading' and elem['entities'][0]]
        for entity in extracted_entities:
            for ent in entity['entity_list']:
                for ent_cand in ent['ent_type']:
                    if len(ent_cand[0].split('.')) == 2:
                        entity_name, attribute = ent_cand[0].split('.')
                        entity_cfg = get_entity_cfg_ep({
                            "solution_id": "elinking2",
                            "entity_name": entity_name},
                            "test_req_id")

                        if attribute == 'entity' and entity_cfg:
                            return entity_name
                        elif entity_cfg:
                            return ent_cand[0]
        return None

    def fetch_heading_confidence(self, elements):
        cnf_score = 1
        extracted_entities = [elem for elem in elements if elem['type'] == 'heading' and elem['entities'][0]]
        if extracted_entities:
            cnf_score = extracted_entities[0]["element_confidence"]
        return cnf_score

    def handle_heading_element(self, element, graph, entity_keys):
        nlp_output = element['entities'][0]
        extracted_entities = extract_entities(nlp_res=nlp_output, graph=graph)

        if nlp_output.get('ontology_list'):
            response_data_o2 = self.extract_ontologies_from_element(nlp_output.get('ontology_list', []), entity_keys)
            response_data = self.extract_ontology_entities(nlp_output['ontology_list'])
            response_data.extend(response_data_o2)
            if response_data:
                extracted_entities.extend(response_data)

        np_entities = self.resolve_NP_entities(nlp_output)
        extracted_entities.extend(np_entities)

        assert self.attach_element_id(extracted_entities, element)

        return extracted_entities

    def extract_ontologies_from_element(self, ontologies, entity_keys):
        entities = []
        for entity in entity_keys:
            attribute = entity.split('.')[-1]
            for ontology in ontologies:
                tag = self.get_tag(ontology, attribute)
                if tag:
                    entities.append({
                        "entity_candidates": [entity],
                        "entity_value": tag,
                        "entity_score": 0.9,
                        "entity_source": "ontology"
                    })
        return entities

    def extract_ontology_entities(self, data):

        entities = []
        entity_attributes = {}
        for row in data:
            entity_name = row['tag'].split(".")[0]

            if not entity_name in entity_attributes:
                entity_cfg = get_entity_cfg_ep(request_data={
                    "solution_id": "elinking2",
                    "entity_name": entity_name
                }, request_id="test_req_id")
                if entity_cfg:
                    entity_attributes[entity_name] = entity_cfg[0]

            if entity_name in entity_attributes:
                attr_values_dict = {}
                for property, value in row['properties'].items():
                    property = property.lower().strip()
                    for attr in entity_attributes[entity_name]['attributes']:
                        key_name = attr['key_name'].lower().strip()
                        if key_name == property:
                            attr_name = "{0}.{1}".format(entity_name, property)
                            attr_values_dict[attr_name] = value
                        if attr['synonym']:
                            attr_synonym = [key.lower().strip() for key in attr['synonym']]
                            if property in attr_synonym:
                                attr_name = "{0}.{1}".format(entity_name, attr['key_name'])
                                attr_values_dict[attr_name] = value

                for attr, value in attr_values_dict.items():
                    entities.append({
                        "entity_candidates": [attr],
                        "entity_value": value,
                        "entity_score": 0.9,
                        "entity_source": "ontology"
                    })

        return entities

    def get_tag(self, ontologies, attribute):
        if not "hierarchy" in ontologies["properties"]:
            return None
        if "name_hierarchy" in ontologies["properties"]["hierarchy"]:
            if self.attribute_match_2(ontologies["properties"]["hierarchy"]["name_hierarchy"], attribute):
                return ontologies["key"]
        else:
            return None

    def attribute_match_2(self, param, attribute):
        class_name_list = []
        for param_ in param:
            class_name_list.extend(param_.split(">>>"))
        for text in class_name_list:
            if text and self.get_match(text, attribute):
                return True
        return False

    def get_match(self, w1, w2):
        match = False
        if len(w1) == 0:
            return match
        dist = 1 - 1.0 * editdistance.eval(w1, w2) / len(w1)
        if dist >= 0.75:
            match = True
        # print(dist)
        return match

    def resolve_NP_entities(self, nlp_output):
        entities_list = nlp_output['entity_list']
        entity_tagged = {}
        entity_cfg_dict = {}
        synonyms = {}
        attr_synonyms = []
        ners = NERS
        for entity in entities_list:
            if entity['text']:
                for type in entity['ent_type']:
                    if type[0] and len(type[0].split('.')) == 2:
                        entity_name, attribute = type[0].split('.')
                        if not attribute == 'entity':
                            synonym_exists = False
                            entity_cfg = None
                            if not entity_name in entity_cfg_dict:
                                entity_cfg = get_entity_cfg_ep({
                                    "solution_id": "elinking2",
                                    "entity_name": entity_name},
                                    "test_req_id")
                            else:
                                entity_cfg = entity_cfg_dict[entity_name]

                            if entity_cfg:
                                entity_cfg_dict[entity_name] = entity_cfg
                                entity_cfg = entity_cfg[0]
                                for attr in entity_cfg['attributes']:
                                    key_name = attr['key_name'].lower()
                                    if attribute.lower() == key_name and attr.get('synonym'):
                                        entity_value = entity['text'].lower().strip().replace(".", "").replace(":", "")
                                        synonym_list = [key.lower().strip() for key in attr['synonym']]
                                        synonym_list.append(key_name)
                                        for synonym in synonym_list:
                                            if entity_value == synonym.replace(".", "").replace(":", ""):
                                                synonyms[type[0]] = synonym_list
                                                synonym_exists = True
                                        if attr['ner_type'] in ners:
                                            synonyms[type[0]] = synonym_list
                                            synonym_exists = True

                            if synonym_exists:
                                entity_tagged[type[0]] = type

        extracted_entities = []
        for ent, attr in entity_tagged.items():
            entity = {
                "entity_candidates": [attr[0]],
                "entity_value": '',
                "entity_score": float(attr[1]) - 0.2,
                "entity_source": 'custom'
            }
            extracted_entities.append(entity)

        return extracted_entities

    def attach_element_id(self, entities, element, element_heading_confidence=1, source_tree_list=[],
                          tagged_entities_index_dict=None,
                          elem_index=1, heading_entity=None):
        element_id = element['id']
        for entity in entities:
            element_heading_confidence = element_heading_confidence if heading_entity and element_heading_confidence else DEFAULT_HEADING_CONFIDENCE
            sources_list = ["heading", str(element_heading_confidence)]
            sources_list.extend(source_tree_list)
            sources_list.extend([entity['entity_source'], entity["entity_score"]])
            sources_list.extend(["element_confidence", element["element_confidence"]])
            elem_conf_score = self.calculate_confidence(element, entity, element_heading_confidence)

            entity_attr = entity["entity_candidates"][0].split(".")
            ds_conf_score = None
            if len(entity_attr) == 2 and tagged_entities_index_dict:
                entity_name = entity_attr[0]
                if entity_name in tagged_entities_index_dict and entity_name != heading_entity:
                    entity_index = tagged_entities_index_dict[entity_name]["index"]
                    if max(entity_index, elem_index) !=0:
                        ds_conf_score = min(entity_index, elem_index) / max(entity_index, elem_index)

                tagged_entities_index_dict[entity_name] = {'index': elem_index}
            if ds_conf_score:
                elem_conf_score = elem_conf_score * ds_conf_score

            # if elem_conf_score < 0.25:
            #     elem_conf_score = 0.25

            entity['entity_value'] = (entity['entity_value'].strip(), element_id, elem_conf_score, sources_list)

        return True

    def calculate_confidence(self, element, entity, element_heading_confidence):
        multi_source = entity.get("multi_source", False)
        result = element["element_confidence"] * entity["entity_score"]
        if element_heading_confidence:
            result = result * element_heading_confidence
        return result

    def handle_paragraph_sentence(self, element, graph, data, entity_keys):
        heading_attribute = data["heading_attribute"]
        heading_tagged_entities = data["heading_tagged_entities"]
        heading_tag = data["heading_tag"]
        element_heading_confidence = data["element_heading_confidence"]
        tagged_entities_index_dict = data["tagged_entities_index_dict"]
        elem_index = data["elem_index"]

        nlp_output = element['entities'][0]
        head_entity = None
        if nlp_output["entity_list"]:
            extracted_entities = extract_entities(nlp_res=nlp_output, graph=graph,
                                                  heading_attribute=heading_attribute, text=element['text'])
            extracted_data, head_entities, head_entity = processs_nlp_text("elinking2",
                                                                           "test_req_id", nlp_output,
                                                                           element, heading_tagged_entities)
            heading_tagged_entities = head_entities if head_entities else heading_tagged_entities
            extracted_entities.extend(extracted_data)
        else:
            extracted_entities = []

        heading_tag = head_entity if head_entity else heading_tag
        if nlp_output.get('ontology_list'):
            response_data = self.extract_ontology_entities(nlp_output['ontology_list'])
            response_data2 = self.extract_ontologies_from_element(nlp_output.get('ontology_list', []), entity_keys)
            response_data.extend(response_data2)
            if response_data:
                extracted_entities.extend(response_data)
        if element['probable_entities'] and element['probable_entities'][0]:
            response_data = self.extract_raw_text_entities(nlp_res=element['probable_entities'][0])
            if response_data:
                extracted_entities.extend(response_data)

        np_entities = self.resolve_NP_entities(nlp_output)
        extracted_entities.extend(np_entities)

        assert self.attach_element_id(extracted_entities, element,
                                      element_heading_confidence=element_heading_confidence,
                                      tagged_entities_index_dict=tagged_entities_index_dict, elem_index=elem_index,
                                      heading_entity=head_entities)
        return extracted_entities, heading_tagged_entities, heading_tag

    def extract_raw_text_entities(self, text=None, nlp_res=None):
        try:
            if not nlp_res and text:
                nlp_res = extract_link_entities(solution_id="elinking2", text=text)
            elif not nlp_res and not text:
                nlp_res = []

            entities = []
            if nlp_res:
                res_sorted = sorted(nlp_res.items(), key=operator.itemgetter(1))

                if res_sorted:
                    entities.append({
                        "probable_entity_candidates": res_sorted[-1][0],
                        "probable_entity_score": res_sorted[-1][1]
                    })
            return entities

        except Exception as e:

            raise InternalError('Error occurred while fetching entities from raw text')

    def get_bag_of_words(self):
        try:
            find_query = dict(solution_id="elinking2")
            db_name = "entity_data"
            coll_name = "nlp_entity_cfg"
            db = DBProvider.get_instance("elinking2")
            results = db.find(table=coll_name, filter_obj=find_query)

            bag_of_words = {}
            for doc in results:
                entity_name = doc["entity_name"]
                entity_bag = []

                if doc["entity_cfg"]["entity_synonym"]:
                    entity_syn = doc["entity_cfg"]["entity_synonym"]
                    entity_syn_list = [item.split("_") for item in entity_syn]
                    [entity_bag.extend(elem) for elem in entity_syn_list]

                for attribute in doc["entity_cfg"]["attributes"]:
                    attr_name = attribute["key_name"]
                    entity_bag.extend(attr_name.split("_"))
                    if attribute["synonym"]:
                        attr_syn = attribute["synonym"]
                        attr_syn_list = [item.split("_") for item in attr_syn]
                        [entity_bag.extend(elem) for elem in attr_syn_list]

                # bag_of_words[entity_name] = list(set(entity_bag))
                bag_of_words[entity_name] = " ".join(set(entity_bag))

            return bag_of_words

        except Exception as e:
            self.context.log(message='Some Error: ' + str(e))

    def extract_key_phrases_service(self, text, score=4):
        # context = tracer.get_context(request_id="test_req_id", log_level="INFO")
        # context.start_span(component="entity_lib.processed_text.extract_key_phrases_service")
        try:
            rake = RakeKeywordExtractor(request_id="test_req_id", solution_id="elinking2")
            keywords = rake.extract(text, incl_scores=True)
            key_phrases = rake.filter_keywords_on_score(keywords, score)
            return json.loads(key_phrases)
        except Exception as e:
            self.context.log(message='Some Error: ' + str(e))
        # finally:
        #     # context.end_span()

    def handle_field_element(self, element, entity_keys, solution_domain_objects,
                             heading_tagged_entities=None, tagged_entities_index_dict=None, elem_index=None,
                             heading_entity=None, enable_similarity_key_match=False):
        preprocess = self.preprocess_keys(entity_keys)
        keys = list(preprocess.keys())

        extracted_entities = []

        nlp_output = element['entities'][0]['entity_list']
        spell_corrected_key = element['spell_corrected_text'][0] if 'spell_corrected_text' in element else ''
        tagged_entities = extract_entities(nlp_res=nlp_output)
        matched_entities = self.get_tagged_entity_match(tagged_entities, element['label'].lower().strip(),
                                                        solution_domain_objects, True,
                                                        spell_corrected_key.lower().strip())
        matched_entity_keys = [tag[0] for tag in matched_entities]
        fuzzy_tags_raw = self.get_fuzzy_entity_match(element['entities'][0],
                                                     solution_domain_objects,
                                                     "fuzzy_tags")
        new_fuzzy_tags = []
        for tag in fuzzy_tags_raw:
            if tag[0] in matched_entity_keys:
                index = matched_entity_keys.index(tag[0])
                matched_entities[index] = list(matched_entities[index])
                if matched_entities[index][1] < tag[1]:
                    matched_entities[index][1] = tag[1]
                    matched_entities[index][2] = "fuzzy_tags"
            else:
                new_fuzzy_tags.append(tag)
        new_tags = [tag[0] for tag in new_fuzzy_tags]
        if fuzzy_tags_raw:
            model_tags = self.get_fuzzy_entity_match(element['entities'][0],
                                                     solution_domain_objects,
                                                     "model_tags")
        else:
            model_tags = []
        for tag in model_tags:
            if tag[0] in new_tags:
                if tag[0] not in matched_entity_keys:
                    matched_entities.append(tag)
            else:
                matched_entities.extend(new_fuzzy_tags)

        if not matched_entities and enable_similarity_key_match:
            key = element['label'].strip().lower()
            distances = self.get_semantic_distance(key, keys)

            if distances and distances[0][1] > 0.8:
                infered_key = preprocess[distances[0][0]]
                score = distances[0][1]
                source = "semantic"
                matched_entities.append((infered_key, score, source))
            else:
                return extracted_entities

            # verifying ner type of infered key against value
            entity_name, attribute = infered_key.split('.')
            attribute_ner_type = get_entity_attribute_ner_type("elinking2", entity_name,
                                                               "test_req_id", attribute)

            value_ner_type = infer_ner_type("elinking2", element['field_value'], "test_req_id")

            if attribute_ner_type not in value_ner_type:
                return extracted_entities

        multiple_matched_entities = True if len(matched_entities) > 1 else False

        for match in matched_entities:
            entity_name, attribute = match[0].split('.')
            entity = {
                "entity_candidates": [match[0]],
                "entity_value": element['field_value'],
                "entity_score": match[1],
                "entity_source": match[2]
            }
            append_entity = True
            entity_name = entity_name.lower().strip()
            if multiple_matched_entities and heading_tagged_entities and entity_name not in heading_tagged_entities:
                entity["entity_score"] = entity["entity_score"] - 0.2

            if append_entity:
                extracted_entities.append(entity)

        element_heading_confidence = heading_tagged_entities[heading_entity] if heading_tagged_entities \
                                                                                and heading_entity in heading_tagged_entities else 1
        assert self.attach_element_id(extracted_entities, element,
                                      element_heading_confidence=element_heading_confidence,
                                      tagged_entities_index_dict=tagged_entities_index_dict, elem_index=elem_index,
                                      heading_entity=heading_entity)

        return extracted_entities

    def get_tagged_entity_match(self, entities, match, domain_objects, exact_match=False,
                                spell_corrected_key=''):
        tagged_entities = []
        entity_cfg_list = []
        for entity in entities:
            ent_value = entity['entity_value'].lower().replace(" ", "")
            value_matched = (
                    ent_value == match.replace(" ", "") or ent_value == spell_corrected_key.replace(" ", "")
            ) if exact_match else entity['entity_value'] in match
            if value_matched:
                key = entity['entity_candidates'][0]
                score = entity['entity_score']
                source = entity['entity_source']
                entity_keys = []
                if isinstance(key, str):
                    entity_keys = key.split('.')
                    if len(entity_keys) == 2:
                        entity_name = entity_keys[0]
                        entity_exists = True
                        if entity_name not in entity_cfg_list:
                            entity_exists = entity_exists_in_domain("elinking2", domain_objects,
                                                                    entity_name)

                        if entity_exists:
                            entity_cfg_list.append(entity_name)
                            tagged_entities.append((key, score, source))

        return tagged_entities

    def get_fuzzy_entity_match(self, nlp_tags, domain_objects, source):
        entity_cfg_list = []
        tagged_entities = []
        if source not in ["fuzzy_tags", "model_tags"]:
            return []
        tags = nlp_tags.get(source, [])

        if source == "model_tags":
            tags = [tags]
        for tag in tags:
            key = tag[0]
            score = tag[1]
            entity_keys = key.split('.')
            if len(entity_keys) == 2:
                entity_name = entity_keys[0]
                entity_exists = True
                # if entity_name not in entity_cfg_list:
                #     entity_exists = entity_exists_in_domain(solution_id, domain_objects, entity_name)

                if entity_exists:
                    entity_cfg_list.append(entity_name)
                if source == "fuzzy_tags":
                    score = 0.15 + 0.85 * (1 - 0.25 * score)
                elif source == "model_tags":
                    score = score * 0.35
                tagged_entities.append((key, score, source))
        return tagged_entities

    def preprocess_keys(self, keys):
        output = {}
        for key in keys:
            attribute = key.split('.')[-1]
            attribute = attribute.replace('_', ' ').lower()
            output[attribute] = key

        return output

    def get_semantic_distance(self, target_word, list_of_words):

        res = WordSemantics.get_semantic_distances(target_word, list_of_words)

        res = list(zip(list_of_words, res))
        output = sorted(res, key=lambda x: x[1], reverse=True)

        return output

    def handle_list_element(self, element, graph=None):
        extracted_entities = []
        for elem in element['entities']:
            if elem:
                list_elem_entities = extract_entities(nlp_res=elem, graph=graph)
                np_entities = self.resolve_NP_entities(elem)
                list_elem_entities.extend(np_entities)
                assert self.attach_element_id(list_elem_entities, element)
                extracted_entities.extend(list_elem_entities)

        return extracted_entities

    def handle_table_element(self, element, domain_objects, domain_ners,
                             element_heading_confidence):

        table_element = TableElement(json_obj=json.loads(element['table']))
        cells_list = table_element.headers + table_element.cells
        extracted_entities = []

        ner_value_attrs = self.form_ner_based_attrs(domain_ners)

        rows_cells_dict, columns_cells_dict = self.find_entities_in_cells(cells_list, element, domain_objects,
                                                                          ner_value_attrs)

        self.rectify_disambiguation_in_cell_entities(rows_cells_dict, columns_cells_dict)

        table_type, table_header_data = self.identify_table_header(rows_cells_dict, columns_cells_dict)

        self.identify_header_for_no_header_row_col(rows_cells_dict, columns_cells_dict,
                                                   table_type, table_header_data)

        extracted_entities_after_mapping = self.map_header_and_cell_values(rows_cells_dict,
                                                                           columns_cells_dict,
                                                                           table_type, table_header_data,
                                                                           element_heading_confidence)

        extracted_entities = self.fetch_parent_entity(domain_objects, extracted_entities_after_mapping)

        return extracted_entities

    def form_ner_based_attrs(self, domain_ners):
        ner_value_attributes = {}
        for x, y in domain_ners.items():
            if y not in ner_value_attributes:
                ner_value_attributes[y] = [x]
            else:
                ner_value_attributes[y].append(x)

        return ner_value_attributes

    def find_entities_in_cells(self, cells, nlp_res, domain_objects, ner_value_attrs):

        rows_cells_dict = {}
        columns_cells_dict = {}

        for index_number, cell_obj in enumerate(cells):

            text_value = cell_obj.text.strip()
            spell_corrected_text_value = nlp_res['spell_corrected_text'][
                index_number] if 'spell_corrected_text' in nlp_res else ''
            synonym_entities = []
            inferred_entities = []
            key_value_entities = {}
            if nlp_res['entities'][index_number]:
                nlp_output = nlp_res['entities'][index_number]
                if nlp_output:
                    nlp_output = nlp_output['entity_list']
                    key_value_entities.update(
                        self.find_key_values_within_cell(nlp_output, domain_objects))
                    tagged_entities = extract_entities(nlp_res=nlp_output)
                    synonym_entities, inferred_entities = self.fetch_category_tagged_entities(tagged_entities,
                                                                                              domain_objects,
                                                                                              ner_value_attrs,
                                                                                              text_value,
                                                                                              spell_corrected_text_value)
            row_index = [str(c) for c in cell_obj.row_index]
            row_index = ",".join(row_index)
            column_index = [str(c) for c in cell_obj.column_index]
            column_index = ",".join(column_index)
            cell_data = {'column_index': column_index, 'row_index': row_index, 'cell_text': text_value,
                         'cell_synonym': synonym_entities, 'id': cell_obj.id, 'cell_keyvalues': key_value_entities,
                         'cell_inferred': inferred_entities, 'cell_entities': [],
                         'element_confidence': cell_obj.confidence}

            if row_index not in rows_cells_dict:
                rows_cells_dict[row_index] = [cell_data]
            else:
                rows_cells_dict[row_index].append(cell_data)

            if column_index not in columns_cells_dict:
                columns_cells_dict[column_index] = [cell_data]
            else:
                columns_cells_dict[column_index].append(cell_data)

        return rows_cells_dict, columns_cells_dict

    def rectify_disambiguation_in_cell_entities(self, rows_cells_dict, columns_cells_dict):
        for row_no, row_cells_list in rows_cells_dict.items():
            for cell_no, cell_data in enumerate(row_cells_list):
                col_cells_list = columns_cells_dict[cell_data['column_index']]
                self.fetch_cell_entity_based_on_frequency(cell_data, row_cells_list, col_cells_list)

    def fetch_cell_entity_based_on_frequency(self, current_cell, row_cells_list, col_cells_list):
        cell_entities = current_cell['cell_synonym'] + current_cell['cell_inferred']
        cell_tagged_entity = None
        if not cell_entities:
            current_cell['cell_type'] = 'value'
        elif len(cell_entities) > 1:
            row_wise_freq = self.row_col_wise_cell_entity_frequency(row_cells_list)
            col_wise_freq = self.row_col_wise_cell_entity_frequency(col_cells_list)

            row_wise_entity_count = sorted(row_wise_freq.items(), key=lambda kv: kv[1])
            col_wise_entity_count = sorted(col_wise_freq.items(), key=lambda kv: kv[1])
            if row_wise_entity_count and col_wise_entity_count:
                max_row_wise_freq = row_wise_entity_count[-1]
                max_col_wise_freq = col_wise_entity_count[-1]
                if max_row_wise_freq[1] >= max_col_wise_freq[1]:
                    freq_wise_type = 'row'
                    freq_type = max_row_wise_freq[0]
                else:
                    freq_wise_type = 'col'
                    freq_type = max_col_wise_freq[0]

                for cell_no, cell_entity in enumerate(cell_entities):
                    entity_attr = cell_entity[0]
                    entity_name = entity_attr.split(".")[0]
                    entity_source = cell_entity[2]

                    entity_wise_soure = "{0}_{1}".format(entity_name, entity_source)
                    attr_wise_soure = "{0}_{1}".format(entity_attr, entity_source)
                    other_criteria_list = [entity_attr, entity_wise_soure, attr_wise_soure]
                    if freq_type == entity_name:
                        other_criteria = {}
                        row_col_wise_count = row_wise_entity_count if freq_wise_type == 'row' else col_wise_entity_count
                        for criteria in row_col_wise_count:
                            if criteria[0] in other_criteria_list:
                                other_criteria[criteria[0]] = criteria[1]

                        other_criteria_fre_count = sorted(other_criteria.items(), key=lambda kv: kv[1])
                        ent_fre_type = None
                        if other_criteria_fre_count:
                            ent_fre_type = other_criteria_fre_count[-1][0]
                        if ent_fre_type in other_criteria_list:
                            cell_tagged_entity = cell_entity
                    elif freq_type in other_criteria_list:
                        cell_tagged_entity = cell_entity
        else:
            cell_tagged_entity = cell_entities[0]

        del current_cell['cell_synonym']
        del current_cell['cell_inferred']

        if cell_tagged_entity:
            current_cell['cell_entities'] = [cell_tagged_entity]
            cell_type = 'key' if cell_tagged_entity[2] == 'synonym' else 'inferred'
            current_cell['cell_type'] = cell_type

    def row_col_wise_cell_entity_frequency(self, cells_list):
        cell_ent_fre_count = {}
        for cell in cells_list:
            cell_entities = cell.get('cell_synonym', []) + cell.get('cell_inferred', []) + cell.get('cell_entities', [])
            for cell_ent in cell_entities:
                entity_name = cell_ent[0].split(".")[0]
                entity_attr = cell_ent[0]
                source = cell_ent[2]
                entity_wise_soure = "{0}_{1}".format(entity_name, source)
                attr_wise_soure = "{0}_{1}".format(entity_attr, source)
                count_list = [entity_name, entity_attr, entity_wise_soure, attr_wise_soure]
                for count_data in count_list:
                    if count_data not in cell_ent_fre_count:
                        cell_ent_fre_count[count_data] = 1
                    else:
                        cell_ent_fre_count[count_data] = cell_ent_fre_count[count_data] + 1
        return cell_ent_fre_count

    def identify_table_header(self, rows_cells_dict, columns_cell_dict):
        no_of_columns = len(columns_cell_dict)
        col_threshold = EntityConfig.get_env_var("elinking2", "col_threshold")
        no_of_rows = len(rows_cells_dict) - 1
        row_threshold = EntityConfig.get_env_var("elinking2", "row_threshold")
        table_header_data = {}

        row_wise_count = self.count_source_entities(rows_cells_dict)
        col_wise_count = self.count_source_entities(columns_cell_dict)
        highest_keys_row = sorted(row_wise_count.items(), key=lambda kv: kv[1]['key'])[-1]
        highest_inferred_row = sorted(row_wise_count.items(), key=lambda kv: kv[1]['inferred'])[-1]
        row_keys = highest_keys_row[1]['key'] * 100 / no_of_columns
        row_inferred = highest_inferred_row[1]['inferred'] * 100 / no_of_columns
        highest_keys_col = sorted(col_wise_count.items(), key=lambda kv: kv[1]['key'])[-1]
        highest_inferred_col = sorted(col_wise_count.items(), key=lambda kv: kv[1]['inferred'])[-1]
        col_keys = highest_keys_col[1]['key'] * 100 / no_of_rows
        col_inferred = highest_inferred_col[1]['inferred'] * 100 / no_of_rows

        matched_col_no = highest_keys_col[0] if col_keys > row_threshold else highest_inferred_col[0]
        matched_row_header_no = highest_keys_row[0] if row_keys > col_threshold else highest_inferred_row[0]

        if row_keys >= col_threshold and (
                (col_keys > row_threshold or col_inferred > row_threshold) and matched_col_no == '0'):
            table_type = 'both'
            table_header_data['row_no'] = highest_keys_row[0]
            table_header_data['col_no'] = matched_col_no
        elif matched_col_no == '0' and col_keys >= row_threshold and (
                row_keys > col_threshold or row_inferred > col_threshold):
            table_type = 'both'
            table_header_data['row_no'] = matched_row_header_no
            table_header_data['col_no'] = highest_keys_col[0]
        elif row_keys >= col_threshold or highest_keys_row[1]['key'] > highest_keys_col[1]['key']:
            table_type = 'row'
            table_header_data['row_no'] = highest_keys_row[0]
        elif col_keys >= row_threshold or highest_keys_col[1]['key'] > highest_keys_row[1]['key']:
            table_type = 'col'
            table_header_data['col_no'] = highest_keys_col[0]
        else:
            table_type = 'row'
            table_header_data['row_no'] = '0'
        return table_type, table_header_data

    def count_source_entities(self, cells_dict):
        count_dict = {}
        for cell, cell_data in cells_dict.items():
            source_entities_count = {'key': 0, 'inferred': 0}
            for data in cell_data:
                cell_type = data['cell_type']
                if cell_type in source_entities_count:
                    source_entities_count[cell_type] = source_entities_count[cell_type] + 1
            count_dict[cell] = source_entities_count

        return count_dict

    def identify_header_for_no_header_row_col(self, rows_cells_dict, columns_cells_dict, table_type,
                                              table_data):
        header_cell_list = []
        cells_dict = {}
        index_type = None
        total_row_col = 0
        if table_type == 'row':
            header_cell_list = rows_cells_dict[table_data['row_no']]
            cells_dict = columns_cells_dict
            index_type = 'column_index'
            total_row_col = len(rows_cells_dict)
        elif table_type == 'col':
            header_cell_list = columns_cells_dict[table_data['col_no']]
            cells_dict = rows_cells_dict
            index_type = 'row_index'
            total_row_col = len(columns_cells_dict)

        for cell_no, cell_data in enumerate(header_cell_list):
            if not cell_data['cell_entities']:
                lookup_cell_list = cells_dict[cell_data[index_type]]
                inferred_header = self.fetch_probable_header_in_cell_list(lookup_cell_list, total_row_col)
                if inferred_header:
                    entity = inferred_header['entity'] + ('inferred_header',)
                    cell_data['cell_entities'] = [entity]
                    cell_data['cell_type'] = 'key'

    def fetch_probable_header_in_cell_list(self, cells_list, total_rows_col):
        headerless_col_threshold = EntityConfig.get_env_var("elinking2", "headerless_threshold")
        col_wise_count = {}
        for cell_no, cell_data in enumerate(cells_list):
            if cell_data['cell_entities']:
                for cell_ent in cell_data['cell_entities']:
                    attr = cell_ent[0]
                    if attr not in col_wise_count:
                        col_wise_count[attr] = {'count': 1, 'ent_details': cell_ent}
                    else:
                        col_wise_count[attr]['count'] = col_wise_count[attr]['count'] + 1

        if col_wise_count:
            highest_matched_attr = sorted(col_wise_count.items(), key=lambda kv: kv[1]['count'])[-1]
            matched_attr_percentage = highest_matched_attr[1]['count'] * 100 / total_rows_col
            if matched_attr_percentage >= headerless_col_threshold:
                attr = highest_matched_attr[0]
                return {'key': attr, 'entity': col_wise_count[attr]['ent_details'][0:3]}

        return None

    def map_header_and_cell_values(self, rows_cells_dict, columns_cell_dict,
                                   table_type, table_header_data, element_heading_confidence):
        extracted_entities = []
        key_value_keys = self.process_empty_and_key_value_cells(rows_cells_dict, columns_cell_dict,
                                                                table_type,
                                                                table_header_data, element_heading_confidence)
        if table_type == 'row' or table_type == 'col':

            header_no = table_header_data['row_no'] if table_type == 'row' else table_header_data['col_no']
            header_entities = rows_cells_dict[header_no] if table_type == 'row' else columns_cell_dict[header_no]
            header_tagged_entities, header_table_entities = self.fetch_header_entity_candidates(header_entities)
            cells_dict = rows_cells_dict if table_type == 'row' else columns_cell_dict

            parent_tables = self.fetch_child_parent_tables_from_key_value(key_value_keys, header_table_entities)

            mapped_entities = self.map_row_col_entities(header_tagged_entities, cells_dict, header_no, parent_tables,
                                                        header_table_entities, element_heading_confidence)
            if 'independent' in mapped_entities:
                extracted_entities.append(mapped_entities['independent'])
                del mapped_entities['independent']

            if parent_tables:
                parent_table_entities = {}
                for key, value in key_value_keys.items():
                    entity_name = key.split(".")[0]
                    if entity_name not in parent_table_entities:
                        parent_table_entities[entity_name] = {key: [value]}
                    else:
                        parent_table_entities[entity_name][key] = [value]

                included_tables = []
                for entity, entity_dict in parent_table_entities.items():
                    child_tables = [table for table, p_ent in parent_tables.items() if entity in p_ent]
                    for child_table in child_tables:
                        if child_table in mapped_entities:
                            included_tables.append(child_table)
                            child_ent_attr = "{0}.{1}".format(entity, child_table)
                            entity_dict[child_ent_attr] = mapped_entities[child_table]
                    extracted_entities.append({entity: [entity_dict]})

                for mapped_entity in mapped_entities:
                    if mapped_entity not in included_tables:
                        extracted_entities.append({mapped_entity: mapped_entities[mapped_entity]})
            else:
                if mapped_entities:
                    extracted_entities.append(mapped_entities)

        elif table_type == 'both':
            row_header_no = table_header_data['row_no']
            col_header_no = table_header_data['col_no']
            row_header_entities = rows_cells_dict[row_header_no]
            col_header_entities = columns_cell_dict[col_header_no]

            row_header_tagged_entities, row_header_tables = self.fetch_header_entity_candidates(row_header_entities)
            col_header_tagged_entities, col_header_tables = self.fetch_header_entity_candidates(col_header_entities)

            parent_tables = {}
            mapped_entities = []
            same_table_headers = False
            if len(row_header_tables) == 1 and len(col_header_tables) == 1:
                if row_header_tables[0] == col_header_tables[0]:
                    same_table_headers = True

            if not same_table_headers:
                for row_header_table in row_header_tables:
                    for col_header_table in col_header_tables:
                        child_entity = self.is_hierarchy_exist(row_header_table, col_header_table)
                        if child_entity:
                            if col_header_table in parent_tables:
                                parent_tables[col_header_table].append(row_header_table)
                            else:
                                parent_tables[col_header_table] = [row_header_table]

                if parent_tables:
                    mapped_entities = self.map_cross_table_row_col_entities(rows_cells_dict, row_header_no,
                                                                            row_header_tagged_entities, 'column_index',
                                                                            columns_cell_dict, col_header_no,
                                                                            col_header_tagged_entities, parent_tables)
                else:
                    for col_header_table in col_header_tables:
                        for row_header_table in row_header_tables:
                            child_entity = self.is_hierarchy_exist(col_header_table, row_header_table)
                            if child_entity:
                                if row_header_table in parent_tables:
                                    parent_tables[row_header_table].append(col_header_table)
                                else:
                                    parent_tables[row_header_table] = [col_header_table]

                    if parent_tables:
                        mapped_entities = self.map_cross_table_row_col_entities(columns_cell_dict, col_header_no,
                                                                                col_header_tagged_entities, 'row_index',
                                                                                rows_cells_dict, row_header_no,
                                                                                row_header_tagged_entities,
                                                                                parent_tables)
            elif same_table_headers:
                parent_tables = {col_header_tables[0]: [row_header_tables[0]]}
                mapped_entities = self.map_cross_table_row_col_entities(rows_cells_dict, row_header_no,
                                                                        row_header_tagged_entities,
                                                                        'column_index', columns_cell_dict,
                                                                        col_header_no,
                                                                        col_header_tagged_entities, parent_tables,
                                                                        same_table_headers)

            all_tables = col_header_tables + row_header_tables

            key_val_parent_tables = self.fetch_child_parent_tables_from_key_value(key_value_keys, all_tables)
            if key_val_parent_tables:
                key_val_parent_table_entities = {}
                for key, value in key_value_keys.items():
                    entity_name = key.split(".")[0]
                    if entity_name not in key_val_parent_table_entities:
                        key_val_parent_table_entities[entity_name] = {key: [value]}
                    else:
                        key_val_parent_table_entities[entity_name][key] = [value]

                included_tables = []
                for entity, entity_dict in key_val_parent_table_entities.items():
                    if entity in parent_tables:
                        for parent_table in parent_tables:
                            included_tables = [].append(parent_table)
                            child_ent_attr = "{0}.{1}".format(entity, parent_table)
                            entity_dict[child_ent_attr] = mapped_entities
                        extracted_entities.append({entity: [entity_dict]})

                for mapped_entity in mapped_entities:
                    if mapped_entity not in included_tables:
                        extracted_entities.append({mapped_entity: mapped_entities[mapped_entity]})
            else:
                if mapped_entities:
                    extracted_entities.append(mapped_entities)

        return extracted_entities

    def process_empty_and_key_value_cells(self, rows_cell_dict, columns_cell_dict, table_type,
                                          table_header_data,
                                          element_heading_confidence):
        key_value_keys = {}
        header_no = '0'
        cells_dict = {}
        default_header_no = '0'
        exclude_header_no = '0'
        if table_type in ['row', 'both']:
            header_no = table_header_data['row_no']
            cells_dict = rows_cell_dict
            exclude_header_no = table_header_data['col_no'] if table_type == 'both' else default_header_no
            table_type = 'row'
        elif table_type == 'col':
            header_no = table_header_data['col_no']
            cells_dict = columns_cell_dict
            exclude_header_no = default_header_no

        row_wise_key_val, col_wise_key_val = self.verify_row_col_key_value_entities(rows_cell_dict,
                                                                                    columns_cell_dict)
        for cell_no, cell_list in cells_dict.items():
            if header_no and cell_no > header_no:
                for cell_index, cell_data in enumerate(cell_list):

                    is_valid_key_val_row_col = self.verify_key_value_cell(cell_data, table_type, row_wise_key_val,
                                                                          col_wise_key_val, 'valid_key_value_cells')
                    col_index = cell_data['column_index']
                    is_valid_column = True if exclude_header_no and col_index > exclude_header_no else False
                    is_valid_key = self.verify_key_value_cell(cell_data, table_type, row_wise_key_val,
                                                              col_wise_key_val, 'valid_keys')
                    cell_key_values = cell_data['cell_keyvalues']
                    if cell_data['cell_type'] == 'key' and is_valid_column and not cell_key_values and is_valid_key:
                        index_type = 'column_index' if table_type == 'row' else 'row_index'
                        neibhbour_cell_dict = columns_cell_dict if table_type == 'row' else rows_cell_dict
                        neighbour_col_index = str(int(cell_data[index_type]) + 1)
                        value = self.fetch_value_for_key_across_cells(cell_data, index_type, neighbour_col_index,
                                                                      neibhbour_cell_dict)
                        if not value:
                            index_type = 'row_index' if table_type == 'row' else 'column_index'
                            neibhbour_cell_dict = rows_cell_dict if table_type == 'row' else columns_cell_dict
                            neighbour_row_index = str(int(cell_data[index_type]) + 1)
                            value = self.fetch_value_for_key_across_cells(cell_data, index_type, neighbour_row_index,
                                                                          neibhbour_cell_dict)
                        if value:
                            key_entities = cell_data['cell_entities']
                            for key_ent in key_entities:
                                attr = key_ent[0].split(".")[1]
                                if not attr == 'entity':
                                    result_conf, source_list = self.calculate_table_confidence_source(
                                        cell_data["element_confidence"], element_heading_confidence, key_ent)
                                    ent_value = (value, cell_data['id'], result_conf, source_list)
                                    cell_data['tagged_entities'] = {key_ent[0]: [ent_value]}
                                    columns_cell_dict[cell_data['column_index']][
                                        int(cell_data['row_index'])] = cell_data
                                    key_value_keys[key_ent[0]] = ent_value
                    if cell_key_values and is_valid_key_val_row_col and is_valid_column:
                        cell_data['cell_type'] = 'key'
                        for key, key_data in cell_key_values.items():
                            value = key_data['value']
                            ent_data = key_data['entities']
                            result_conf, source_list = self.calculate_table_confidence_source(
                                cell_data["element_confidence"],
                                element_heading_confidence, ent_data)
                            ent_value = (value, cell_data['id'], result_conf, source_list)
                            cell_data['tagged_entities'] = {ent_data[0]: [ent_value]}
                            columns_cell_dict[cell_data['column_index']][int(cell_data['row_index'])] = cell_data

        return key_value_keys

    def fetch_value_for_key_across_cells(self, current_cell, index_type, neighbour_index, cells_dict):
        if neighbour_index in cells_dict:
            cell_index_type = 'row_index' if index_type == 'column_index' else 'column_index'
            neighbour_cell = cells_dict[neighbour_index][int(current_cell[cell_index_type])]
            if neighbour_cell['cell_type'] == 'value' and neighbour_cell['cell_text']:
                value = neighbour_cell['cell_text']
                neighbour_cell['cell_text'] = ''
                return value
            elif neighbour_cell['cell_type'] == 'value' and not neighbour_cell['cell_text']:
                neighbour_index = str(int(neighbour_cell[index_type]) + 1)
                value = self.fetch_value_for_key_across_cells(neighbour_cell, index_type, neighbour_index, cells_dict)
                if value:
                    return value
        return None

    def find_key_values_within_cell(self, entities_list, domain_objects):

        sorted_entities = sorted(entities_list, key=lambda kv: kv.get('begin', 0))
        key_value_entities = {}
        for entity in sorted_entities:
            for type in entity['ent_type']:
                if type[0] and type[2] == 'synonym':
                    entity_list = type[0].split(".")
                    entity_name = entity_list[0]
                    if not entity_list[1] == 'entity':
                        entity_exists = entity_exists_in_domain("elinking2", domain_objects, entity_name)
                        if entity_exists:
                            key_value_entities[type[0]] = {'entities': (type[0], type[1], type[2]), 'value': ''}
                if type[0] and type[2] in ['NP', 'named_entity']:
                    for key, value in key_value_entities.items():
                        attr_name = key.split(".")[1].replace("_", "")
                        ent_value = entity['text'].replace(" ", "")
                        if not value['value'] and not ent_value == attr_name:
                            key_value_entities[key]['value'] = entity['text']

        valid_key_value_entities = {}
        for key, value in key_value_entities.items():
            if value['value']:
                valid_key_value_entities[key] = value

        return valid_key_value_entities

    def fetch_category_tagged_entities(self, entities, domain_objects, ner_value_attrs,
                                       text_value,
                                       spell_corrected_text_value=''):

        synonym_entities = []
        inferred_entities = []

        entity_cfg_list = []
        for entity in entities:
            key = entity['entity_candidates'][0]
            score = entity['entity_score']
            source = entity['entity_source']
            entity_keys = key.split('.')
            if len(entity_keys) == 2:
                entity_name = entity_keys[0]
                entity_exists = False
                if entity_name not in entity_cfg_list:
                    entity_exists = entity_exists_in_domain("elinking2", domain_objects, entity_name)
                else:
                    entity_exists = True

                if entity_exists:
                    entity_cfg_list.append(entity_name)
                    plan_ent_value = re.sub('[^0-9a-z A-Z]+', '', entity['entity_value']).strip().lower()
                    plan_txt_value = re.sub('[^0-9a-z A-Z]+', '', text_value).strip().lower()
                    spell_txt_value = re.sub('[^0-9a-z A-Z]+', '', spell_corrected_text_value).strip().lower()
                    if source in ['synonym'] and (
                            plan_ent_value == plan_txt_value or plan_ent_value == spell_txt_value):
                        entities_data = (key, score, source)
                        synonym_entities.append(entities_data)
                    elif source in ['corpus', 'dictionary', 'rules']:
                        entities_data = (key, score, source, entity['entity_value'])
                        inferred_entities.append(entities_data)
            elif source in ['named_entity']:
                entities_data = (key, score, source, entity['entity_value'])
                matched_ner_attrs = self.identity_ner_attribute(entities_data, ner_value_attrs,
                                                                domain_objects)
                inferred_entities.extend(matched_ner_attrs)

        return synonym_entities, inferred_entities

    def identity_ner_attribute(self, ner_entity, ner_value_attributes, domain_objects):
        ner_tagged_attrs = []

        ner_type = ner_entity[0]
        if ner_type in ner_value_attributes:
            ner_attrs = ner_value_attributes[ner_type]
            for attr in ner_attrs:
                entity_name = attr.split('.')[0]
                entity_exists = entity_exists_in_domain("elinking2", domain_objects, entity_name)
                if entity_exists:
                    ner_tagged_attrs.append((attr, ner_entity[1], ner_entity[2], ner_entity[3]))

        return ner_tagged_attrs

    def fetch_header_entity_candidates(self, entities_list):
        table_entity_count_dict = {}
        for index_no, cell in enumerate(entities_list):
            cell_entities = cell['cell_entities']
            if cell_entities:
                for entity in cell_entities:
                    ent_candidates = entity[0].split(".")
                    entity_name = ent_candidates[0]
                    if entity_name not in table_entity_count_dict:
                        table_entity_count_dict[entity_name] = 1
                    else:
                        prev_count = table_entity_count_dict[entity_name]
                        table_entity_count_dict[entity_name] = prev_count + 1

        table_entitiy_threshold = EntityConfig.get_env_var("elinking2", "table_entitiy_threshold")
        table_entities = [key for key, count in table_entity_count_dict.items() if count >= table_entitiy_threshold]

        tagged_headers_list = []
        for index_no, cell in enumerate(entities_list):
            cell_entities = cell['cell_entities']
            match_found = None
            if cell_entities:
                for entity in cell_entities:
                    ent_candidates = entity[0].split(".")
                    entity_name = ent_candidates[0]
                    if entity_name in table_entities:
                        match_found = entity

            match_found = match_found if match_found else ''
            tagged_headers_list.append(match_found)

        return tagged_headers_list, table_entities

    def fetch_child_parent_tables_from_key_value(self, key_value_keys, child_tables):
        parent_tables = {}
        if key_value_keys:
            for key in key_value_keys:
                entity_name = key.split(".")[0]
                if entity_name not in parent_tables and entity_name not in child_tables:
                    for child_table in child_tables:
                        child_entity = self.is_hierarchy_exist(entity_name, child_table)
                        if child_entity and child_table in parent_tables:
                            parent_tables[child_table].append(entity_name)
                        elif child_entity and child_table not in parent_tables:
                            parent_tables[child_table] = [entity_name]

        return parent_tables

    def is_hierarchy_exist(self, parent_entity, child_entity):
        child_ent_exists = get_entity_cardinality("elinking2", parent_entity, child_entity,
                                                  "test_req_id")
        if child_ent_exists:
            return child_entity

        return None

    def calculate_table_confidence_source(self, element_confidence, element_heading_confidence, header_entity=None,
                                          cell_entity=[]):
        result_confidence = 1
        result_path = ["heading", element_heading_confidence] if element_heading_confidence != 1 else []
        if header_entity:
            header_confidence = header_entity[1]
            result_confidence = header_confidence
            result_path.extend(["table_header", header_confidence, ])
        result_path.extend(["cell_confidence", element_confidence])
        result_confidence = result_confidence * element_heading_confidence * element_confidence
        if cell_entity:
            cell_entity = cell_entity[0]
            if len(cell_entity) >= 3:
                result_path.extend([cell_entity[2], cell_entity[1]])
                result_confidence = result_confidence * cell_entity[1]

        return result_confidence, result_path

    def fetch_parent_entity(self, domain_objects, extracted_entities):
        extracted_entities_dict = {}
        child_parent_found = []
        for dom in domain_objects:
            domain_entities = get_domain_entities("elinking2", dom, "test_req_id")
            for extracted_ent in extracted_entities:
                for ent, ent_data in extracted_ent.items():
                    for dom_ent in domain_entities:
                        if not ent == dom_ent:
                            is_parent = self.is_hierarchy_exist(dom_ent, ent)
                            if is_parent:
                                child_parent_found.append(ent)
                                parent_child_name = '.'.join([dom_ent, ent])
                                if dom_ent in extracted_entities_dict:
                                    extracted_entities_dict[dom_ent].append({parent_child_name: ent_data})
                                else:
                                    extracted_entities_dict[dom_ent] = [{parent_child_name: ent_data}]
                    if ent not in child_parent_found:
                        extracted_entities_dict[ent] = ent_data

        return [extracted_entities_dict] if extracted_entities_dict else []

    def map_row_col_entities(self, header_entities, cells_dict, table_header_no, parent_tables, header_tables,
                             element_heading_confidence):
        entity_tagged_entities = {}
        independent_group_list = []
        for header_no, cell_data_list in cells_dict.items():
            if header_no > table_header_no:
                entity_dict = {}
                for cell_no, cell_data in enumerate(cell_data_list):
                    header_entity = header_entities[cell_no]
                    if header_entity:
                        header_entity_attr = header_entity[0].split(".")[1]
                        if not header_entity_attr == 'entity':
                            cell_text = cell_data['cell_text'].strip()
                            inferred_sources = ['corpus', 'dictionary', 'named_entity', 'rules']
                            is_inferred_header = True if header_entity[2] in inferred_sources else False
                            if header_entity and (
                                    cell_data['cell_type'] == 'value' or is_inferred_header) and cell_text:
                                element_id = cell_data['id']
                                cell_confidence, cell_source = self.calculate_table_confidence_source(
                                    cell_data["element_confidence"], element_heading_confidence, header_entity,
                                    cell_data["cell_entities"])
                                entity_value = (cell_text, element_id, cell_confidence, cell_source)
                                entity_dict[header_entity[0]] = [entity_value]
                            elif cell_data['cell_type'] == 'key':
                                if cell_data.get('tagged_entities'):
                                    for key_ent, value in cell_data['tagged_entities'].items():
                                        entity_name = key_ent.split(".")[0]
                                        entity_attr = key_ent.split(".")[1]
                                        if not entity_attr == 'entity':
                                            if entity_name not in parent_tables and entity_name not in header_tables:
                                                child_tables = [table for table, p_ent in parent_tables.items() if
                                                                entity_name in p_ent]
                                                if not child_tables:
                                                    independent_group_list.append({key_ent: value})
                                            elif entity_name in header_tables:
                                                entity_dict.update({key_ent: value})
                if entity_dict:
                    self.form_entity_based_entities(entity_dict, entity_tagged_entities)

        if independent_group_list:
            independent_entity_dict = {}
            for group_dict in independent_group_list:
                for group, group_data in group_dict.items():
                    entity = group.split(".")
                    if len(entity) > 1:
                        entity_name = entity[0]
                        if entity_name not in independent_entity_dict:
                            independent_entity_dict[entity_name] = [{group: group_data}]
                        else:
                            independent_entity_dict[entity_name].append({group: group_data})

            if independent_entity_dict:
                entity_tagged_entities['independent'] = independent_entity_dict

        return entity_tagged_entities

    def form_entity_based_entities(self, entity_dict, entity_tagged_entities):
        entity_entities = {}
        for key, value in entity_dict.items():
            entity_name = key.split(".")[0]
            if entity_name in entity_entities:
                entity_entities[entity_name].update({key: value})
            else:
                entity_entities[entity_name] = {key: value}

        for entity_name, ent_data in entity_entities.items():
            if entity_name in entity_tagged_entities:
                entity_tagged_entities[entity_name].append(ent_data)
            else:
                entity_tagged_entities[entity_name] = [ent_data]

    def verify_row_col_key_value_entities(self, rows_cells_dict, column_cells_dict):
        row_wise_key_value_status = {}
        col_wise_key_value_status = {}
        cell_key_value_threshold = EntityConfig.get_env_var("elinking2", "cell_key_value_threshold")
        cell_summary_key_threshold = EntityConfig.get_env_var("elinking2", "summary_key_threshold")
        for row_no, row_cell_list in rows_cells_dict.items():
            row_wise_key_value_status[row_no] = {'valid_key_value_cells': False, 'valid_keys': False}
            no_of_key_val_cells = len([cell_no for cell_no, cell_data in enumerate(row_cell_list)
                                       if cell_data['cell_keyvalues']])
            no_of_key_cells = len([cell_no for cell_no, cell_data in enumerate(row_cell_list)
                                   if cell_data['cell_type'] == 'key'])

            matched_key_val_percentage = no_of_key_val_cells * 100 / len(row_cell_list)
            if matched_key_val_percentage >= cell_key_value_threshold:
                row_wise_key_value_status[row_no]['valid_key_value_cells'] = True

            if no_of_key_cells == cell_summary_key_threshold:
                row_wise_key_value_status[row_no]['valid_keys'] = True

        for col_no, col_cell_list in column_cells_dict.items():
            col_wise_key_value_status[col_no] = {'valid_key_value_cells': False, 'valid_keys': False}
            no_of_key_val_cells = len([cell_no for cell_no, cell_data in enumerate(col_cell_list)
                                       if cell_data['cell_keyvalues']])
            no_of_key_cells = len([cell_no for cell_no, cell_data in enumerate(col_cell_list)
                                   if cell_data['cell_type'] == 'key'])

            matched_percentage = no_of_key_val_cells * 100 / len(col_cell_list)
            if matched_percentage >= cell_key_value_threshold:
                col_wise_key_value_status[col_no]['valid_key_value_cells'] = True

            if no_of_key_cells == cell_summary_key_threshold:
                col_wise_key_value_status[col_no]['valid_keys'] = True

        return row_wise_key_value_status, col_wise_key_value_status

    def verify_key_value_cell(self, cell_data, table_type, row_wise_key_value_status, col_wise_key_value_status,
                              criteria):
        if table_type == 'row':
            if criteria == 'valid_keys':
                row_no = cell_data['row_index']
                if row_no in row_wise_key_value_status and row_wise_key_value_status[row_no][criteria]:
                    return True
            else:
                col_no = cell_data['column_index']
                if col_no in col_wise_key_value_status and col_wise_key_value_status[col_no][criteria]:
                    return True
        elif table_type == 'col':
            row_no = cell_data['row_index']
            if row_no in row_wise_key_value_status and row_wise_key_value_status[row_no][criteria]:
                return True

        return False

    def map_cross_table_row_col_entities(self, parent_cell_dict, parent_header_no, parent_header_entities,
                                         index_type, child_cells_dict, child_header_no, child_header_entities,
                                         parent_tables, same_header=False):
        entity_based_entities = {}
        parent_header_cell_list = parent_cell_dict[parent_header_no]
        for cell_no, parent_header_entity in enumerate(parent_header_entities):
            parent_entity_dict = {}
            if parent_header_entity:
                parent_header_entity_attr = parent_header_entity[0].split(".")[1]
                if not parent_header_entity_attr == 'entity':
                    source = parent_header_entity[2]
                    inferred_sources = ['corpus', 'dictionary', 'named_entity', 'rules']
                    cell_data = parent_header_cell_list[cell_no]
                    cell_text = parent_header_entity[3] if source in inferred_sources else cell_data['cell_text']
                    entity_value = (cell_text, cell_data['id'], parent_header_entity[1], parent_header_entity[2])
                    parent_entity_dict[parent_header_entity[0]] = [entity_value]
                    child_index = cell_data[index_type]
                    if child_index > child_header_no:
                        child_cells_list = child_cells_dict[child_index]
                        child_entity_dict = {}
                        for child_cell_no, child_cell_data in enumerate(child_cells_list):
                            child_cell_entity = child_header_entities[child_cell_no]
                            if child_cell_entity:
                                child_header_entity_attr = child_cell_entity[0].split(".")[1]
                                if not child_header_entity_attr == 'entity':
                                    inferred_sources = ['corpus', 'dictionary', 'named_entity', 'rules']
                                    c_cell_text = child_cell_data['cell_text'].strip()
                                    c_cource = child_cell_entity[2]
                                    c_cell_text = child_cell_entity[3] if c_cource in inferred_sources else c_cell_text
                                    c_cell_element_id = child_cell_data['id']
                                    c_entity_value = (
                                        c_cell_text, c_cell_element_id, child_cell_entity[1], child_cell_entity[2])
                                    child_entity_dict[child_cell_entity[0]] = [c_entity_value]
                        if child_entity_dict and not same_header:
                            child_entities = {}
                            for key, value in child_entity_dict.items():
                                entity_name = key.split(".")[0]
                                if entity_name in child_entities:
                                    child_entities[entity_name].update({key: value})
                                else:
                                    child_entities[entity_name] = {key: value}
                            for entity_name, ent_data in child_entities.items():
                                if entity_name in parent_tables:
                                    for parent_table in parent_tables[entity_name]:
                                        child_ent_attr = "{0}.{1}".format(parent_table, entity_name)
                                        parent_entity_dict[child_ent_attr] = [ent_data]
                        elif child_entity_dict and same_header:
                            parent_entity_dict.update(child_entity_dict)
            if parent_entity_dict:
                self.form_entity_based_entities(parent_entity_dict, entity_based_entities)

        return entity_based_entities

    def batch_insert_element_data(self, elements_data):
        try:

            for element in elements_data:
                element_id = element[0]
                data = {'element_id': element[0], 'element_type': element[1], 'element_value': element[2]}
                set_cache_data(element_id, data)

            '''
            #disablig this as we moved to redis
            db = DB.get_instance(keyspace=os.getenv('ENTITY_KEYSPACE', 'entity_data'))

            session = db.get_session()

            insert_sql = session.prepare("INSERT INTO raw_elements_data(solution_id,element_id,element_type,element_value) VALUES (?,?,?,?)")

            batch = BatchStatement()

            for element in elements_data:
                element_id = uuid.UUID("{0}".format(element[0]))
                batch.add(insert_sql, (solution_id, element_id, element[1], element[2]))

            session.execute(batch)
            '''

            return "Successfully inserted"

        except Exception as e:
            tb = traceback.format_exc()
            raise InternalError("DB operation failed saving raw elements entity" + str(e), traceback=tb)

    @staticmethod
    def generate_service_configurations():
        return []


if __name__ == "__main__":
    payload = {"raw_text":[{"section_id":"314cb2c1-05d9-4816-ad19-dfcc461ba7f8","doc_id":"853a705e-95ae-4b82-90c5-8e4b08717c17","id":"2b1fe110-adee-48cf-a538-5783e170eb7f","type":"sentence","text":["Name Title Harold Craig Patient Education Liaison Services Provider Name MCKESSON PSP NameNumber or MR Project NameNumber The Oak Clinic"],"tokenize":True,"original_text":"Name, Title: Harold Craig, Patient Education Liaison Services Provider Name: MCKESSON PSP Name/Number or MR Project Name/Number: The Oak Clinic","element_confidence":0.8,"error":[""],"entities":[{"entity_list":[{"num":0,"sent_num":0,"begin":0,"end":10,"tok_begin":0,"tok_end":1,"text":"Name Title","attributes":[],"relevance":0,"ent_type":["case_information.source_case_form_sender_name"],"confidence":0,"value":None,"is_negation":False},{"num":7,"sent_num":0,"begin":0,"end":10,"tok_begin":0,"tok_end":1,"text":"Name Title","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":8,"sent_num":0,"begin":11,"end":41,"tok_begin":2,"tok_end":5,"text":"Harold Craig Patient Education","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":1,"sent_num":0,"begin":24,"end":31,"tok_begin":4,"tok_end":4,"text":"Patient","attributes":[],"relevance":0,"ent_type":["case_information.source_case_form_sender_organization"],"confidence":0,"value":None,"is_negation":False,"proximity_np":[["Name Title",3]]},{"num":1,"sent_num":1,"begin":42,"end":58,"tok_begin":6,"tok_end":7,"text":"Liaison Services","attributes":[],"relevance":0,"ent_type":[["ORG",0.8,"named_entity"]],"confidence":0,"value":None,"is_negation":False},{"num":9,"sent_num":1,"begin":42,"end":67,"tok_begin":6,"tok_end":8,"text":"Liaison Services Provider","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":2,"sent_num":1,"begin":50,"end":72,"tok_begin":7,"tok_end":9,"text":"Services Provider Name","attributes":[],"relevance":0,"ent_type":["case_information.source_case_form_sender_organization"],"confidence":0,"value":None,"is_negation":False},{"num":10,"sent_num":2,"begin":68,"end":81,"tok_begin":9,"tok_end":10,"text":"Name MCKESSON","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":3,"sent_num":3,"begin":82,"end":121,"tok_begin":11,"tok_end":16,"text":"PSP NameNumber or MR Project NameNumber","attributes":[],"relevance":0,"ent_type":["case_information.sponsor_study_number"],"confidence":0,"value":None,"is_negation":False},{"num":11,"sent_num":3,"begin":82,"end":96,"tok_begin":11,"tok_end":12,"text":"PSP NameNumber","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":2,"sent_num":3,"begin":100,"end":102,"tok_begin":14,"tok_end":14,"text":"MR","attributes":[],"relevance":0,"ent_type":[["ORG",0.8,"named_entity"]],"confidence":0,"value":None,"is_negation":False,"proximity_np":[["PSP NameNumber",2]]},{"num":12,"sent_num":3,"begin":100,"end":110,"tok_begin":14,"tok_end":15,"text":"MR Project","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":13,"sent_num":4,"begin":111,"end":121,"tok_begin":16,"tok_end":16,"text":"NameNumber","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":14,"sent_num":5,"begin":122,"end":136,"tok_begin":17,"tok_end":19,"text":"The Oak Clinic","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4}],"ontology_list":[],"context":{"svo":[],"spo":[],"svp":[],"pps":[],"noun_chunks":["Name Title","Harold Craig Patient Education","Liaison Services Provider","Name MCKESSON","PSP NameNumber","MR Project","NameNumber","The Oak Clinic"]},"noun_list":[]}],"probable_entities":[],"link_entities_error":[]},{"section_id":"314cb2c1-05d9-4816-ad19-dfcc461ba7f8","doc_id":"853a705e-95ae-4b82-90c5-8e4b08717c17","id":"a1e44975-4ad2-4877-b34c-0694983b6bc6","type":"sentence","text":["Telephone 7831587383"],"tokenize":True,"original_text":"Telephone: 7831587383","element_confidence":0.8,"error":[""],"entities":[{"entity_list":[{"num":0,"sent_num":0,"begin":0,"end":9,"tok_begin":0,"tok_end":0,"text":"Telephone","attributes":[],"relevance":0,"ent_type":["case_information.source_case_form_sender_tel"],"confidence":0,"value":None,"is_negation":False},{"num":3,"sent_num":0,"begin":0,"end":9,"tok_begin":0,"tok_end":0,"text":"Telephone","attributes":[],"relevance":0,"ent_type":[["NP",0.5,"NP"]],"confidence":0.4},{"num":1,"sent_num":0,"begin":10,"end":20,"tok_begin":1,"tok_end":1,"text":"7831587383","attributes":[],"relevance":0,"ent_type":[["CARDINAL",0.8,"named_entity"],["ipaddress",0.8,"rules"],["phone_number",0.8,"rules"]],"confidence":0,"value":None,"is_negation":False}],"ontology_list":[],"context":{"svo":[],"spo":[],"svp":[],"pps":[],"noun_chunks":["Telephone"]},"noun_list":[]}],"probable_entities":[],"link_entities_error":[]}],"configuration":{"transactional_commit":False}}
    raw_ent = {'entity_id': 'd7a1b0e7-44bc-4f8a-afa9-6c9091518d12', 'solution_id': 'elinking2',
               'entity_name': 'document', 'data': {}, 'entity_type': 'document',
               'solution_domain_objects': ['combined_domain_object'], 'domain_ners': {},
               'entities_look_for': ['treating_physician_information.reporter_fax',
                                     'treating_physician_information.reporter_family_name',
                                     'treating_physician_information.reporter_middle_name',
                                     'treating_physician_information.reporter_name',
                                     'treating_physician_information.reporter_telephone',
                                     'treating_physician_information.reporter_address',
                                     'treating_physician_information.reporter_email',
                                     'treating_physician_information.reporter_full_name',
                                     'test_information.test_result_highest_range',
                                     'test_information.test_result_lowest_range', 'test_information.test_date',
                                     'test_information.test_result', 'test_information.test_name',
                                     'test_information.lab_result_notes', 'reporter_information.reporter_date_time',
                                     'reporter_information.reporter_fax', 'reporter_information.reporter_family_name',
                                     'reporter_information.reporter_middle_name', 'reporter_information.reporter_name',
                                     'reporter_information.reporter_title', 'reporter_information.reporter_suffix',
                                     'reporter_information.reporter_country',
                                     'reporter_information.reporter_qualification',
                                     'reporter_information.reporter_email', 'reporter_information.reporter_telephone',
                                     'reporter_information.reporter_hcp', 'reporter_information.reporter_address',
                                     'reporter_information.reporter_full_name', 'event_information.event_occur_country',
                                     'event_information.seriousness_criteria_death',
                                     'event_information.seriousness_criteria_other',
                                     'event_information.seriousness_criteria_lifethreatening',
                                     'event_information.seriousness_criteria_congenital_anomaly',
                                     'event_information.event_additional_information',
                                     'event_information.event_verbatim', 'event_information.event_outcome',
                                     'event_information.event_start_date', 'drug_information.drug_event_recur',
                                     'drug_information.drug_dosage_unit',
                                     'drug_information.drug_treatment_duration_text',
                                     'drug_information.drug_action_taken',
                                     'drug_information.drug_recur_readministration',
                                     'drug_information.drug_rechallenge_result',
                                     'drug_information.drug_characterization',
                                     'drug_information.drug_assessment_result',
                                     'drug_information.drug_indication_verbatim', 'drug_information.ongoing',
                                     'drug_information.drug_end_date', 'drug_information.drug_start_date',
                                     'drug_information.drug_administration_route_text',
                                     'drug_information.drug_interval_dosage_text', 'drug_information.drug_dosage_text',
                                     'drug_information.medicinal_product', 'patient_information.patient_last_name',
                                     'patient_information.patient_first_name',
                                     'patient_information.patient_medical_history_text',
                                     'patient_information.patient_onset_age_unit',
                                     'patient_information.patient_pregnant', 'patient_information.patient_initial',
                                     'patient_information.patient_past_drug_start_date',
                                     'patient_information.patient_past_drug_end_date',
                                     'patient_information.patient_weight',
                                     'patient_information.patient_death_report_text',
                                     'patient_information.patient_death_date',
                                     'patient_information.patient_onset_age_text',
                                     'patient_information.patient_birth_date',
                                     'patient_information.patient_investigation_number',
                                     'patient_information.patient_sex', 'case_information.latest_receive_datetime',
                                     'case_information.first_receive_safety_datetime',
                                     'case_information.first_receive_datetime', 'case_information.duplicate_source',
                                     'case_information.followup_receipt_date', 'case_information.duplicate_identifier',
                                     'case_information.safety_report_id', 'case_information.report_type',
                                     'case_information.receipt_type', 'case_information.case_narrative',
                                     'case_information.sponsor_study_number',
                                     'case_information.source_case_form_sender_organization',
                                     'case_information.source_case_form_sender_tel',
                                     'case_information.source_case_form_sender_name'],
               'extraction_start_time': '2021-07-12 11:40:18.891469', 'root_id': 'd7a1b0e7-44bc-4f8a-afa9-6c9091518d12',
               'synonyms': ['reporterfax', 'Physician Fax', 'reporterfamilyname', 'Physician Last Name',
                            'reportermiddlename', 'Physician Middle Name', 'reportername', 'Physician First Name',
                            'reportertelephone', 'Phone', 'No phone', 'Physician Phone', 'reporteraddress', 'Address',
                            'Physician Address', 'reporteremail', 'Email', 'Physician Email', 'reporterfullname',
                            'Name', 'testresulthighestrange', 'Normal Range', 'Normal Range (Specify units)',
                            'testresultlowestrange', 'Normal Range (Specify units)', 'testdate', 'Date (DD-MM-YYYY)',
                            'Date (DD- MM- YYYY)', 'testresult', 'Value', 'testname', 'Tests', 'labresultnotes',
                            'Test / Assessment / Notes', 'reporterdatetime', 'Date and Time to call for follow- up',
                            'Date and Time to call for follow up', 'Date and Time to call for follow-up',
                            'Date and Time to call for follow  up', 'reporterfax', 'Reporter Fax', 'reporterfamilyname',
                            'Reporter Last Name', 'reportermiddlename', 'Reporter Middle Name', 'reportername',
                            'Reporter First Name', 'reportertitle', 'Reporter Title', 'reportersuffix', 'Signature',
                            'reportercountry', 'Country of occurrence', 'reporterqualification', 'reporteremail',
                            'Email', 'Reporter Email', 'reportertelephone', 'Phone', 'No phone', 'reporterhcp',
                            'Healthcare Professional', 'Health Care Professional', 'Health Care Professional?',
                            'Health Care Prof essional?', 'Health Care Prof essional', 'Healthcare Professional?',
                            'reporteraddress', 'Address', 'Address(Countrymustbespecified)',
                            'Address (Country must be specified)', 'AddressCountrymustbespecified',
                            'Reporter Address (please print)', 'Reporter Address please print',
                            'Reporter Address  please print', 'reporterfullname', 'Name', 'eventoccurcountry',
                            'seriousnesscriteriadeath', 'seriousnesscriteriaother',
                            'seriousnesscriterialifethreatening', 'seriousnesscriteriacongenitalanomaly',
                            'eventadditionalinformation', 'eventverbatim', 'eventoutcome', 'Outcome of Event',
                            'Outcom e of Event', 'eventstartdate', 'Date Event Started', 'Date adverse event started',
                            'Date AE started', 'drugeventrecur', 'Did event recur', 'drugdosageunit', 'Dosage Unit',
                            'drugtreatmentdurationtext', 'Duration of Administration', 'Duration of', 'drugactiontaken',
                            'Action taken on this suspect drug', 'drugrecurreadministration',
                            'If stopped, was drug re-administer', 'If stopped, was drug re- administer',
                            'If stopped was drug re- administer', 'drugrechallengeresult', 'If yes, recur date',
                            'If yes recur date', 'If yes  recur date', 'drugcharacterization',
                            'Suspect Product Information',
                            'Suspect Product Information(complete any known information)',
                            'Suspect Product Informationcomplete any known information', 'SuspectProductInformation',
                            'SuspectProductInformationcompleteanyknowninformation',
                            'Sus pe ct Pr oduct Infor m ation (complete any know n inf ormation)',
                            'drugassessmentresult', 'Related to Company Product', 'Related to Company Product?',
                            'Related to Com pany Product', 'Related to Com pany Product?', 'Related te Company Product',
                            'drugindicationverbatim', 'Indication', 'Indication / Taken For', 'Taken For', 'ongoing',
                            'Ongoing', 'Ongoing?', 'Stop Date / Ongoing?', 'drugenddate', 'Therapy stop date',
                            'Therapy Dates', 'Stop Date', 'drugstartdate', 'Therapy start date', 'Therapy Dates',
                            'Therapy Dates from', 'Start Date', 'drugadministrationroutetext', 'Route',
                            'drugintervaldosagetext', 'Frequency', 'drugdosagetext', 'Dose/Unit', 'Dose', 'Dose/ Unit',
                            'DoseUnit', 'Dose Unit', 'Dosage', 'medicinalproduct', 'Suspect Product Name',
                            'Product Name (INN', 'Brand)', 'Product Name', 'Drug/Product Name', 'DrugProduct Name',
                            'Drug Product Name', 'patientlastname', 'Patient Last NameInitial', 'Patient Last Name',
                            'Patient Last Name Initial', 'Patient Last Name/Initial', 'patientfirstname',
                            'Patient First Name/Initial', 'Patient First NameInitial', 'Patient First Name',
                            'Patient First Name Initial', 'Patient initials', 'patientmedicalhistorytext',
                            'RELEVANT MEDICAL HISTORY/CONDITIONS & PATIENT RISK FACTORS', 'relevant_medical_history',
                            'patientonsetageunit', 'Age Unit', 'patientpregnant', 'Pregnancy', 'patientinitial',
                            'patientpastdrugstartdate', 'From/To Dates', 'patientpastdrugenddate', 'patientweight',
                            'patientdeathreporttext', 'Cause of death', 'Causeofdeath', 'Cause of Death',
                            'patientdeathdate', 'Date of death', 'Dateofdeath', 'Date of Death', 'patientonsetagetext',
                            'Age', 'Age or Age group', 'AgeorAgegroup', '2a. AGE', 'patientbirthdate', 'Date Of Birth',
                            '2. DATE OF BIRTH', 'Date of Birth', 'patientinvestigationnumber',
                            'Unique Patient Identifier', 'Unique Patient Identif ier', 'patientsex', 'Sex', '3. SEX',
                            'Gender', 'latestreceivedatetime', 'Signature Date', 'firstreceivesafetydatetime',
                            'Local PV receipt date', 'firstreceivedatetime',
                            'Date Event was First Reported to Services Provider',
                            'Date Event w as First Reported to Services Provider', 'Date Event Reported',
                            'Company contact date', 'duplicatesource', 'followupreceiptdate',
                            'Date and Time to call for follow-up', 'duplicateidentifier', 'Local reference ID',
                            'safetyreportid', 'AEGIS database ID', 'reporttype', 'Source of Case:', 'Source of Case',
                            'receipttype', 'Type of Case:', 'Type of Case', 'casenarrative', 'event verbatim',
                            'sponsorstudynumber', 'PSP NameNumber or MR Project NameNumber', 'Name of Program',
                            'PP NameNumber or MR Project NameNumber', 'PP Name/Nu mber or MR Project Name/Nu mber',
                            'PP NameNu mber or MR Project NameNu mber', 'PP Name/Number or MR Project Name/Number',
                            'sourcecaseformsenderorganization', 'Services Provider Name', 'Support Program No',
                            'Patient Support Program NameNumber', 'Patient Support Program Name',
                            'sourcecaseformsendertel', 'Telephone', 'Telephone Number', 'sourcecaseformsendername',
                            'Name Title', 'Name', 'Title', 'NameTitle', 'Name, Title'], 'section_hierarchy': {
            '0ca2bac4-505d-4ccc-9241-a01465ff1ccc': {
                'sections': {'fcab1427-86d9-4bdb-84ab-34b546109232': {'sections': {}, 'entities': 'process_elements'},
                             '40d0445b-d011-4554-830b-9ed86ae9ad91': {'sections': {}, 'entities': 'process_elements'},
                             'd912afd0-c28e-4648-9e04-7f088d8c01ca': {'sections': {
                                 '67a0b8a3-86ce-46b6-aa82-1579f237f6af': {'sections': {},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             '02ef2de8-a1a2-4e10-b265-34c3b4de8c80': {'sections': {
                                 '109b3331-e1bf-4d4d-97e5-9b6c651d10b4': {'sections': {
                                     'c17dbb2c-2c7e-4b39-a150-3964ae10cc1c': {'sections': {},
                                                                              'entities': 'process_elements'}},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             'bc99d33b-d6f9-44a6-ae81-077d69c7c4c3': {'sections': {
                                 'c0d87b70-5084-416c-abb6-6624a5387c00': {'sections': {},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             '175d843d-29a8-40a3-bb25-19b0d5576f98': {'sections': {
                                 '5e886d4d-dc3b-4cac-8041-aa1e5aec3070': {'sections': {
                                     '02c26a10-a5ef-4225-9b67-56cfd22baabc': {'sections': {},
                                                                              'entities': 'process_elements'},
                                     '355f8b59-7c71-4f18-b8ae-7829ead49888': {'sections': {},
                                                                              'entities': 'process_elements'}},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             '8f91811f-2c67-43bb-92d4-0d0d96d5c0c2': {'sections': {}, 'entities': 'process_elements'},
                             '64d3c6fa-27a2-4bc3-b766-55d8ccb7d53e': {'sections': {
                                 'b063cc60-4395-4ed5-9c6c-4874d6d4a3b9': {'sections': {
                                     '83d6ad06-5cd7-4c01-a996-4019bc44af77': {'sections': {},
                                                                              'entities': 'process_elements'},
                                     '9dcc560a-2716-4775-aa2f-7720de264f2a': {'sections': {},
                                                                              'entities': 'process_elements'},
                                     '955a9ba6-68ab-4ffb-b4a2-b291be6109ed': {'sections': {},
                                                                              'entities': 'process_elements'}},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             'c8696202-aa3e-43ed-bc9b-0f0d4ded4dbc': {'sections': {
                                 '8cbfa7ee-f1c6-45c6-a68d-7a83ad96a0af': {'sections': {
                                     'da37a3d4-3414-4467-b291-2c804555d25b': {'sections': {},
                                                                              'entities': 'process_elements'}},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             'cdccf547-c549-439d-9b00-fe43532c774b': {'sections': {
                                 '8ba7a8eb-5193-45f5-bc21-c2830564360b': {'sections': {},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             'baa38b33-318c-4d7f-8860-7e0c06846560': {'sections': {
                                 '2249aaa1-7598-41ea-857f-be93c492a36e': {'sections': {
                                     'fd939acf-5d81-41d1-8e55-7bc736ec3847': {'sections': {},
                                                                              'entities': 'process_elements'}},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             'aae8f978-e202-4b9c-b85c-942518f68487': {'sections': {
                                 '150fcead-9479-4753-9e9b-4a6b2948a9c2': {'sections': {
                                     '3f64ce08-8891-48fa-8c41-3625ca9c9b39': {'sections': {},
                                                                              'entities': 'process_elements'}},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'},
                             '6b41a9eb-e986-4f74-9ae7-e7e9fdde2ec2': {'sections': {
                                 '416ad878-019a-497a-b780-824d8af4552c': {'sections': {},
                                                                          'entities': 'process_elements'}},
                                                                      'entities': 'process_elements'}},
                'entities': {}}},
               'element_sections': ['fcab1427-86d9-4bdb-84ab-34b546109232', '40d0445b-d011-4554-830b-9ed86ae9ad91',
                                    'd912afd0-c28e-4648-9e04-7f088d8c01ca', '67a0b8a3-86ce-46b6-aa82-1579f237f6af',
                                    '02ef2de8-a1a2-4e10-b265-34c3b4de8c80', 'c17dbb2c-2c7e-4b39-a150-3964ae10cc1c',
                                    '109b3331-e1bf-4d4d-97e5-9b6c651d10b4', 'bc99d33b-d6f9-44a6-ae81-077d69c7c4c3',
                                    'c0d87b70-5084-416c-abb6-6624a5387c00', '175d843d-29a8-40a3-bb25-19b0d5576f98',
                                    '02c26a10-a5ef-4225-9b67-56cfd22baabc', '5e886d4d-dc3b-4cac-8041-aa1e5aec3070',
                                    '355f8b59-7c71-4f18-b8ae-7829ead49888', '8f91811f-2c67-43bb-92d4-0d0d96d5c0c2',
                                    '64d3c6fa-27a2-4bc3-b766-55d8ccb7d53e', '83d6ad06-5cd7-4c01-a996-4019bc44af77',
                                    '9dcc560a-2716-4775-aa2f-7720de264f2a', '955a9ba6-68ab-4ffb-b4a2-b291be6109ed',
                                    'b063cc60-4395-4ed5-9c6c-4874d6d4a3b9', 'c8696202-aa3e-43ed-bc9b-0f0d4ded4dbc',
                                    '8cbfa7ee-f1c6-45c6-a68d-7a83ad96a0af', 'da37a3d4-3414-4467-b291-2c804555d25b',
                                    'cdccf547-c549-439d-9b00-fe43532c774b', '8ba7a8eb-5193-45f5-bc21-c2830564360b',
                                    'baa38b33-318c-4d7f-8860-7e0c06846560', 'fd939acf-5d81-41d1-8e55-7bc736ec3847',
                                    '2249aaa1-7598-41ea-857f-be93c492a36e', 'aae8f978-e202-4b9c-b85c-942518f68487',
                                    '3f64ce08-8891-48fa-8c41-3625ca9c9b39', '150fcead-9479-4753-9e9b-4a6b2948a9c2',
                                    '6b41a9eb-e986-4f74-9ae7-e7e9fdde2ec2', '416ad878-019a-497a-b780-824d8af4552c'],
               'failed_section_elements': [], 'failed_sections': [], 'doc_class_name': 'default_combined_domain_object',
               'enable_svo_linking': 'false', 'enable_similarity_matching': 'false',
               'enable_similarity_key_matching': 'false', 'attribute_confidence_threshold': 0.0,
               'remove_null_values': 'yes', 'remove_field_synonym': 'no', 'merge_entity_groups': 'no',
               'negex_preceeding_patterens': '', 'negex_following_patterens': '', 'enable_spell_correction': 'false'}

    pt = ProcessedText()
    pt.run(payload=payload, raw_ent=raw_ent)
