def append_attribute(config=None, **kwargs):
    elements = []
    kwargs_d = kwargs[0] if isinstance(kwargs, list) else kwargs
    if 'domain' in kwargs_d:
        domain_obj = kwargs_d["domain"][0]
        doc_id = kwargs_d["domain"][0]["doc_id"]
        for domain in domain_obj["children"]:
            domain_name = domain["name"]
            for entity in domain["children"]:
                entity_name = entity["name"]
                insight_id = entity["insight_id"]
                for attribute in entity["children"]:
                    el_dict = {
                        "att_name": attribute["name"],
                        "entity_name": entity_name,
                        "element_id": attribute["element_id"],
                        "insight_id": insight_id,
                        "value": attribute["value"]

                    }
                    elements.append(el_dict)

            if elements:
                append_payload = {
                    "doc_id": doc_id,
                    "entities": [
                        {
                            "entity_type": "domain_object",
                            "entity_name": domain_name,
                            "children": [
                                {
                                    "entity_name": elements[0]["entity_name"],
                                    "entity_type": "entity",
                                    "attributes": [
                                        {
                                            "name": elements[0]["att_name"],
                                            "value": "123456",
                                            "confidence": 0.8,
                                            "element_id": elements[0]["element_id"]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
                if isinstance("kwargs", list):
                    kwargs.append(append_payload)
                else:
                    kwargs.update(append_payload)
    return kwargs

