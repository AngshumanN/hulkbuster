from uuid import uuid4


def upsert_domain_function(config=None, **kwargs):
    elements = []
    kwargs_d = kwargs[0] if isinstance(kwargs, list) else kwargs
    if 'domain' in kwargs_d:
        doc_id = kwargs_d["domain"][0]["doc_id"]
        domain_obj = kwargs_d["domain"][0]
        domain_insight_id = domain_obj["insight_id"]
        for domain in domain_obj["children"]:
            for entity in domain["children"]:
                entity_name = entity["name"]
                insight_id = entity["insight_id"]
                for attribute in entity["children"]:
                    el_dict = {
                        "att_name": attribute["name"],
                        "entity_name": entity_name,
                        "element_id": attribute["element_id"],
                        "insight_id": insight_id,
                        "value": attribute["value"]

                    }
                    elements.append(el_dict)

        if elements:
            upsert_payload = [
                {
                    "action": "upsert",
                    "data": {
                        "entity_name": elements[0]["entity_name"],
                        "attributes": {
                            elements[0]["att_name"]: [
                                [
                                    "This is for updating the existing attribute",
                                    elements[0]["element_id"]
                                ]
                            ]
                        }
                    },
                    "insight_id": elements[0]["insight_id"]
                },
                {
                    "action": "delete",
                    "data": {
                        "entity_name": elements[1]["entity_name"],
                        "attributes": {
                            elements[1]["att_name"]: [
                                [
                                    elements[1]["value"],
                                    elements[1]["element_id"]
                                ]
                            ]
                        }
                    },
                    "insight_id": elements[1]["insight_id"]

                },
                {
                    "action": "upsert",
                    "data": {
                        "entity_name": elements[0]["entity_name"],
                        "attributes": {
                            elements[0]["att_name"]: [
                                [
                                    "This is for creating  a new attribute",
                                    str(uuid4())
                                ]
                            ]
                        }
                    },
                    "parent_insight_id": domain_insight_id
                }
            ]
            if isinstance(kwargs, list):
                kwargs = kwargs[0]
            kwargs.update({"update_data": upsert_payload})
    return kwargs





# sample_payload_output
update_data_payload = [
    {
        "action": "upsert",
        "data": {
            "entity_name": "skills",
            "attributes": {
                "project_methodologies": [
                    [
                        "python",
                        "fed9ec5d-7a8f-4d59-bd33-92fdd27f2924"
                    ]
                ]
            }
        },
        "insight_id": "a1704c04-9ef5-40be-b2a7-d0d05d8f9628"
    },
    {
        "action": "delete",
        "insight_id": "fd0904fa-49b0-420e-a3b4-758bed105ae3"
    },
    {
        "action": "upsert",
        "data": {
            "entity_name": "contact_details",
            "attributes": {
                "address": [
                    [
                        "vikash",
                        "df62461f-7f58-4c4e-88ad-433761a67c95"
                    ]
                ]
            }
        },
        "insight_id": "bd6bc1e9-b3c3-42ce-9442-73acbb321d8f"
    },
    {
        "action": "delete",
        "insight_id": "d0ba6c94-773a-45f6-82f7-c6762e6df69a"
    },
    {
        "action": "delete",
        "data": {
            "attributes": {
                "resume_experience": [

                ]
            }
        },
        "insight_id": "183fd015-4735-49ae-8405-4ceac2409861"
    },
    {
        "action": "delete",
        "data": {
            "attributes": {
                "year_of_joining": [
                    [
                        "Feb 2014",
                        "df62461f-7f58-4c4e-88ad-433761a67c95"
                    ]
                ]
            }
        },
        "insight_id": "4075dfe8-3929-43a7-9b37-c358f6d23e26"
    },
    {
        "action": "upsert",
        "data": {
            "entity_name": "education",
            "attributes": {
                "year_of_passing": [
                    [
                        "Feb 2012",
                        "df62461f-7f58-4c4e-88ad-433761a67c95"
                    ]
                ],
                "location_institute": [
                    [
                        "pune",
                        "df62461f-7f58-4c4e-88ad-433761a67c95"
                    ]
                ]
            }
        },
        "insight_id": "4075dfe8-3929-43a7-9b37-c358f6d23e26"
    },
    {
        "action": "delete",
        "insight_id": "7ef3ebb6-d1dc-40b5-b940-69758c0f214d"
    }
]
