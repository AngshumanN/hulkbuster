from xpms_file_storage.file_handler import XpmsResource, LocalResource

csv_minio_urn = "minio://sample/path/to/csv.csv"
local_csv_path = "/tmp/local_csv_path.csv"
minio_resource = XpmsResource.get(urn=csv_minio_urn)
local_res = LocalResource(key=local_csv_path)
minio_resource.copy(local_res)