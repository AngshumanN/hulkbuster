from xpms_objects.models import configuration

service_name = "sample service"
service_desc = "sample service description"
service_path = "sample_service"


svc_config = configuration.BooleanConfiguration()
svc_config.name = service_name
svc_config.description = service_desc
svc_config.multi_select = False
svc_config.data_type = configuration.DataType.BOOLEAN.value
svc_config.config_path = "config.data.configuration.{0}".format(service_path)
print(svc_config.as_json())