nlp_output = {'entity_list': [{'num': 0, 'sent_num': 0, 'begin': 0, 'end': 3, 'tok_begin': 0, 'tok_end': 0, 'text': 'PSP', 'attributes': [], 'relevance': 0.0, 'ent_type': [['ORG', 0.8, 'named_entity'], ['psp.entity', 0.8, 'synonym']], 'confidence': 0.0, 'value': None}, {'num': 2, 'sent_num': 0, 'begin': 0, 'end': 20, 'tok_begin': 0, 'tok_end': 3, 'text': 'PSP NameNumber or MR', 'attributes': [], 'relevance': 0.0, 'ent_type': [['person.psp_name', 0.312499985063808, 'fuzzy_synonym']], 'confidence': 0.4}, {'num': 5, 'sent_num': 0, 'begin': 0, 'end': 14, 'tok_begin': 0, 'tok_end': 1, 'text': 'PSP NameNumber', 'attributes': [], 'relevance': 0.0, 'ent_type': [['NP', 0.5, 'NP']], 'confidence': 0.4}, {'num': 3, 'sent_num': 0, 'begin': 15, 'end': 17, 'tok_begin': 2, 'tok_end': 2, 'text': 'or', 'attributes': [], 'relevance': 0.0, 'ent_type': [['patient_information.patient_name', 0.05263157942881706, 'fuzzy_synonym'], ['medical_history.medical_history_risk_factors', 0.06250000135783565, 'fuzzy_synonym']], 'confidence': 0.4}, {'num': 6, 'sent_num': 0, 'begin': 18, 'end': 29, 'tok_begin': 3, 'tok_end': 4, 'text': 'MR Services', 'attributes': [], 'relevance': 0.0, 'ent_type': [['NP', 0.5, 'NP']], 'confidence': 0.4}, {'num': 4, 'sent_num': 0, 'begin': 21, 'end': 29, 'tok_begin': 4, 'tok_end': 4, 'text': 'Services', 'attributes': [], 'relevance': 0.0, 'ent_type': [['person.service_provider_name', 0.06250000135783565, 'fuzzy_synonym'], ['provider_details.secondary_specialty', 0.06250000135783565, 'fuzzy_synonym'], ['person.date_event_reported', 0.06250000135783565, 'fuzzy_synonym'], ['provider_details.primary_specialty', 0.06250000135783565, 'synonym']], 'confidence': 0.4, 'proximity_np': [['PSP NameNumber', 3]]}], 'ontology_list': [], 'context': {'svo': [], 'spo': [], 'svp': [], 'pps': [], 'noun_chunks': ['PSP NameNumber', 'MR Services']}, 'noun_list': []}
element = {'section_id': 'a70df990-cd17-4394-b0bb-3a492042ba63', 'doc_id': '2cb65716-316c-41c3-808c-96e608b51fe4', 'id': '6676b089-0a0d-4464-946d-c243d4c521af', 'type': 'sentence', 'text': ['PSP NameNumber or MR Services are goofy'], 'tokenize': True, 'element_confidence': 0.8, 'error': [''], 'entities': [{'entity_list': [{'num': 0, 'sent_num': 0, 'begin': 0, 'end': 3, 'tok_begin': 0, 'tok_end': 0, 'text': 'PSP', 'attributes': [], 'relevance': 0.0, 'ent_type': [['ORG', 0.8, 'named_entity'], ['psp.entity', 0.8, 'synonym']], 'confidence': 0.0, 'value': None}, {'num': 2, 'sent_num': 0, 'begin': 0, 'end': 20, 'tok_begin': 0, 'tok_end': 3, 'text': 'PSP NameNumber or MR', 'attributes': [], 'relevance': 0.0, 'ent_type': [['person.psp_name', 0.312499985063808, 'fuzzy_synonym']], 'confidence': 0.4}, {'num': 5, 'sent_num': 0, 'begin': 0, 'end': 14, 'tok_begin': 0, 'tok_end': 1, 'text': 'PSP NameNumber', 'attributes': [], 'relevance': 0.0, 'ent_type': [['NP', 0.5, 'NP']], 'confidence': 0.4}, {'num': 3, 'sent_num': 0, 'begin': 15, 'end': 17, 'tok_begin': 2, 'tok_end': 2, 'text': 'or', 'attributes': [], 'relevance': 0.0, 'ent_type': [['patient_information.patient_name', 0.05263157942881706, 'fuzzy_synonym'], ['medical_history.medical_history_risk_factors', 0.06250000135783565, 'fuzzy_synonym']], 'confidence': 0.4}, {'num': 6, 'sent_num': 0, 'begin': 18, 'end': 29, 'tok_begin': 3, 'tok_end': 4, 'text': 'MR Services', 'attributes': [], 'relevance': 0.0, 'ent_type': [['NP', 0.5, 'NP']], 'confidence': 0.4}, {'num': 4, 'sent_num': 0, 'begin': 21, 'end': 29, 'tok_begin': 4, 'tok_end': 4, 'text': 'Services', 'attributes': [], 'relevance': 0.0, 'ent_type': [['person.service_provider_name', 0.06250000135783565, 'synonym'], ['provider_details.secondary_specialty', 0.06250000135783565, 'fuzzy_synonym'], ['person.date_event_reported', 0.06250000135783565, 'synonym'], ['provider_details.primary_specialty', 0.06250000135783565, 'synonym']], 'confidence': 0.4, 'proximity_np': [['PSP NameNumber', 3]]}], 'ontology_list': [], 'context': {'svo': [], 'spo': [], 'svp': [], 'pps': [], 'noun_chunks': ['PSP NameNumber', 'MR Services']}, 'noun_list': []}], 'probable_entities': [], 'link_entities_error': []}

def get_tagged_details(text, tagged_list):
    for tagged in tagged_list:
        if tagged["text"] == text:
            return tagged
    return None

def processs_nlp_text(nlp_output, element):
    element_text = element["text"][0]
    extracted_data = []
    tagged_list = []
    entity_list = nlp_output["entity_list"]
    for entity in entity_list:
        entity_text = entity["text"]
        ent_type = entity["ent_type"]
        for type in ent_type:
            if len(type[0].split('.')) == 2:
                entity_name, attribute = type[0].split('.')
                if attribute != "entity":
                    if type[2] != "fuzzy_synonym":
                        tagged_list.append({"attribute": type[0], "text": entity_text, "conf":type[1], "source": type[2]})

    if tagged_list:
        tagged_text_list = [a["text"] for a in tagged_list]
        for i, n in enumerate(tagged_text_list):
            if n in element_text:
                tagged_text_list[i] = (n, element_text.index(n))
            else:
                tagged_text_list.remove(n)
        sorted(tagged_text_list, key=lambda x: x[1])
        for index, item in enumerate(tagged_text_list):
            if item != tagged_text_list[-1]:
                start = element_text.index(tagged_text_list[index][0])
                end = element_text.index(tagged_text_list[index + 1][0])
                result_text = element_text[start:end]
            else:
                start = element_text.index(tagged_text_list[index][0])
                result_text = element_text[start:len(element_text)]

            tagged_dict = get_tagged_details(tagged_text_list[index][0], tagged_list)
            extracted_dict = {
                "entity_candidates": [tagged_dict["attribute"]],
                "entity_value": result_text,
                "entity_score": tagged_dict["conf"],
                "entity_source": tagged_dict["source"]
            }
            extracted_data.append(extracted_dict)
    return extracted_data


extracted_data = processs_nlp_text(nlp_output, element)
print(extracted_data)