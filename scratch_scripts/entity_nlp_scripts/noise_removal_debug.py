# this needs to be run in entity

from microservice.entity_raw import get_entity_raw, remove_noise_from_linking
from microservice.entity_util import get_xpms_domain_objects

solution_id = 'smoketestbc964dd0'
request_id = "02570a5a-df76-49bc-812b-22f25b07ef31"
doc_id = '1e883932-8fc0-494f-9c95-ff861c15a275'
doc_filter = {'solution_id': solution_id, 'entity_name': 'document', 'entity_id': doc_id}
doc = get_entity_raw(doc_filter, return_raw_data=True)
if doc is not None and len(doc) > 0:
  doc[0]['root_id'] = "1e883932-8fc0-494f-9c95-ff861c15a275"
  doc[0]['doc_id'] = "1e883932-8fc0-494f-9c95-ff861c15a275"
  doc = get_xpms_domain_objects({}, doc, solution_id)
  domain_name = 'resume'
  remove_noise_from_linking(solution_id, request_id, doc, domain_name)
  print(doc)