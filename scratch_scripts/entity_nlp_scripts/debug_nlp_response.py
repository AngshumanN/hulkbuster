#this script should be run in NLP container

from jrules_lib.rule_manager import RuleManager

import json
ruleManager = RuleManager()

solution_id = "sample_sol_id"
text_list = ["sample", "text", "list"]
service_name = "nlp"

data = {
        "solution_id": solution_id,
        "source": text_list,
        "service_name": service_name
        }

result = ruleManager.process("executeRulesByService", data)
json.dumps(result)
