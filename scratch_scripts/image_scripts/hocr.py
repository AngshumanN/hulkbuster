import copy
import random
from collections import OrderedDict, namedtuple

from bs4 import BeautifulSoup
from xpms_file_storage.file_handler import XpmsResourceFactory, LocalResource


def get_ocr_words(png_urn: str, hocr_urn: str, metadata={}):
    from datetime import datetime
    ocr_words = []
    try:


        # Copying hocr to local resource
        hocr_src = XpmsResourceFactory.create_resource(key=hocr_urn)
        hocr_lcl = LocalResource(rel_path=hocr_src.key)
        hocr_lcl.mkdir(parent=True)
        if hocr_src.storage != hocr_lcl.storage:
            hocr_src.copy(hocr_lcl)
        encoding = 'latin1'
        with open(hocr_lcl.fullpath, mode="r", encoding=encoding) as f:
            html = f.read()
        soup = BeautifulSoup(html, "html.parser")
        spans = (soup.find_all('span', attrs={'class': 'ocrx_word'}))
        word_id = 0
        for span in spans:
            bbox = list(map(int, span['title'].split(';')[0].replace('bbox', '').strip().split()))
            score = (int(span['title'].split(';')[1].replace('x_wconf', '').strip()))
            if (len(span.text.strip()) <= 0) or len(bbox) != 4:
                continue
            pw_list = get_possible_words_after_seperators(bbox, span.text)
            for pw in pw_list:
                word_id += 1
            bbox = pw[0]
            pw_text = pw[1]
            this_word = dict(x1=bbox[0], y1=bbox[1], x2=bbox[2], y2=bbox[3], text=pw_text, confidence=score)
            this_word["word_id"] = word_id
            this_word["line_num"] = 0
            if score == 75:
                this_word["confidence"] = random.randint(75, 100)
            else:
                this_word["confidence"] = score
            ocr_words.append(this_word)
    except Exception as e:
    # todo log
        print(e)
    return ocr_words
Rectangle = namedtuple('Rectangle', 'xmin ymin xmax ymax')

def area(a, b):  # returns None if rectangles don't intersect
    dx = min(a.xmax, b.xmax) - max(a.xmin, b.xmin)
    dy = min(a.ymax, b.ymax) - max(a.ymin, b.ymin)
    if (dx >= 0) and (dy >= 0):
        return dx * dy
    else:
        return 0

def check_word(words:list, anotate_cords:dict):
    phrase = []
    for word in words:
        word_cord = [word["x1"],word["y1"],word["x2"],word["y2"]]
        AREA = abs(word_cord[2] - word_cord[0]) * abs(word_cord[3] - word_cord[1])
        anotate_rect = Rectangle(anotate_cords["x1"], anotate_cords["y1"], anotate_cords["x2"], anotate_cords["y2"])
        word_rect = Rectangle(word_cord[0], word_cord[1], word_cord[2], word_cord[3])
        intersect_area = (area(anotate_rect, word_rect) / AREA)
        if intersect_area > 0.5:
            phrase.append(word)
    return phrase


def get_possible_words_after_seperators(bbox, text, SEPARATORS=";#?:-"):
    pw_list = []
    mtext = text
    for s in SEPARATORS:
        mtext = mtext.replace(s, s + " ")
    words_list = mtext.split()
    bbox_width = bbox[2] - bbox[0]
    avg_char_width = int(1.0 * bbox_width / len(text))
    previous_x = bbox[0]
    for w in words_list:
        wbbox = copy.deepcopy(bbox)
        wbbox[0] = previous_x
        wbbox[2] = wbbox[0] + len(w) * avg_char_width
        previous_x = wbbox[2]
        pw_list.append((wbbox, w))
    return pw_list

ocr_words = get_ocr_words("enso1376/documents/0e9828cf-4731-460d-b5db-40d82b5b617f/pages/energy_portfolios-1.png", "enso1376/documents/0e9828cf-4731-460d-b5db-40d82b5b617f/pages/col_17_energy_portfolios-1.hocr")


y1 = int(265.853658536585)
x1 = int(1319.51219512195)
x2 = int (1319.51219512195 + 656.09756097561)
y2 = int(265.853658536585 + 465.853658536585)

payload = {"x1":x1,"y1":y1,"x2":x2,"y2":y2}

phrase = check_word(ocr_words,payload)
text = " ".join([p["text"] for p in phrase])