import json
import traceback
import requests
from xpms_common.errors import ElasticSearchError
from xpms_file_storage.file_handler import XpmsResourceFactory, LocalResource
from uuid import uuid4
import os

ELASTIC_BASE_URL = "http://vpc-xp-poc-d2x2xemfvgrrvf272kmixyir5m.us-east-1.es.amazonaws.com"


def create_query_string(vocabulary_type, tags=None):
    query = {"query": {"bool": {"must": [{"match": {"type": vocabulary_type}}]}}}
    if tags:
        query["query"]["bool"]["must"].append({"terms": {"tag": tags}})
    return query


def elastic_data(request_indices, query, size, from_index):
    base_url = ELASTIC_BASE_URL
    request_string = "{0}/{1}/_search?size={2}&&from={3}&&pretty=true".format(base_url, request_indices, size, from_index)
    response = requests.post(url=request_string, json=query)
    result_dict = response.json()
    return result_dict


def export_data_from_elastic(solution_id, export_path, vocabulary_type, tag=None):
    try:
        index_prefix = os.environ['NAMESPACE']
        index = str(solution_id).split("_")[0]
        indices_list = ['data', 'fuzzy', 'exact']
        restore_dict = {}
        query = create_query_string(vocabulary_type, tag)
        local_folder_path = "/tmp/" + str(uuid4())
        local_folder = LocalResource(key=local_folder_path)
        for indices in indices_list:
            request_indices = "{0}_{1}_{2}".format(index_prefix, index, indices)
            file_name = request_indices
            from_index = 0
            size = 1000
            result_dict = elastic_data(request_indices, query, size, from_index)
            if result_dict['hits']['hits']:
                source_ar = []
                for hits in result_dict['hits']['hits']:
                    source_ar.append(hits.get("_source"))
                restore_dict.update({request_indices: source_ar})
                local_file_path = "{0}/{1}.json".format(local_folder_path, file_name)
                local_file = LocalResource(key=local_file_path)
                local_file.mkdir(parent=True)
                with local_file.open(mode="w") as f:
                    for r in restore_dict[request_indices]:
                        f.write(json.dumps({"index":{}})+"\n")
                        f.write(json.dumps(r)+"\n")

            vocab_folder = XpmsResourceFactory.create_resource(urn="{0}/{1}/{2}".format(export_path, "vocabulary", vocabulary_type))
            local_folder.copytree(vocab_folder)
            return vocab_folder.urn

    except Exception as e:
        raise ElasticSearchError('Failed in processing create create_query_string ',
                                 traceback=traceback.format_exc())


def fetch_dictionary_corpus_data(solution_id, type):
    index_prefix = os.environ['NAMESPACE']
    index = str(solution_id).split("_")[0]
    indices_list = ['data', 'fuzzy', 'exact']
    asset_list = []
    tags_done = []
    try:
        query = create_query_string(type)
        for indices in indices_list:
            request_indices = "{0}_{1}_{2}".format(index_prefix, index, indices)
            from_index = 0
            size = 1000
            result_dict = elastic_data(request_indices, query, size, from_index)
            if result_dict['hits']['hits']:
                for hit in result_dict['hits']['hits']:
                    if hit["_source"]["tag"] not in tags_done:
                        asset_list.append({"name": hit["_source"]["tag"], "id": hit["_source"]["id"]})
                        tags_done.append(hit["_source"]["tag"])
    except:
        asset_list = []
    finally:
        return asset_list


def import_from_elastic(import_path, vocabulary_type, solution_id):
        index_prefix = os.environ['NAMESPACE']
        index = str(solution_id).split("_")[0]
        base_url = ELASTIC_BASE_URL
        response_json = []
        updated_import_path = "{0}/{1}/{2}".format(import_path, "vocabulary", vocabulary_type)
        minio_resource = XpmsResourceFactory.create_resource(urn=updated_import_path)
        local_file = LocalResource(key="/tmp/{0}".format(uuid4()))
        minio_resource.copytree(local_file)
        headers = {'Content-Type': 'application/x-ndjson'}
        files_list = local_file.list()
        for file in files_list:
            index_postfix = str(file.filename_without_extension).split("_")[-1]
            upload_index = "{0}_{1}_{2}".format(index_prefix,index,index_postfix)
            data = open(file.key, 'rb').read()
            url = "{0}/{1}/doc/_bulk".format(base_url, upload_index)
            r = requests.post(url, data=data, headers=headers)
            response_json.append(r.json())
        return True


def import_dictionary_corpus(assets_json, import_path, solution_id):
    dictionary_list = []
    corpus_list = []
    dictionary_selection_status = "none"
    corpus_selection_status = "none"
    dictionary_is_selected = False
    corpus_is_selected = False
    dictionary_service = {}
    corpus_service = {}

    if assets_json is not None:
        all_asset_is_selected = assets_json["isSelected"]
        all_asset_selection_status = assets_json["selectionStatus"]
        for service in assets_json["children"]:
            if service["name"] == "Dictionary":
                dictionary_service = service
                dictionary_selection_status = dictionary_service["selectionStatus"]
                dictionary_is_selected = dictionary_service["isSelected"]
            if service["name"] == "Corpus":
                corpus_service = service
                corpus_selection_status = corpus_service["selectionStatus"]
                corpus_is_selected = corpus_service["isSelected"]

        if all_asset_selection_status not in ['all', 'enabled', 'default']:
            if dictionary_is_selected:
                if dictionary_selection_status in ['all', 'enabled', 'default']:
                    for children in dictionary_service["children"]:
                        dictionary_list.append(children["name"])
                else:
                    for children in dictionary_service["children"]:
                        if children["selectionStatus"] != "none" and children["isSelected"]:
                            dictionary_list.append(children["name"])

            if corpus_is_selected:
                if corpus_selection_status in ['all', 'enabled', 'default']:
                    for children in corpus_service["children"]:
                        corpus_list.append(children["name"])
                else:
                    for children in corpus_service["children"]:
                        if children["selectionStatus"] != "none" and children["isSelected"]:
                            corpus_list.append(children["name"])
        else:
            if dictionary_is_selected:
                for children in dictionary_service["children"]:
                    dictionary_list.append(children["name"])
            if corpus_is_selected:
                for children in corpus_service["children"]:
                    corpus_list.append(children["name"])
        if dictionary_list:
            import_from_elastic(import_path, "dictionary", solution_id )
        if corpus_list:
            import_from_elastic(import_path, "corpus", solution_id)
    return True


def export_dictionary_corpus(solution_id, assets_json, export_path):
    dictionary_list = []
    corpus_list = []
    dictionary_selection_status = "none"
    corpus_selection_status = "none"
    dictionary_is_selected = False
    corpus_is_selected = False
    dictionary_service = {}
    corpus_service = {}

    if assets_json is not None:
        all_asset_is_selected = assets_json["isSelected"]
        all_asset_selection_status = assets_json["selectionStatus"]
        for service in assets_json["children"]:
            if service["name"] == "Dictionary":
                dictionary_service = service
                dictionary_selection_status = dictionary_service["selectionStatus"]
                dictionary_is_selected = dictionary_service["isSelected"]
            if service["name"] == "Corpus":
                corpus_service = service
                corpus_selection_status = corpus_service["selectionStatus"]
                corpus_is_selected = corpus_service["isSelected"]

        if all_asset_selection_status not in ['all', 'enabled', 'default']:
            if dictionary_is_selected:
                if dictionary_selection_status in ['all', 'enabled', 'default']:
                    for children in dictionary_service["children"]:
                        dictionary_list.append(children["name"])
                else:
                    for children in dictionary_service["children"]:
                        if children["selectionStatus"] != "none" and children["isSelected"]:
                            dictionary_list.append(children["name"])

            if corpus_is_selected:
                if corpus_selection_status in ['all', 'enabled', 'default']:
                    for children in corpus_service["children"]:
                        corpus_list.append(children["name"])
                else:
                    for children in corpus_service["children"]:
                        if children["selectionStatus"] != "none" and children["isSelected"]:
                            corpus_list.append(children["name"])
        else:
            if dictionary_is_selected:
                for children in dictionary_service["children"]:
                    dictionary_list.append(children["name"])
            if corpus_is_selected:
                for children in corpus_service["children"]:
                    corpus_list.append(children["name"])
        if dictionary_list:
            export_data_from_elastic(solution_id, export_path, "dictionary", dictionary_list)
        if corpus_list:
            export_data_from_elastic(solution_id, export_path, "corpus", corpus_list)
    return True

