from datetime import datetime

task_from = {}
task_to = {}
from_id = "extraction"
to_id = "linking"
for dic in inp:
    if dic["task_id"] == from_id:
        task_from.update({dic["ref_id"]: dic["time_stamp"]})
    if dic["task_id"] == to_id:
        task_to.update({dic["ref_id"]: dic["time_stamp"]})
diff = {}

for ref, times in task_from.items():
    if ref in task_to:
        difftime = (datetime.strptime(task_to[ref], '%d/%m/%y %H:%M:%S') - datetime.strptime(times, '%d/%m/%y %H:%M:%S')).total_seconds()
        diff.update({ref: difftime})
count = 0
sum = 0
for ref,time in diff.items():
    sum = sum + time
    count = count + 1
avg = sum/count
print(avg)
