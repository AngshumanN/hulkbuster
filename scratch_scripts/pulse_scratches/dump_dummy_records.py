import random
from datetime import datetime, timedelta
from uuid import uuid4

from microservice.models.execution.executions import ExecutionObject

ref_id_list = []
process_uid = "eab6502d-1967-4ac5-93b6-849e9b8b6c5d"
all_records_to_dump = []

node_types = ["paragraph", "sentence", "table", "field"]
attributes = ["age", "name", "contact_no", "skills", "email", "dob"]
tasks = ["claim_intitated", "claim_validation", "claim_approval", "claim_rejected", "claim_settlement"]

for i in range(1, 150, 1):
    ref_id_list.append(str(uuid4()))

initial_date = datetime.now() - timedelta(weeks=52)

approver = ["Berry Allen", "Eobard Thawne", "Joe West", "Iris West"]

for index, ref in enumerate(ref_id_list):
    new_date = initial_date + timedelta(days=index + 10)
    log_dict = {
        'time_stamp': new_date,
        "process_uid": process_uid,
        "task_num": "1",
        "task_name": "claim_intitated",
        "status": "entry",
        "ref_id": ref,
        "event_data": [{
            "node_type": random.choice(node_types),
            "conf_score": 10 + index
        },
            {
                "attribute_name": random.choice(attributes),
                "conf_score": 5 + index
            }],
        "decision_data": [
            {
                "system_accuracy": 14 + index,
                "feedback_accuracy": 17 + index
            }
        ]
    }
    all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 120:
        new_date = initial_date + timedelta(days=index + 11)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "1",
            "task_name": "claim_intitated",
            "status": "exit",
            "ref_id": ref,
            "event_data": [{
                "node_type": random.choice(node_types),
                "conf_score": 10 + index
            },
                {
                    "attribute_name": random.choice(attributes),
                    "conf_score": 5 + index
                }],
            "decision_data": [
                {
                    "system_accuracy": 14 + index,
                    "feedback_accuracy": 17 + index
                }
            ]
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 100:
        new_date = initial_date + timedelta(days=index + 12)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2",
            "task_name": "claim_validation",
            "status": "entry",
            "ref_id": ref,
            "event_data": [{
                "node_type": random.choice(node_types),
                "conf_score": 10 + index
            }],
            "decision_data": [
                {
                    "system_accuracy": 14 + index,
                    "feedback_accuracy": 17 + index
                }
            ]
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 90:
        new_date = initial_date + timedelta(days=index + 13)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2",
            "task_name": "claim_validation",
            "status": "exit",
            "ref_id": ref,
            "event_data": [
                {
                    "attribute_name": random.choice(attributes),
                    "conf_score": 5 + index
                }]
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 80:
        new_date = initial_date + timedelta(days=index + 14)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2.1",
            "task_name": "claim_approval",
            "status": "entry",
            "ref_id": ref,
            "event_data": [{
                "node_type": random.choice(node_types),
                "conf_score": 10 + index
            },
                {
                    "attribute_name": random.choice(attributes),
                    "conf_score": 5 + index
                }],
            "decision_data": [
                {
                    "system_accuracy": 14 + index,
                    "feedback_accuracy": 17 + index
                }
            ]
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 70:
        new_date = initial_date + timedelta(days=index + 15)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2.1",
            "task_name": "claim_approval",
            "status": "exit",
            "ref_id": ref,
            "event_data": [{
                "node_type": random.choice(node_types),
                "conf_score": 10 + index
            }],
            "decision_data": [
                {
                    "system_accuracy": 14 + index,
                    "feedback_accuracy": 17 + index
                }
            ]
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 30:
        new_date = initial_date + timedelta(days=index + 16)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2.2",
            "task_name": "claim_rejected",
            "status": "entry",
            "ref_id": ref,
            "event_data":
                {
                    "attribute_name": random.choice(attributes),
                    "conf_score": 5 + index
                }
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 30:
        new_date = initial_date + timedelta(days=index + 17)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2.2",
            "task_name": "claim_rejected",
            "status": "exit",
            "ref_id": ref,
            "event_data": [{
                "node_type": random.choice(node_types),
                "conf_score": 10 + index
            },
                {
                    "attribute_name": random.choice(attributes),
                    "conf_score": 5 + index
                }],
            "decision_data": [
                {
                    "system_accuracy": 14 + index,
                    "feedback_accuracy": 17 + index
                }
            ]
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 30:
        new_date = initial_date + timedelta(days=index + 18)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2.1.1",
            "task_name": "claim_settlement",
            "status": "entry",
            "ref_id": ref,
            "event_data": [{
                "node_type": random.choice(node_types),
                "conf_score": 10 + index
            },
                {
                    "attribute_name": random.choice(attributes),
                    "conf_score": 5 + index
                }],
            "decision_data": [
                {
                    "system_accuracy": 14 + index,
                    "feedback_accuracy": 17 + index
                }
            ]
        }
        all_records_to_dump.append(log_dict)

for index, ref in enumerate(ref_id_list):
    if index <= 20:
        new_date = initial_date + timedelta(days=index + 19)
        log_dict = {
            'time_stamp': new_date,
            "process_uid": process_uid,
            "task_num": "2.1.1",
            "task_name": "claim_settlement",
            "status": "exit",
            "ref_id": ref,
            "event_data": [{
                "node_type": random.choice(node_types),
                "conf_score": 10 + index
            },
                {
                    "attribute_name": random.choice(attributes),
                    "conf_score": 5 + index
                }],
            "decision_data": [
                {
                    "system_accuracy": 14 + index,
                    "feedback_accuracy": 17 + index
                }
            ]
        }
        all_records_to_dump.append(log_dict)

print(all_records_to_dump)
execution = ExecutionObject()
execution.save_executions(all_records_to_dump)
# cols = all_records_to_dump[0].keys()
# rows = convert_rows_to_records(all_records_to_dump)
# db_con.insert_batch_data(, final_list_event_columns, event_rows)
