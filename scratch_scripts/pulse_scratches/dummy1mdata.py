import random
from uuid import uuid4

import requests

decision_extraction_data = []
decision_entity_data = []
node_types = ["paragraph", "sentence", "heading", "table", "list"]
att_names = ["age", "email", "name", "phone", "languages_known"]
confidence = [10, 20, 25, 30, 40, 45, 50, 60, 65, 70, 80, 85, 90, 95]
s_type = ["system","user"]
for i in range(1, 100000):
    ext_json = {
        "node_type": random.choice(node_types),
        "confidence": random.choice(confidence),
        "insight_id": str(uuid4()),
        "type": random.choice(s_type)
    }
    decision_extraction_data.append(ext_json)
    ent_json = {
        "attribute_name": random.choice(att_names),
        "attribute_value": random.choice(att_names),
        "confidence": random.choice(confidence),
        "insight_id": str(uuid4()),
        "type": random.choice(s_type)
    }
    decision_entity_data.append(ent_json)

body = [
    {
        "time_stamp": "01/09/20 09:47:05",
        "process_uid": "04bd9f1c-6f84-40ee-b61b-1d66d8a1076b",
        "task_id": "p1",
        "task_name": "document_ingestion",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:47:40",
        "process_uid": "04bd9f1c-6f84-40ee-b61b-1d66d8a1076b",
        "task_id": "p2",
        "task_name": "document_classification",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:52:42",
        "process_uid": "04bd9f1c-6f84-40ee-b61b-1d66d8a1076b",
        "task_id": "p3",
        "task_name": "text_extraction",
        "status": "exit_success",
        "ref_id": "ref10",
        "decision_data": decision_extraction_data,
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:54:43",
        "process_uid": "04bd9f1c-6f84-40ee-b61b-1d66d8a1076b",
        "task_id": "p3_1",
        "task_name": "entity_linking",
        "status": "exit_success",
        "ref_id": "ref10",
        "decision_data": decision_entity_data,
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:54:53",
        "process_uid": "04bd9f1c-6f84-40ee-b61b-1d66d8a1076b",
        "task_id": "p4",
        "task_name": "end-task",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    }
]

pulse_url = "https://pulse-mvp.dev1.nextgen.xpms.ai/{endpoint}".format(endpoint="save_execution_logs")
r = requests.post(url=pulse_url, json=body)