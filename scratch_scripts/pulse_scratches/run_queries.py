from datetime import datetime

from microservice.db_handlers.db_handler import PulseDBHandler

query_string = """
select avg((e.data->>'conf_score')::numeric) AS avg_conf_score from event_data as e,
execution_log as el where el.exec_id=e.exec_id and (e.data->> 'conf_score')::numeric >
(select avg((data->>'conf_score')::numeric) from event_data);
            """

db = PulseDBHandler()
print("before_time : " + str(datetime.now()))
response = db.execute_raw_sql(query_string)
print(response)
print("after_time : " + str(datetime.now()))



""" select avg((e.data->>'conf_score')::numeric) AS avg_conf_score from event_data as e,
execution_log as el where el.exec_id=e.exec_id and (e.data->> 'conf_score')::numeric >
(select avg((data->>'conf_score')::numeric) from event_data); """