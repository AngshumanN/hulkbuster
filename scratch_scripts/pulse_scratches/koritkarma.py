[
    {
        "time_stamp": "01/09/20 09:47:05",
        "process_uid": "demo702",
        "task_id": "p1",
        "task_name": "document_ingestion",
        "status": "exit_success",
        "ref_id": "ref11",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:47:40",
        "process_uid": "demo702",
        "task_id": "p2",
        "task_name": "document_classification",
        "status": "exit_success",
        "ref_id": "ref11",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:52:42",
        "process_uid": "demo702",
        "task_id": "p3",
        "task_name": "text_extraction",
        "status": "exit_success",
        "ref_id": "ref11",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:54:43",
        "process_uid": "demo702",
        "task_id": "p3_2",
        "task_name": "entity_linking_2",
        "status": "exit_success",
        "ref_id": "ref11",
        "decision_data":[
  {
    "attribute_name": "name",
    "attribute_value": "abcd",
    "confidence": "65",
    "insight_id": "1234",
    "type": "system"
  },
  {
    "attribute_name": "age",
    "attribute_value": "23",
    "confidence": "67",
    "insight_id": "1254",
    "type": "user"
  },
 {
    "attribute_name": "highest_qualification",
    "attribute_value": "MSC",
    "confidence": "55",
    "insight_id": "1674",
    "type": "system"
  },
 {
    "attribute_name": "college",
    "attribute_value": "vivekananda institute",
    "confidence": "70",
    "insight_id": "9834",
    "type": "user"
  },
 {
    "attribute_name": "email",
    "attribute_value": "abcd@bhj.com",
    "confidence": "76",
    "insight_id": "1234",
    "type": "system"
  },
 {
    "attribute_name": "mobile",
    "attribute_value": "6789567843",
    "confidence": "79",
    "insight_id": "1739",
    "type": "user"
  }
],
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:54:53",
        "process_uid": "demo702",
        "task_id": "p4",
        "task_name": "end-task",
        "status": "exit_success",
        "ref_id": "ref11",
        "errors": {

        }
    }
]




[
    {
        "time_stamp": "01/09/20 09:47:05",
        "process_uid": "demo702",
        "task_id": "p1",
        "task_name": "document_ingestion",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:47:40",
        "process_uid": "demo702",
        "task_id": "p2",
        "task_name": "document_classification",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:52:42",
        "process_uid": "demo702",
        "task_id": "p3",
        "task_name": "text_extraction",
        "status": "exit_success",
        "ref_id": "ref10",
        "decision_data":[
  {
    "node_type": "field",
    "confidence": 50,
    "insight_id": "1234",
    "type": "system"
  },
  {
    "node_type": "paragraph",
    "confidence": 30,
    "insight_id": "1234",
    "type": "system"
  },
  {
    "node_type": "field",
    "confidence": 60,
    "insight_id": "1234",
    "type": "user"
  },
  {
    "node_type": "table",
    "confidence": 70,
    "insight_id": "1234",
    "type": "system"
  },
  {
    "node_type": "sentence",
    "confidence": 40,
    "insight_id": "1234",
    "type": "user"
  },
{
    "node_type": "field",
    "confidence": 55,
    "insight_id": "1234",
    "type": "system"
  },
  {
    "node_type": "paragraph",
    "confidence": 40,
    "insight_id": "1234",
    "type": "system"
  },
  {
    "node_type": "field",
    "confidence": 68,
    "insight_id": "1234",
    "type": "system"
  },
  {
    "node_type": "table",
    "confidence": 75,
    "insight_id": "1234",
    "type": "user"
  },
  {
    "node_type": "sentence",
    "confidence": 45,
    "insight_id": "1234",
    "type": "system"
  }
],
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:54:43",
        "process_uid": "demo702",
        "task_id": "p3_1",
        "task_name": "entity_linking_1",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:54:45",
        "process_uid": "demo702",
        "task_id": "p3_1_1",
        "task_name": "entity_linking_1_1",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    },
    {
        "time_stamp": "01/09/20 09:54:53",
        "process_uid": "demo702",
        "task_id": "p4",
        "task_name": "end-task",
        "status": "exit_success",
        "ref_id": "ref10",
        "errors": {

        }
    }
]