from datetime import datetime, timedelta

from elasticsearch import Elasticsearch

es = Elasticsearch()
artist_names = ["kurt kobain", "jimmy hendrix", "john mayer", "steven wilson", "adele", "joe armstrong",
                "sid sriram", "arijit singh", "shreya ghoshal"]
genres = ["Rock", "Classical", "Jazz", "Blues", "metal"]
record_id = ["V-1", "V-2", "V-3", "V-4", "V-5", "V-6", "V-7", "V-8", "V-9", "V-10"]

ecs_bulk_data = []
non_ecs_bulk_data = []


def insert_non_ecs_data(artist_names, genres, record_id):
    month_day = 30
    revenue = 1000
    for i in range(10):
        if i % 2 == 0:
            month_day = month_day * 2
            revenue = revenue * 5
        for artist in artist_names:
            for record in record_id:
                for genre in genres:
                    index_id = artist + "_" + record + "_" + str(genre) + "_" + str(i)
                    d = {
                        "_id": index_id,
                        "_source": {
                            "artist_name": artist,
                            "tracks": 20,
                            "release_date": datetime.now() - timedelta(days=month_day),
                            "album_name": "{0}_{1}_{2}".format(record, artist, genre),
                            "type": "album",
                            "revenue_earned": revenue,
                            "genre": genre
                        }
                    }
                    non_ecs_bulk_data.append(d)


# insert_non_ecs_data(artist_names, genres, record_id)
# print(len(non_ecs_bulk_data))

# non_ecs_start_time = time.time()
# non_ecs_bul_res = helpers.bulk(es, non_ecs_bulk_data, index='music-data-1')
# print("time took to load 1m non ecs file:- ", time.time() - non_ecs_start_time)
# print("\nactions RESPONSE:", non_ecs_bul_res)

range_query = {
    "from": 0,
    "size": 1000,
    "query": {
        "bool": {
            "must": [{
                "range": {
                    "release_date": {
                        "gte": datetime.now() - timedelta(days=300),
                        "lte": datetime.now()
                    }
                }
            }]
        }
    }
}

agg_avg_query = {
    "from": 0,
    "size": 0,
    "aggs": {
        "avg_rev": {"avg": {"field": "revenue_earned"}}
    }
}

range_match_agg_query = {
    "from": 0,
    "size": 0,
    "query": {
        "bool": {
            "must": [{
                "range": {
                    "release_date": {
                        "gte": datetime.now() - timedelta(days=300),
                        "lte": datetime.now()
                    }
                }
            },
                {
                    "match": {
                        "genre": "Rock"
                    }
                }
            ]
        }
    },
    "aggs": {
        "sum_tracks": {"sum": {"field": "tracks"}}
    }
}

count_query = {
    "from": 0,
    "size": 0,
    "aggregations": {
        "total_per_genre": {
            "terms": {
                "field": "genre.keyword"
            }
        },
        "total_per_artist": {
            "terms": {
                "field": "artist_name.keyword"
            }
        }
    }
}


# count of albums released last month
# query = {
#     "from": 0,
#     "size": 1000,
#     "query": {
#         "bool": {
#             "must": [{
#                 "range": {
#                     "release_date": {
#                         "gte": datetime.now() - timedelta(days=30),
#                         "lte": datetime.now()
#                     }
#                 }
#             }]
#         }
#     }
# }
#
# count of albums released with specific genre
# query = {
#     "from": 0,
#     "size": 0,
#     "query": {
#         "bool": {
#             "must": [{
#                 "match": {
#                     "genre": "Rock"
#                 }
#             }]
#         }
#     }
# }
#
# revenue calculated since last 10 months for a specific genre
# query = {
#     "from": 0,
#     "size": 0,
#     "query": {
#         "bool": {
#             "must": [{
#                 "range": {
#                     "release_date": {
#                         "gte": datetime.now() - timedelta(days=300),
#                         "lte": datetime.now()
#                     }
#                 }
#             },
#                 {
#                     "match": {
#                         "genre": "Rock"
#                     }
#                 }
#             ]
#         }
#     },
#     "aggs": {
#         "revenue_earned_sum": {"sum": {"field": "revenue_earned"}}
#     }
# }
#
# max revenue calculated since last 10 months for a specific genre
# test_query = {
#     "from": 0,
#     "size": 0,
#     "query": {
#         "bool": {
#             "must": [{
#                 "range": {
#                     "release_date": {
#                         "gte": datetime.now() - timedelta(days=300),
#                         "lte": datetime.now()
#                     }
#                 }
#             },
#                 {
#                     "match": {
#                         "genre": "Rock"
#                     }
#                 }
#             ]
#         }
#     },
#     "aggs": {
#         "revenue_earned_max": {"max": {"field": "revenue_earned"}}
#     }
# }
#
# average revenue calculated since last 10 months for a specific genre
# test_query = {
#     "from": 0,
#     "size": 0,
#     "query": {
#         "bool": {
#             "must": [{
#                 "range": {
#                     "release_date": {
#                         "gte": datetime.now() - timedelta(days=300),
#                         "lte": datetime.now()
#                     }
#                 }
#             },
#                 {
#                     "match": {
#                         "genre": "Rock"
#                     }
#                 }
#             ]
#         }
#     },
#     "aggs": {
#         "revenue_earned_average": {"avg": {"field": "revenue_earned"}}
#     }
# }
#
# group by artist and genre
# count_query = {
#     "from": 0,
#     "size": 0,
#     "aggregations": {
#         "total_per_genre": {
#             "terms": {
#                 "field": "genre.keyword"
#             }
#         },
#         "total_per_artist": {
#             "terms": {
#                 "field": "artist_name.keyword"
#             }
#         }
#     }
# }


non_ecs_res = es.search(index="music-data-1", body=test_query)
print(non_ecs_res)