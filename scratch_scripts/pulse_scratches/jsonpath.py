import json
from datetime import datetime

import requests
from jsonpath_rw import parse


# domain = '$.domain.[0].children[*].children[*].children[*]'
# document = '$.document.[0].elements[*].children[*].children[*]'

def process(inputs):
    print(inputs)
    json_data = inputs[0]
    # process_uid = "abcd"
    # task_num = "1"
    # task_name = "extraction"
    # status = "complete"
    #
    # ref_id_config = "jsonpath"
    # event_data_config = "jsonpath"
    # decision_data_config = "string"
    #
    # ref_id_input = '$.document.[0].doc_id'
    # if ref_id_config.lower() == "jsonpath":
    #     ref_id = ""
    #     jsonpath_expression = parse(ref_id_input)
    #
    #     json_response = jsonpath_expression.find(json_data)
    #     for match in json_response:
    #         ref_id = match.value
    # else:
    #     ref_id = ref_id_input

    event_data_input = '$.document.[0].metadata.template_info'
    event_data = {}
    # if event_data_config.lower() == "jsonpath":
    event_attributes_to_capture = "doc_class_name,domain_mapping"
    event_attributes_list = event_attributes_to_capture.split(",")
    jsonpath_expression = parse(event_data_input)
    decision_type = "system"
    if isinstance(jsonpath_expression.find(json_data), dict):
        event_data = jsonpath_expression.find(json_data).value
    else:
        data_output = []
        for match in jsonpath_expression.find(json_data):
            match_dict = {}
            # for att in event_attributes_list:
            #     match_dict.update({att: match.value.get(att, "")})
            # event_data.append(match_dict)

            match_dict = {}
            if decision_type == "user":
                source = match.value.get("source", "")
                if match.value.get("node_type", "") == "field":
                    source = match.value["value"].get("source", "")
                if source == "feedback":
                    for att in event_attributes_list:
                        match_dict.update({att: match.value.get(att, "")})
                    match_dict.update({"type": decision_type})
                    data_output.append(match_dict)
            else:
                for att in event_attributes_list:
                    match_dict.update({att: match.value.get(att, "")})
                match_dict.update({"type": decision_type})
                data_output.append(match_dict)

    print(data_output)
    # decision_data_input = "test"
    # decision_data = {}
    # if decision_data_config.lower() == "jsonpath":
    #     decision_attributes_to_capture = ""
    #     decision_attributes_list = decision_attributes_to_capture.split(",")
    #     jsonpath_expression = parse(decision_data_input)
    #     if isinstance(jsonpath_expression.find(json_data), dict):
    #         decision_data = jsonpath_expression.find(json_data).value
    #     else:
    #         decision_data = []
    #         for match in jsonpath_expression.find(json_data):
    #             match_dict = {}
    #             for att in decision_attributes_list:
    #                 match_dict.update({att: match.value.get(att, "")})
    #             decision_data.append(match_dict)
    # else:
    #     decision_data = json.loads(decision_data_input)
    #
    # send_data = {
    #     "time_stamp": datetime.now().strftime('%d/%m/%y %H:%M:%S'),
    #     "process_uid": process_uid,
    #     "task_num": task_num,
    #     "task_name": task_name,
    #     "status": status,
    #     "ref_id": ref_id,
    #     "event_data": event_data,
    #     "decision_data": decision_data
    # }
    # pulse_url = "{url}/{endpoint}".format(url="ddd", endpoint="save_execution_logs")
    # r = requests.post(url=pulse_url, json=send_data)


inputs = [
  {
    "document": [
      {
        "doc_id": "727a6a0d-7b4e-4365-b898-08a2e569e7a5",
        "solution_id": "demopulse",
        "root_id": "727a6a0d-7b4e-4365-b898-08a2e569e7a5",
        "is_root": True,
        "children": [
          
        ],
        "metadata": {
          "properties": {
            "file_resource": {
              "bucket": "qa-staging",
              "key": "demopulse/documents/727a6a0d-7b4e-4365-b898-08a2e569e7a5/invoice.csv",
              "storage": "minio",
              "class": "MinioResource"
            },
            "filename": "invoice.csv",
            "extension": ".csv",
            "num_pages": 1,
            "size": 132,
            "pdf_resource": {
              "bucket": None,
              "key": None,
              "storage": None,
              "class": "XpmsResource"
            },
            "digital_pdf_resource": {
              "bucket": None,
              "key": None,
              "storage": None,
              "class": "XpmsResource"
            },
            "is_digital_pdf": False,
            "email_info": {
              "insight_id": "15b36dd6-2e85-4e02-8e81-d83581f82db1"
            },
            "file_metadata": {
              "file_path": "minio://qa-staging/demopulse/console/direct/6deee9a3-34a8-4240-a6b5-0468508bca25/invoice.csv",
              "ref_id": "7d333b1e-0cb2-494b-a876-4758f2714cb1",
              "ingest_ts": "2020-11-08T18:45:11.411944",
              "process_id": "ad056df5-cfe6-423e-ac4a-4b65003c9b16",
              "dag_execution_id": "mNwi0AI1TZ2zaUzALWFztA",
              "dag_instance_id": "bxjR6SmNS1-7xlpslTERHg$ServiceTask_0pu0ve1",
              "insight_id": "08359353-23f4-4d4e-ad21-52fd569641a8"
            }
          },
          "template_info": {
            "_XpmsObjectMixin__type": "DocumentTemplateInfo",
            "_XpmsObjectMixin__api_version": "9.5",
            "id": "unknown",
            "name": "unknown",
            "template_type": "unknown",
            "score": 0,
            "doc_class_name": "default_invoice_csv",
            "domain_mapping": "invoice_csv",
            "models": {
              "key_value_classifier": "a2d2bae5-28f3-4880-a205-eec082659fab",
              "header_classifier": "90d7f8d6-c915-4ee5-a69a-4c776b151853",
              "paragraph_classifier": "271b4b94-2af9-4d23-8ccb-cff77069b688",
              "omr_classifier": "b138699e-0a27-486a-b3c9-7f9d348c2c63",
              "insight_id": "2a9bdf70-33d4-4efd-8184-1a2618f155f0"
            }
          }
        },
        "processing_state": "extract_document_elements_json_complete",
        "doc_state": "processing",
        "is_test": False,
        "page_groups": [
          {
            "_XpmsObjectMixin__type": "PageGroup",
            "_XpmsObjectMixin__api_version": "9.5",
            "start_page": 1,
            "end_page": 1,
            "score": 0,
            "template_name": "unknown",
            "template_id": "unknown",
            "template_score": 0,
            "template_type": "unknown",
            "doc_class_name": "default_invoice_csv",
            "insight_id": "dc7df534-0973-4977-8710-20826f3c582d",
            "domain_mapping": "invoice_csv",
            "model_mapping": {
              "key_value_classifier": "a2d2bae5-28f3-4880-a205-eec082659fab",
              "header_classifier": "90d7f8d6-c915-4ee5-a69a-4c776b151853",
              "paragraph_classifier": "271b4b94-2af9-4d23-8ccb-cff77069b688",
              "omr_classifier": "b138699e-0a27-486a-b3c9-7f9d348c2c63",
              "insight_id": "62f15b21-59ee-4758-86ed-822370a7f502"
            }
          }
        ],
        "transitions": [
          [
            "ingest_complete",
            "2020-11-08T18:45:11.432432"
          ],
          [
            "convert_initialized",
            "2020-11-08T18:45:15.058355"
          ],
          [
            "convert_complete",
            "2020-11-08T18:45:15.080644"
          ],
          [
            "extract_document_elements_json_initialized",
            "2020-11-08T18:46:52.400302"
          ],
          [
            "extract_document_elements_json_complete",
            "2020-11-08T18:46:52.412517"
          ]
        ],
        "elements": [
          {
            "id": "d5d40c66-6dba-4266-a955-17d09d7910d8",
            "generation_id": 442,
            "insight_id": "04fb8384-f677-4964-a6f4-477564c58f20",
            "node_type": "default_section",
            "confidence": 1,
            "count": 16,
            "is_best": True,
            "justification": "",
            "is_deleted": False,
            "name": "",
            "regions": [
              
            ],
            "_XpmsObjectMixin__type": "ElementTree",
            "text": "",
            "script": "en-us",
            "lang": "en-us",
            "parent_id": "d5d40c66-6dba-4266-a955-17d09d7910d8",
            "_name": "default_section_d5d_1",
            "accept_count": 16,
            "review_required": 0,
            "correct_count": 0,
            "children": [
              {
                "id": "9f2188d3-098c-499e-bac6-5b2c6acaea2f",
                "generation_id": 216,
                "insight_id": "c83bbfea-1b29-4ffb-9a12-7f746552314a",
                "node_type": "table",
                "confidence": 1,
                "count": 1,
                "is_best": True,
                "justification": "",
                "is_deleted": False,
                "name": "sheet_1",
                "regions": [
                  
                ],
                "_XpmsObjectMixin__type": "TableElement",
                "text": "",
                "script": "en-us",
                "lang": "en-us",
                "headers": [
                  {
                    "id": "5c82b331-aa28-457a-9244-db5dba69555e",
                    "generation_id": 551,
                    "insight_id": "3db58c48-e23b-4e93-8092-69a3811a9d91",
                    "node_type": "table_header",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableHeaderElement",
                    "text": "item_name",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      0
                    ],
                    "row_index": [
                      0
                    ],
                    "is_header": True,
                    "parent_id": "5c82b331-aa28-457a-9244-db5dba69555e",
                    "_name": "table_header_5c8_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "206f42ab-7374-4f85-bd3d-7c2daed6c0cb",
                    "generation_id": 935,
                    "insight_id": "3928948a-dbde-4fbf-b1a3-0ef20bc78159",
                    "node_type": "table_header",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableHeaderElement",
                    "text": "item_number",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      1
                    ],
                    "row_index": [
                      0
                    ],
                    "is_header": True,
                    "parent_id": "206f42ab-7374-4f85-bd3d-7c2daed6c0cb",
                    "_name": "table_header_206_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "18c445fe-a4ff-4e2a-b8c3-b5ce6e6fdbf3",
                    "generation_id": 458,
                    "insight_id": "a78b5109-c61b-44de-b0c2-31c6116fd446",
                    "node_type": "table_header",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableHeaderElement",
                    "text": "item_count",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      2
                    ],
                    "row_index": [
                      0
                    ],
                    "is_header": True,
                    "parent_id": "18c445fe-a4ff-4e2a-b8c3-b5ce6e6fdbf3",
                    "_name": "table_header_18c_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "815f62d6-bddf-416a-a139-f0687e0f2cfd",
                    "generation_id": 402,
                    "insight_id": "cd996418-7fbb-42a4-89bc-0a0292279ffa",
                    "node_type": "table_header",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableHeaderElement",
                    "text": "item_price",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      3
                    ],
                    "row_index": [
                      0
                    ],
                    "is_header": True,
                    "parent_id": "815f62d6-bddf-416a-a139-f0687e0f2cfd",
                    "_name": "table_header_815_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  }
                ],
                "cells": [
                  {
                    "id": "3273b14a-6534-4497-a3b6-53a855df1c7d",
                    "generation_id": 619,
                    "insight_id": "b502c03c-043f-4eca-be9c-c90525b39609",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "hulk cigar",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      0
                    ],
                    "row_index": [
                      1
                    ],
                    "parent_id": "3273b14a-6534-4497-a3b6-53a855df1c7d",
                    "_name": "table_cell_327_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "eb2a3971-0124-48b3-a2dd-598880a8a1a8",
                    "generation_id": 699,
                    "insight_id": "1d032066-765d-40bc-8afe-29eb0481ac0c",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "90587",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      1
                    ],
                    "row_index": [
                      1
                    ],
                    "parent_id": "eb2a3971-0124-48b3-a2dd-598880a8a1a8",
                    "_name": "table_cell_eb2_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "48bd6e92-cb8c-4403-a148-9b734a83d687",
                    "generation_id": 853,
                    "insight_id": "aeaf349d-a36e-450e-81a7-355a21d815a7",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "2",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      2
                    ],
                    "row_index": [
                      1
                    ],
                    "parent_id": "48bd6e92-cb8c-4403-a148-9b734a83d687",
                    "_name": "table_cell_48b_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "a45c0b9d-a253-4940-bc21-35ec6d5557ed",
                    "generation_id": 143,
                    "insight_id": "44433f0b-ccd4-499f-8e78-a060804fa491",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "65000",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      3
                    ],
                    "row_index": [
                      1
                    ],
                    "parent_id": "a45c0b9d-a253-4940-bc21-35ec6d5557ed",
                    "_name": "table_cell_a45_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "6bc67aa2-82ff-4ccc-9a4b-bad7dcec817b",
                    "generation_id": 479,
                    "insight_id": "0f338b0f-95bb-438a-9920-9912b4509c7f",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "hawkeye goggles",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      0
                    ],
                    "row_index": [
                      2
                    ],
                    "parent_id": "6bc67aa2-82ff-4ccc-9a4b-bad7dcec817b",
                    "_name": "table_cell_6bc_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "989872c1-3950-42b7-bd25-ee5e235c09e1",
                    "generation_id": 958,
                    "insight_id": "28d0e648-d62d-4df2-812c-54b2fa438461",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "789657",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      1
                    ],
                    "row_index": [
                      2
                    ],
                    "parent_id": "989872c1-3950-42b7-bd25-ee5e235c09e1",
                    "_name": "table_cell_989_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "27485825-a368-49aa-acb6-28e22420d211",
                    "generation_id": 540,
                    "insight_id": "136cc9b9-2694-405a-96a5-bf5ef24aeb56",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "12",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      2
                    ],
                    "row_index": [
                      2
                    ],
                    "parent_id": "27485825-a368-49aa-acb6-28e22420d211",
                    "_name": "table_cell_274_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "6b4e9089-1c40-4d8c-b260-449e65f24240",
                    "generation_id": 836,
                    "insight_id": "1a5c1a32-154f-4281-a95c-5f75ef65bea2",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "7800",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      3
                    ],
                    "row_index": [
                      2
                    ],
                    "parent_id": "6b4e9089-1c40-4d8c-b260-449e65f24240",
                    "_name": "table_cell_6b4_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "73784d6c-9676-45c3-9ab4-a099126f72a3",
                    "generation_id": 780,
                    "insight_id": "27f31fdb-2218-4885-ae46-9628ccd78e39",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "spiderman bugspray ",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      0
                    ],
                    "row_index": [
                      3
                    ],
                    "parent_id": "73784d6c-9676-45c3-9ab4-a099126f72a3",
                    "_name": "table_cell_737_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "a51db6f2-5f80-42f3-98a7-a392bb7d5f85",
                    "generation_id": 777,
                    "insight_id": "03a52836-b5d9-4873-9d67-1cd6ea81001b",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "2312",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      1
                    ],
                    "row_index": [
                      3
                    ],
                    "parent_id": "a51db6f2-5f80-42f3-98a7-a392bb7d5f85",
                    "_name": "table_cell_a51_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "ea3dfbf3-aa00-4019-a078-c21a3b23506a",
                    "generation_id": 207,
                    "insight_id": "d6af359a-0a44-4d68-a392-01490ae220a8",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "13",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      2
                    ],
                    "row_index": [
                      3
                    ],
                    "parent_id": "ea3dfbf3-aa00-4019-a078-c21a3b23506a",
                    "_name": "table_cell_ea3_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  },
                  {
                    "id": "c854a01a-c828-4b6b-8d47-bc132cadbce0",
                    "generation_id": 630,
                    "insight_id": "e9f78201-4c7a-4bb2-a94c-93c6719832b6",
                    "node_type": "table_cell",
                    "confidence": 1,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "",
                    "regions": [
                      
                    ],
                    "_XpmsObjectMixin__type": "TableCellElement",
                    "text": "200",
                    "script": "en-us",
                    "lang": "en-us",
                    "column_index": [
                      3
                    ],
                    "row_index": [
                      3
                    ],
                    "parent_id": "c854a01a-c828-4b6b-8d47-bc132cadbce0",
                    "_name": "table_cell_c85_1",
                    "status": "accepted",
                    "children": [
                      
                    ]
                  }
                ]
              }
            ]
          }
        ],
        "is_template": False,
        "confidence": 1,
        "context": "",
        "doc_context_json": "",
        "pages": {
          "1": {
            "num": 1,
            "metadata": {
              "_XpmsObjectMixin__type": "PageMetadata",
              "_XpmsObjectMixin__api_version": "9.5",
              "raw": {
                "bucket": "qa-staging",
                "key": "demopulse/documents/727a6a0d-7b4e-4365-b898-08a2e569e7a5/pages/sheet_1.json",
                "storage": "minio",
                "class": "MinioResource"
              }
            },
            "deskew_info": 0
          }
        }
      }
    ],
    "doc_id": "727a6a0d-7b4e-4365-b898-08a2e569e7a5",
    "root_id": "727a6a0d-7b4e-4365-b898-08a2e569e7a5",
    "request_type": "extract_document_elements_json"
  }
]


process(inputs)