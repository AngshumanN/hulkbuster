from datetime import datetime, timedelta

from elasticsearch import Elasticsearch, helpers

es = Elasticsearch()
process_name = ["WGS_LOAD_POSTBACK", "EDIS_UPDATE", "FILEGEN", "WGS_UPDATE", "WORK_CREATION", "WGS_PREMAPPER_POSTBACK",
                "EDI_CLAIMS_INTAKE", "ORCHESTRATION", "PSD_UPDATE", "OBJECT_GENERATION"]
codes = [201, 210, 500, 510, 400]
record_id = ["V-1", "V-2", "V-3", "V-4", "V-5", "V-6", "V-7", "V-8", "V-9", "V-10"]

ecs_bulk_data = []
non_ecs_bulk_data = []


# def insert_ecs_data(process_name, codes, record_id):
#     for i in range(2000):
#         for process in process_name:
#             for record in record_id:
#                 for code in codes:
#                     index_id = process + "_" + record + "_" + str(code) + "_" + str(i)
#                     d = {
#                         "_id": index_id,
#                         "_source": {"@timestamp": datetime.now() - timedelta(days=i), "ecs.version": "1.6.0-dev",
#                                     "message": "WGS LOAD POSTBACK",
#                                     "timestamp": datetime.now() - timedelta(days=i), "process.name": process,
#                                     "file.name": "EDI_Claims.5.txt",
#                                     "record_id": record, "code": code, "source.id": "cd5e1ae88dde11eaac7b4c348889a238",
#                                     "claim_type": "PROFESSIONAL",
#                                     "rec_start_process_timestamp": datetime.now() - timedelta(days=i, hours=1),
#                                     "rec_end_process_timestamp": datetime.now() - timedelta(days=i),
#                                     "begin_recordstatus": "CLAIM STAGED",
#                                     "end_recordstatus": "COMPLETED POSTBACK",
#                                     "created_date": datetime.now() - timedelta(days=i),
#                                     "ciwclaimstatus": "Resolved-Denied",
#                                     "job_id": "EDI_Claims_P_Prod_288_20191220145209.1580364690380000.txt",
#                                     "stag_unique_claim_id": "CDE126243729", "dcn": "2020034BF7281", "itemcode": "80",
#                                     "sequencenumber": "999",
#                                     "kernelnumber": "", "group.number": "ITS271",
#                                     "nationalprovideridentifier": "1629398219",
#                                     "ecn": "CP19354092954263555", "sccfnumber": "",
#                                     "company_mbu_fund_code": "COMP CODE :, MBU :, FUND :",
#                                     "lob": "Claim", "claimformtype": "P", "inputchannel": "EDI",
#                                     "datereceived": "20191220",
#                                     "servicestartdate": "2019/07/31 00:00:00", "serviceenddate": "2019/07/31 00:00:00",
#                                     "submitteddate": "20191220",
#                                     "membercode": "20", "membersource": "W-WGS", "totalclmchrgamt": "359",
#                                     "totalclaimallowedamount": "359",
#                                     "paidamount": "0", "systemindicator": "W-WGS",
#                                     "claim_file_type": "ITS HOST Professional",
#                                     "exception_code": "500", "exceptionmessage": "EM 1", "book_of_busn_cd": "101",
#                                     "clm_lob_cd": "101",
#                                     "receiver_id": "WGSITSNH", "error_timestamp": "",
#                                     "resolved_timestamp": "2020/02/05 02:09:35",
#                                     "ddc_currentstatus_cd": "99", "company_cd": "", "pay_action_cd": "RG2400",
#                                     "workbasket_txt": "",
#                                     "ciw_ddcfunctioncode": "", "ciw_ddcunit_code": "", "exception_type_cd": "1000",
#                                     "end_extraction_date": "2020/02/05 19:28:40"
#
#                                     }
#                     }
#                     ecs_bulk_data.append(d)
#                     # if process == "WGS_PREMAPPER_POSTBACK" and code == 400:
#                     #     break
#                     # if process == "OBJECT_GENERATION" and code == 510:
#                     #     break
#                     # es.index(index="ecs-pulse-bulk10000", id=index_id, body=doc)


def insert_non_ecs_data(process_name, codes, record_id):
    for i in range(2000):
        for process in process_name:
            for record in record_id:
                for code in codes:
                    index_id = process + "_" + record + "_" + str(code) + "_" + str(i)
                    d = {
                        "_id": index_id,
                        "_source": {"@timestamp": datetime.now() - timedelta(days=i),
                                    "message": "WGS LOAD POSTBACK",
                                    "timestamp": datetime.now() - timedelta(days=i), "process": process,
                                    "file": "EDI_Claims.5.txt",
                                    "record_id": record, "code": code, "source": "cd5e1ae88dde11eaac7b4c348889a238",
                                    "claim_type": "PROFESSIONAL",
                                    "rec_start_process_timestamp": datetime.now() - timedelta(days=i, hours=1),
                                    "rec_end_process_timestamp": datetime.now() - timedelta(days=i),
                                    "begin_recordstatus": "CLAIM STAGED",
                                    "end_recordstatus": "COMPLETED POSTBACK",
                                    "created_date": datetime.now() - timedelta(days=i),
                                    "ciwclaimstatus": "Resolved-Denied",
                                    "job_id": "EDI_Claims_P_Prod_288_20191220145209.1580364690380000.txt",
                                    "stag_unique_claim_id": "CDE126243729", "dcn": "2020034BF7281", "itemcode": "80",
                                    "sequencenumber": "999",
                                    "kernelnumber": "", "group.number": "ITS271",
                                    "nationalprovideridentifier": "1629398219",
                                    "ecn": "CP19354092954263555", "sccfnumber": "",
                                    "company_mbu_fund_code": "COMP CODE :, MBU :, FUND :",
                                    "lob": "Claim", "claimformtype": "P", "inputchannel": "EDI",
                                    "datereceived": "20191220",
                                    "servicestartdate": "2019/07/31 00:00:00", "serviceenddate": "2019/07/31 00:00:00",
                                    "submitteddate": "20191220",
                                    "membercode": "20", "membersource": "W-WGS", "totalclmchrgamt": "359",
                                    "totalclaimallowedamount": "359",
                                    "paidamount": "0", "systemindicator": "W-WGS",
                                    "claim_file_type": "ITS HOST Professional",
                                    "exception_code": "500", "exceptionmessage": "EM 1", "book_of_busn_cd": "101",
                                    "clm_lob_cd": "101",
                                    "receiver_id": "WGSITSNH", "error_timestamp": "",
                                    "resolved_timestamp": "2020/02/05 02:09:35",
                                    "ddc_currentstatus_cd": "99", "company_cd": "", "pay_action_cd": "RG2400",
                                    "workbasket_txt": "",
                                    "ciw_ddcfunctioncode": "", "ciw_ddcunit_code": "", "exception_type_cd": "1000",
                                    "end_extraction_date": "2020/02/05 19:28:40"

                                    }
                    }
                    non_ecs_bulk_data.append(d)
                    # if process == "WGS_PREMAPPER_POSTBACK" and code == 400:
                    #     break
                    # if process == "OBJECT_GENERATION" and code == 510:
                    #     break
                    # es.index(index="pulse-bulk10000", id=index_id, body=doc)


# insert_ecs_data(process_name=process_name, codes=codes, record_id=record_id)
# print(len(ecs_bulk_data))
# insert_non_ecs_data(process_name=process_name, codes=codes, record_id=record_id)
# print(len(non_ecs_bulk_data))

try:
    import time

    # # make the bulk call using 'actions' and get a response
    # ecs_start_time = time.time()
    # ecs_bul_res = helpers.bulk(es, ecs_bulk_data, index='ecs-pulse-bulk1million')
    # print("time took to load 1m ecs file:- ", time.time() - ecs_start_time)
    # print("\nactions RESPONSE:", ecs_bul_res)
    # non_ecs_start_time = time.time()
    # non_ecs_bul_res = helpers.bulk(es, non_ecs_bulk_data, index='pulse-bulk1million')
    # print("time took to load 1m non ecs file:- ", time.time() - non_ecs_start_time)
    # print("\nactions RESPONSE:", non_ecs_bul_res)
except Exception as e:
    print("\nERROR:", e)

ecs_query = {
    "query": {
        "bool": {
            "must": [
                {
                    "range": {
                        "@timestamp": {
                            "gte": datetime.now() - timedelta(days=125),
                            "lte": datetime.now()
                        }
                    }
                }
            ]
        }
    },
    "aggs": {
        "byday": {
            "date_histogram": {
                "field": "@timestamp",
                "calendar_interval": "month",
                "format": "yyyy-MM-dd HH:mm",
                "min_doc_count": 0,
                "extended_bounds": {
                    "min": "2020-02-01 03:30",
                    "max": "2020-12-12 05:30"
                }
            },
            "aggs": {
                "distinct_colors": {
                    "terms": {
                        "field": "process.name.keyword",
                        "min_doc_count": 0
                    },
                    "aggs": {
                        "distinct_colors": {
                            "terms": {
                                "field": "code.keyword",
                                "min_doc_count": 0
                            }
                        }
                    }
                }
            }
        }
    }
}

non_ecs_query = {
    "query": {
        "bool": {
            "must": [
                {
                    "range": {
                        "@timestamp": {
                            "gte": datetime.now() - timedelta(days=125),
                            "lte": datetime.now()
                        }
                    }
                }
            ]
        }
    },
    "aggs": {
        "byday": {
            "date_histogram": {
                "field": "@timestamp",
                "calendar_interval": "month",
                "format": "yyyy-MM-dd HH:mm",
                "min_doc_count": 0,
                "extended_bounds": {
                    "min": "2020-02-01 03:30",
                    "max": "2020-12-12 05:30"
                }
            },
            "aggs": {
                "distinct_colors": {
                    "terms": {
                        "field": "process.keyword",
                        "min_doc_count": 0
                    },
                    "aggs": {
                        "distinct_colors": {
                            "terms": {
                                "field": "code.keyword",
                                "min_doc_count": 0
                            }
                        }
                    }
                }
            }
        }
    }
}
import time

# ecs_start_time = time.time()
# ecs_res = es.search(index="ecs-pulse-bulk", body=ecs_query)
# ecs_res_execution_time = time.time()-ecs_start_time
avg_ecs_exec_time = 0
# for i in range(10):
#     ecs_start_time = time.time()
#     ecs_res = es.search(index="pulse-bulk1million", body=ecs_query)
#     ecs_res_execution_time = time.time() - ecs_start_time
#     avg_ecs_exec_time += ecs_res_execution_time

# avg_ecs_exec_time = avg_ecs_exec_time / 10
# print("Got %d Hits:" % ecs_res['hits']['total']['value'], avg_ecs_exec_time)

avg_non_ecs_exec_time = 0
for i in range(10):
    start_time = time.time()
    non_ecs_res = es.search(index="pulse-bulk1million", body=non_ecs_query)
    non_ecs_res_execution_time = time.time() - start_time
    avg_non_ecs_exec_time += non_ecs_res_execution_time

avg_non_ecs_exec_time = avg_non_ecs_exec_time / 10
print("Got %d Hits:" % non_ecs_res['hits']['total']['value'], avg_non_ecs_exec_time)
