# # import json
# # from copy import deepcopy
# #
# # from jrules_lib.rule_manager import RuleManager
# # from xpms_common.mq_validate import is_valid_json
# #
# # from microservice.rule_util import transform_save_rule_object, transform_save_action_object, generate_result_payload
# #
# #
# # def save_rule(payload_data, solution_id):
# #     payload = payload_data
# #     result = {}
# #     if "solution_id" not in payload:
# #         payload.update({"solution_id": solution_id})
# #     save_rule_schema = {
# #         "$schema": "http://json-schema.org/schema#",
# #         "type": "object",
# #         "properties": {
# #             "rules": {"type": "array"},
# #             "solution_id": {"type": "string"},
# #         },
# #         "required": ["rules"]
# #     }
# #     if "rule" in payload:
# #         is_valid_json(payload["rule"], save_rule_schema)
# #         ui_payload = deepcopy(payload)
# #         payload['ui_obj'] = ui_payload
# #         payload_rule = eval(str(payload["rule"]).replace("\'rules\':", "\'conditions\':"))
# #         payload["rule"] = transform_save_rule_object(payload_rule)
# #         if "actions" in payload["rule"]:
# #             payload["rule"]["actions"] = transform_save_action_object(payload["rule"])["actions"]
# #         response_data = RuleManager().process("saveRule", payload)
# #         if response_data["status"]["success"]:
# #             data = {"rule_id": response_data["metadata"]["rule_id"]}
# #         else:
# #             data = None
# #         result = generate_result_payload(response_data, data=data, message="New Rule Created")
# #     else:
# #         result["status"] = 400
# #         result["message"] = "no Rule found in request"
# #     return json.dumps(result)
# #
# #
# # payload_data = {
# #   "solution_id": "testdomso_upSBjacJUa7gHgfkKgrZoW",
# #   "rule_set_id": "2ab141b1-4b67-4816-817a-1877f7ce41de",
# #   "rule_name": "test123",
# #   "desc": "",
# #   "rule": {
# #     "rules": [
# #       {
# #         "inputValue": "",
# #         "domain_attribute": "medical_history.medical_history_risk_factors",
# #         "selected_entity": "medical_history",
# #         "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
# #         "lval_options": {
# #           "field": "list of entities",
# #           "source_type": "domain",
# #           "Key_path_expression": "value"
# #         },
# #         "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
# #         "operator": "Greater Than",
# #         "rval": "45"
# #       },
# #       {
# #         "run_type": "and",
# #         "rules": [
# #           {
# #             "inputValue": "",
# #             "domain_attribute": "medical_history.medical_history_risk_factors",
# #             "selected_entity": "medical_history",
# #             "lval_options": {
# #               "field": "list of entities",
# #               "source_type": "domain",
# #               "Key_path_expression": "value"
# #             },
# #             "operator_name": "Greater Than",
# #             "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
# #             "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
# #             "operator": "Greater Than",
# #             "rval": "67"
# #           },
# #           {
# #             "operator": "Less Than",
# #             "run_type": "and",
# #             "operator_name": "Less Than",
# #             "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
# #             "selected_entity": "medical_history",
# #             "inputValue": "",
# #             "domain_attribute": "medical_history.medical_history_risk_factors",
# #             "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
# #             "lval_options": {
# #               "field": "list of entities",
# #               "source_type": "domain",
# #               "Key_path_expression": "value"
# #             },
# #             "rval": "63"
# #           }
# #         ]
# #       }
# #     ],
# #     "source_scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
# #     "actions": [
# #       {
# #         "is_previous_action_result": False,
# #         "entityPath": "medical_history.medical_history_risk_factors",
# #         "sourceScopeField": "medical_history.medical_history_risk_factors",
# #         "is_source_object": False,
# #         "is_condition_object": False,
# #         "rval": "8",
# #         "attribute_name": "medical_history_risk_factors",
# #         "type": "custom",
# #         "inputValue": "",
# #         "operator": "Create Attribute",
# #         "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]"
# #       }
# #     ],
# #     "is_service_rule": True
# #   },
# #   "rule_id": "02479f19-c59c-48b9-81e0-dbcbf1a0552d"
# # }
# # sol_id = "rules"
# # save_rule(payload_data,sol_id)
#
# def get_rule(response_data):
#     rule_object = response_data["metadata"]["ui_obj"]
#     for key in response_data["metadata"]['rule']:
#         if key != 'ui_obj' and key not in rule_object:
#             rule_object[key] = response_data["metadata"]['rule'][key]
#
#     print(rule_object)
# resp_data = { "metadata": {
#   "solution_id": "testdomso_upSBjacJUa7gHgfkKgrZoW",
#   "rule_set_id": "2ab141b1-4b67-4816-817a-1877f7ce41de",
#   "rule_name": "test123",
#   "desc": "",
#   "rule": {
#     "conditions": [
#       {
#         "inputValue": "",
#         "domain_attribute": "medical_history.medical_history_risk_factors",
#         "selected_entity": "medical_history",
#         "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#         "lval_options": {
#           "field": "list of entities",
#           "source_type": "domain",
#           "Key_path_expression": "value"
#         },
#         "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
#         "rval": 45,
#         "op": ">"
#       },
#       {
#         "run_type": "and"
#       },
#       {
#         "conditions": [
#           {
#             "inputValue": "",
#             "domain_attribute": "medical_history.medical_history_risk_factors",
#             "selected_entity": "medical_history",
#             "lval_options": {
#               "field": "list of entities",
#               "source_type": "domain",
#               "Key_path_expression": "value"
#             },
#             "operator_name": "Greater Than",
#             "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#             "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
#             "rval": 67,
#             "op": ">"
#           },
#           {
#             "run_type": "and"
#           },
#           {
#             "operator_name": "Less Than",
#             "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#             "selected_entity": "medical_history",
#             "inputValue": "",
#             "domain_attribute": "medical_history.medical_history_risk_factors",
#             "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
#             "lval_options": {
#               "field": "list of entities",
#               "source_type": "domain",
#               "Key_path_expression": "value"
#             },
#             "rval": 63,
#             "op": "<"
#           }
#         ]
#       }
#     ],
#     "source_scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#     "actions": [
#       {
#         "action": "create_attribute",
#         "lval": {
#           "key": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')].children",
#           "is_condition_object": False,
#           "is_previous_action_result": False,
#           "is_source_object": False
#         },
#         "rval": {
#           "attributes": [
#             [
#               {
#                 "key": "$.name",
#                 "value": "medical_history_risk_factors"
#               },
#               {
#                 "key": "$.value",
#                 "value": "8"
#               }
#             ]
#           ]
#         },
#         "options": {
#           "is_previous_action_result": False,
#           "entityPath": "medical_history.medical_history_risk_factors",
#           "sourceScopeField": "medical_history.medical_history_risk_factors",
#           "is_source_object": False,
#           "is_condition_object": False,
#           "rval": "8",
#           "attribute_name": "medical_history_risk_factors",
#           "type": "custom",
#           "inputValue": "",
#           "operator": "Create Attribute",
#           "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]"
#         }
#       }
#     ],
#     "is_service_rule": True
#   },
#   "rule_id": "02479f19-c59c-48b9-81e0-dbcbf1a0552d",
#   "ui_obj": {
#     "solution_id": "testdomso_upSBjacJUa7gHgfkKgrZoW",
#     "rule_set_id": "2ab141b1-4b67-4816-817a-1877f7ce41de",
#     "rule_name": "test123",
#     "desc": "",
#     "rule": {
#       "rules": [
#         {
#           "inputValue": "",
#           "domain_attribute": "medical_history.medical_history_risk_factors",
#           "selected_entity": "medical_history",
#           "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#           "lval_options": {
#             "field": "list of entities",
#             "source_type": "domain",
#             "Key_path_expression": "value"
#           },
#           "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
#           "operator": "Greater Than",
#           "rval": "45"
#         },
#         {
#           "run_type": "and",
#           "rules": [
#             {
#               "inputValue": "",
#               "domain_attribute": "medical_history.medical_history_risk_factors",
#               "selected_entity": "medical_history",
#               "lval_options": {
#                 "field": "list of entities",
#                 "source_type": "domain",
#                 "Key_path_expression": "value"
#               },
#               "operator_name": "Greater Than",
#               "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#               "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
#               "operator": "Greater Than",
#               "rval": "67"
#             },
#             {
#               "operator": "Less Than",
#               "run_type": "and",
#               "operator_name": "Less Than",
#               "scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#               "selected_entity": "medical_history",
#               "inputValue": "",
#               "domain_attribute": "medical_history.medical_history_risk_factors",
#               "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]",
#               "lval_options": {
#                 "field": "list of entities",
#                 "source_type": "domain",
#                 "Key_path_expression": "value"
#               },
#               "rval": "63"
#             }
#           ]
#         }
#       ],
#       "source_scope": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')]",
#       "actions": [
#         {
#           "is_previous_action_result": False,
#           "entityPath": "medical_history.medical_history_risk_factors",
#           "sourceScopeField": "medical_history.medical_history_risk_factors",
#           "is_source_object": False,
#           "is_condition_object": False,
#           "rval": "8",
#           "attribute_name": "medical_history_risk_factors",
#           "type": "custom",
#           "inputValue": "",
#           "operator": "Create Attribute",
#           "lval": "$.domain[0].children[?(@.name == 'psp')].children[?(@.name == 'medical_history')].children[?(@.name == 'medical_history_risk_factors')]"
#         }
#       ],
#       "is_service_rule": True
#     },
#     "rule_id": "02479f19-c59c-48b9-81e0-dbcbf1a0552d"
#   }
# }}
# get_rule(response_data=resp_data)

#
# def minion_game(s):
#     vowels = 'AEIOU'
#
#     kevsc = 0
#     stusc = 0
#     for i in range(len(s)):
#         if s[i] in vowels:
#             kevsc += (len(s)-i)
#         else:
#             stusc += (len(s)-i)
#
#     if kevsc > stusc:
#         print("Kevin", kevsc)
#     elif kevsc < stusc:
#         print("Stuart", stusc)
#     else:
#         print("Draw")
#
# minion_game("BANANA")
from xpms_storage.db_handler import DBProvider

AB_TESTING_COLLECTION = "ab_testing_cfg"


def find_recent_test_record(filter_obj):
    db = DBProvider.get_instance(db_name=filter_obj["solution_id"])
    result = db.find(table=AB_TESTING_COLLECTION, filter_obj=filter_obj, sort=[{"key":"created_ts", "order": "desc"}])
    result = result[0] if isinstance(result[0], list) else result
    return result


res = find_recent_test_record({"solution_id":"markabtest"})
print(res)