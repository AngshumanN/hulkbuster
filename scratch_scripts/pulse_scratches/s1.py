# # if __name__ == "__main__":
# #     a_s = input()
# #     a_l = str(a_s).split(" ")[0]
# #     s = str(a_s).split(" ")[1]
# #     a = input()
# #     try:
# #         ary = [int(i) for i in str(a).split(" ")]
# #     except:
# #         ary = []
# #     done = False
# #     for ind, val in enumerate(ary):
# #         if ind == 37:
# #             pass
# #         pat = 2
# #         while pat <= int(a_l):
# #             a_r = ary[ind:pat]
# #             if sum(a_r) == int(s):
# #                 print("{0} {1}".format(ind + 1, pat))
# #                 done = True
# #                 break
# #             pat = pat + 1
# #         if done == True:
# #             break
# #     if done==False:
# #         print("-1")
# from datetime import datetime
# #
# #
# def fibo(n,lookup):
#     if n == 0 or n == 1:
#         lookup[n] = n
#
#     if lookup[n] is None:
#         lookup[n] = fibo(n - 1, lookup) + fibo(n - 2, lookup)
#     return lookup[n]
# # def fibo(n):
# #     if n<=1:
# #         return n
# #     else:
# #         return fibo(n-1) + fibo(n-2)
#
# if __name__=="__main__":
#     start_time = datetime.now()
#     print("start_time",start_time)
#     lookup = [None]*(1000001)
#     res = fibo(1000000,lookup)
#     print(res)
#     del lookup
#     end_time = datetime.now()
#     print("start_time", start_time)
#     print("diff",end_time-start_time)
# from datetime import datetime
# #
# #
# # from datetime import datetime
# #
# #
# # def DecimalToBinary(num,lookup):
# #
# #     if num > 1:
# #         DecimalToBinary(num // 2)
# #     print(num % 2, end='')
# #
# # if __name__=="__main__":
# #     start_time = datetime.now()
# #     print(start_time)
# #     lookup = [None] * 445
# #     DecimalToBinary(444,lookup)
# #     del lookup
# #     end_time = datetime.now()
# #     print(end_time-start_time)
# #     # print(res)
# #
# # # def trap_water(elevation_map):
# # #     water_units = 0
# # #     for index,m in enumerate(elevation_map):
# # #         if index !=0 and index<len(elevation_map)-1:
# # #             left_max = max(elevation_map[0:index])
# # #             right_max = max(elevation_map[index:len(elevation_map)])
# # #             min_l_r = min(left_max,right_max)
# # #             if (min_l_r-m) > 0:
# # #                 water_units = water_units + (min_l_r-m)
# # #     return water_units
# # #
# # # print(trap_water([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]))
# #
# # def fibo(n):
# #     if n<=1:
# #         return n
# #     else:
# #         return fibo(n-1) + fibo(n-2)
#
#
#
#
#

def decimal_to_binary(num, binary):
    if num > 1:
        decimal_to_binary(num // 2, binary)
    binary.append(num % 2)


def binary_to_decimal(binary):
    decimal, i= 0, 0
    for b in binary:
        dec = b % 10
        decimal = decimal + dec * pow(2, i)
        i += 1
    return decimal


def toggle_bits(num, lindex, rindex):
    binary = []
    decimal_to_binary(num, binary)
    binary.reverse()
    for i in range(lindex-1,rindex):
        binary[i] = 1 if binary[i] == 0 else 0
    return binary_to_decimal(binary)


if __name__ == "__main__":
    test_cases = 1
    for t in range(0, test_cases):
        # num_lind_rind = str(input()).strip()
        num = 50
        lind = 2
        rind = 5
        print(toggle_bits(num, lind, rind))


[
        {
          "time_stamp": "01/09/20 09:47:05",
          "process_uid": "demo",
          "task_id": "ingest",
          "task_name": "document_ingestion",
          "status": "exit_success",
          "ref_id": "ref11",
          "errors": {
          }
        },
        {
          "time_stamp": "01/09/20 09:47:40",
          "process_uid": "demo",
          "task_id": "classify",
          "task_name": "document_classification",
          "status": "exit_success",
          "ref_id": "ref11",
          "errors": {
          }
        },
        {
          "time_stamp": "01/09/20 09:52:42",
          "process_uid": "demo",
          "task_id": "extraction",
          "task_name": "text_extraction",
          "status": "exit_success",
          "ref_id": "ref11",
          "decision_data": [
            {
              "node_type": "field",
              "confidence": 50,
              "insight_id": "1234",
              "type": "system"
            },
            {
              "node_type": "paragraph",
              "confidence": 30,
              "insight_id": "1234",
              "type": "system"
            },
            {
              "node_type": "field",
              "confidence": 60,
              "insight_id": "1234",
              "type": "user"
            },
            {
              "node_type": "table",
              "confidence": 70,
              "insight_id": "1234",
              "type": "system"
            },
            {
              "node_type": "sentence",
              "confidence": 40,
              "insight_id": "1234",
              "type": "user"
            },
            {
              "node_type": "field",
              "confidence": 55,
              "insight_id": "1234",
              "type": "system"
            },
            {
              "node_type": "paragraph",
              "confidence": 40,
              "insight_id": "1234",
              "type": "system"
            },
            {
              "node_type": "field",
              "confidence": 68,
              "insight_id": "1234",
              "type": "system"
            },
            {
              "node_type": "table",
              "confidence": 75,
              "insight_id": "1234",
              "type": "user"
            },
            {
              "node_type": "sentence",
              "confidence": 45,
              "insight_id": "1234",
              "type": "system"
            }
          ],
          "errors": {
          }
        },
        {
          "time_stamp": "01/09/20 09:54:43",
          "process_uid": "demo",
          "task_id": "linking",
          "task_name": "entity_linking",
          "status": "exit_success",
          "ref_id": "ref11",
          "errors": {
          }
        },
        {
          "time_stamp": "01/09/20 09:54:53",
          "process_uid": "demo",
          "task_id": "end",
          "task_name": "end-task",
          "status": "exit_success",
          "ref_id": "ref11",
          "errors": {
          }
        }
      ]