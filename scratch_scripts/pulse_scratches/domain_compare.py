inp = {
    "id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "generation_id": 656,
    "insight_id": "69cd3239-22dc-4509-8ea9-9fc20b5b9285",
    "node_type": "document",
    "confidence": 0,
    "count": 1,
    "is_best": True,
    "justification": "",
    "is_deleted": False,
    "name": "document",
    "regions": [

    ],
    "_XpmsObjectMixin__type": "DocumentDomain",
    "ts": "2020-07-15T16:26:16.113359",
    "solution_id": "turnkeydefault",
    "type": "",
    "version": 0,
    "health": 1.0,
    "node_id": "",
    "cardinality": 1,
    "rules_applied": [

    ],
    "elements": [

    ],
    "confidence_dict": {
        "domain": 0.3551,
        "count": 33,
        "groups": {
            "patient_details": 0.2839,
            "admission": 0.2789,
            "payment_information": 0.5568,
            "customer_experience": 0.25,
            "patient": 0.3551,
            "insight_id": "52e585b3-c2f8-4c31-8940-8bc9366a2906"
        },
        "insight_id": "db0df17b-a914-4a88-85db-a69f36f0e750"
    },
    "validation": [

    ],
    "root_id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "doc_id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "parent_id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "_name": "document_6a5_0",
    "review_counts": {
        "accepted": 1,
        "needs_review": 32,
        "corrected": 0,
        "insight_id": "e3886db6-efc7-426d-8b3b-83f5ad87d880"
    },
    "confidence_model_enabled": False,
    "children": [
        {
            "id": "fdb961a7-cce7-4303-a91e-415be6c2b760",
            "generation_id": 828,
            "insight_id": "c1478956-b877-42ab-8116-59007be454d3",
            "node_type": "domain",
            "confidence": 0.35506666666666664,
            "count": 33,
            "is_best": True,
            "justification": "",
            "is_deleted": False,
            "name": "patient",
            "regions": [

            ],
            "_XpmsObjectMixin__type": "DomainObject",
            "solution_id": "turnkeydefault",
            "type": "",
            "version": 0,
            "health": 0,
            "node_id": "",
            "cardinality": 1,
            "rules_applied": [

            ],
            "elements": [

            ],
            "ts": "2020-07-15T16:26:16.113463",
            "confidence_dict": {
                "insight_id": "97dc34d6-03a3-49dc-9fe5-15789bff9bff"
            },
            "validation": [

            ],
            "children": [
                {
                    "id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                    "generation_id": 384,
                    "insight_id": "7dffc3af-8642-40f2-8fcc-b0f241984b48",
                    "node_type": "entity",
                    "confidence": 0.28782222222222226,
                    "count": 9,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "patient_details",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.113555",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "4f1c78f6-9a89-4a76-8c54-136d4122db39"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "77fd1289-3ebf-40c6-85f7-6dfc2002a89b",
                            "generation_id": 856,
                            "insight_id": "b6279f18-38ae-40fc-bc20-be8fdbeced6c",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "provider_id",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "8e4ee9cd-6d03-468c-93b2-32d560bf7680",
                            "value": "EX5009",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "086b4601-8e20-49f9-9365-c5a8dc154251",
                            "generation_id": 252,
                            "insight_id": "53d0c750-cc6c-4141-9c0b-fb25c355d345",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "vendor",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "3ebb9e96-28c3-499d-aba7-853e5b00b56f",
                            "value": "423009",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "39582615-75a7-42c7-8d84-2c1cee793e36",
                            "generation_id": 707,
                            "insight_id": "1e66e271-631c-47d9-b424-76bbc35770e4",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "patient_name",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "55c634e4-acc7-43ab-9483-0ceece33392c",
                            "value": "John Larry",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "93e2fa75-5ef6-41aa-85c5-2c09f8987461",
                            "generation_id": 410,
                            "insight_id": "7a07795b-bbcb-4ca5-9cfe-d91081c41102",
                            "node_type": "attribute",
                            "confidence": 0.3264,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "ssn",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "ce0bd2a7-2d15-4436-90d3-1532ec9ac77c",
                            "value": "4893X50",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "37ea7ce1-594e-4d9b-900c-b10c25f6da5d",
                            "generation_id": 103,
                            "insight_id": "8dc46c7e-3e44-4f9c-a86c-1e3c3840320e",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "gender",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "893f07ea-aeec-4864-85fc-604e612b1b9f",
                            "value": "Male",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "19010b8b-b2c8-4988-bb42-96dc8aa7fe9d",
                            "generation_id": 699,
                            "insight_id": "962f11c8-d2a6-4731-ab53-3365d8172a5c",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "phone",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "819a336b-bed4-4110-a51f-4447665af548",
                            "value": "786564322",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "523beeaa-a799-4585-8b1f-d66cba152ad2",
                            "generation_id": 757,
                            "insight_id": "36f0bf50-8f47-4938-9247-9f46f6d07491",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "dateofbirth",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "017804cc-82fc-47cd-b5f7-96e4b04b0471",
                            "value": "05.11.1965",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "50a26557-c0b3-4e40-8d2a-68c0737dfb56",
                            "generation_id": 825,
                            "insight_id": "d9b48479-ce96-47f8-9aa9-e7c667470974",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "member_id",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "838798e6-97bc-448f-b888-5ce97da2c5a0",
                            "value": "56432",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "ca1884df-f945-4738-be73-dcd232a319a7",
                            "generation_id": 174,
                            "insight_id": "288a95f4-7793-4577-a9a9-e80995011fee",
                            "node_type": "attribute",
                            "confidence": 0.33599999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "year",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "d4e41520-5bff-4744-82c5-60f16336ae0c",
                            "value": "2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                    "generation_id": 809,
                    "insight_id": "8c132ad9-379a-4fe5-96f8-a0308647f073",
                    "node_type": "entity",
                    "confidence": 0.2778181818181818,
                    "count": 11,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "admission",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.114287",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "bb278cb6-42ef-42cf-aa5d-bfc676edb011"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "1e27fb24-e578-4bc0-b84a-ee1a255e2c25",
                            "generation_id": 368,
                            "insight_id": "3a945872-a385-49b4-a58d-76e34a79837f",
                            "node_type": "attribute",
                            "confidence": 0.33599999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "year",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "d4e41520-5bff-4744-82c5-60f16336ae0c",
                            "value": "2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "3765fea1-6bc4-4889-8456-9439de595bd6",
                            "generation_id": 348,
                            "insight_id": "6c484bd7-2fda-4851-ad04-148778316688",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "date_of_admit",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "814f3f34-0e86-4f20-9315-6a783eef2621",
                            "value": "05.04.2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "402ae701-3dd6-4b54-b319-dfa224ad2ccf",
                            "generation_id": 707,
                            "insight_id": "e9348ece-fa74-4902-93b8-48cdc98b4541",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "procedure_group",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "a7b62b2b-ec8c-4ef0-90d0-df314246c650",
                            "value": "Med",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "3c888be6-81e5-4568-9a76-2a57998c49a5",
                            "generation_id": 369,
                            "insight_id": "46f0cf85-f63f-4050-a899-fdab96f4bb90",
                            "node_type": "attribute",
                            "confidence": 0.264,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "date_of_discharge",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "3672e067-5826-4c7d-9d5e-8019af5ee920",
                            "value": "05.06.2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.33
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "f9772381-8ebd-49e8-906a-0dc2cbc94f69",
                            "generation_id": 149,
                            "insight_id": "e23300c9-f605-4f4b-90ab-f3b526d59ca7",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "suplos",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "8ea1a8e0-8a28-4138-be60-6e54ca740564",
                            "value": "0",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "7c6c3bf1-b136-45c1-9f3f-da060e1fa8ee",
                            "generation_id": 216,
                            "insight_id": "66b6b2e4-d52c-45de-a8ff-0cff2992c855",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "speciality",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "18cd44e7-3656-43cd-9704-d6769f497ceb",
                            "value": "Emergency",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "14deb5ea-6312-4eec-a028-59a6cff65849",
                            "generation_id": 470,
                            "insight_id": "72daa011-3838-495b-8b57-55f877983155",
                            "node_type": "attribute",
                            "confidence": 0.264,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "lengthofstay",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "b632d5cc-bc3f-4466-9ca5-837bd68ffd4c",
                            "value": "3 days",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.33
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "6691cbf1-8671-46d7-a6a9-d674bac017d5",
                            "generation_id": 995,
                            "insight_id": "537b13f2-72eb-45ad-8a99-cdfb6a0f322b",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "placesvc",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "8e925bd6-6d93-4c5c-89b9-214ae4b36572",
                            "value": "Urgent Care",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "381b33d9-1f3f-47e5-8929-12da906898ca",
                            "generation_id": 911,
                            "insight_id": "3b60f5d1-491a-4f02-9d39-774a497a8df6",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "pcp",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "bbac4b24-cfd6-4190-bfb4-4a81b7ce3a72",
                            "value": "27564",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "06c2f7c1-b2d4-43d3-b878-a42377541759",
                            "generation_id": 983,
                            "insight_id": "8907099f-afdf-4ce8-9a06-44eedcd751a0",
                            "node_type": "attribute",
                            "confidence": 0.256,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "primary_condition_group",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "5140280e-0db6-4f9d-b8e3-b2f1775e14fc",
                            "value": "NFEC4",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.32
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "bb934512-fbf8-4417-b652-ae8295e4462e",
                            "generation_id": 984,
                            "insight_id": "a8ab6e1c-4e2f-47d4-ab53-74796bd11a61",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "charlsonindex",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "e09eb3de-d349-48c0-a817-3ac81beaab40",
                            "value": "1-2",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "66a170d8-44a1-4835-bc28-ef331b1ed5d8",
                    "generation_id": 317,
                    "insight_id": "4da136f6-cd0b-4cf3-8192-885def3090af",
                    "node_type": "entity",
                    "confidence": 0.25,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "payment_information",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115138",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "379d5a36-0c65-4cb1-82a9-dfbca9b380a4"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "0217e626-b043-4b9c-8754-a0828cb28184",
                            "generation_id": 554,
                            "insight_id": "05bd8331-1807-42ec-acdf-688799cd3e83",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "insurername",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "b50eb56d-0f9f-479f-8270-dcb4d3943323",
                            "value": "Amount Paid (USD)  Co-Payment (USD)  Settlement days",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.3
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "66a170d8-44a1-4835-bc28-ef331b1ed5d8",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                    "generation_id": 897,
                    "insight_id": "6198a2aa-861f-4f9e-9de9-86e57c0219e2",
                    "node_type": "entity",
                    "confidence": 0.6986666666666667,
                    "count": 3,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "payment_information",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115236",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "cb0b66d2-e0b5-415f-b9a5-23dda763e024"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "54277621-c4d9-413d-ba9a-af0380885983",
                            "generation_id": 684,
                            "insight_id": "35a3c25c-241a-4c24-8e23-598563992658",
                            "node_type": "attribute",
                            "confidence": 0.816,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "insurername",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "0087aa25-1eff-4b41-af13-b5bf2c7e6830",
                            "value": "cigana health",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.96,
                                "cell_confidence",
                                0.85
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                            "solution_id": "turnkeydefault",
                            "status": "accepted"
                        },
                        {
                            "id": "bb2d1581-1272-4cc9-933a-3956baab03a2",
                            "generation_id": 709,
                            "insight_id": "ad825cbe-f1dd-4135-8fb1-06744fff890e",
                            "node_type": "attribute",
                            "confidence": 0.6080000000000001,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "amountpaid_usd",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "1b8ba550-221f-4db1-b67b-d8c61dc55307",
                            "value": "430",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.76
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "b0f57310-b3a8-4567-82b9-5997743e9a61",
                            "generation_id": 604,
                            "insight_id": "2a800561-05d7-4e74-a662-a39c9d7a7d31",
                            "node_type": "attribute",
                            "confidence": 0.672,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "copayment_usd_123",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "44ed360e-b920-42db-a4b6-fb6ab6020975",
                            "value": "50",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.84
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                    "generation_id": 130,
                    "insight_id": "f1a72174-3a88-471f-b028-4c819043b963",
                    "node_type": "entity",
                    "confidence": 0.7216,
                    "count": 3,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "payment_information",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115446",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "f1e569ec-1041-4b47-8995-39827fd96c52"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "10480b56-7a40-46c1-a787-36c2ddf4c891",
                            "generation_id": 417,
                            "insight_id": "ecd7eeac-25b9-4f02-8094-3026f97cda93",
                            "node_type": "attribute",
                            "confidence": 0.7968000000000001,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "insurername",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "9c2a374c-6865-449d-813e-8f66d832f9da",
                            "value": "cvs",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.96,
                                "cell_confidence",
                                0.8300000000000001
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "fa0a383c-bbba-40b3-8743-02e40012c728",
                            "generation_id": 306,
                            "insight_id": "cd703489-d683-4d9b-bfce-9f3acfb86f6d",
                            "node_type": "attribute",
                            "confidence": 0.6880000000000001,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "amountpaid_usd",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "3a710bd8-d516-4dcd-9480-dea10fce0fd9",
                            "value": "230",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.86
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "349d3ae0-573e-42fc-8b21-178ad9b78ee2",
                            "generation_id": 725,
                            "insight_id": "0e11a66f-7e9b-48a1-b68a-267910bc1383",
                            "node_type": "attribute",
                            "confidence": 0.68,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "copayment_usd_123",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "5586a5e2-2797-40b4-8822-32c7e7334bbb",
                            "value": "30",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.85
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                    "generation_id": 979,
                    "insight_id": "db120b74-236a-4b7e-8c65-cb7ade421a8a",
                    "node_type": "entity",
                    "confidence": 0.25,
                    "count": 4,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "customer_experience",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115655",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "886e4f2c-97f8-4c50-aa68-8b3dad61f323"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "1463c95b-db1c-4c79-be0d-97b34807f058",
                            "generation_id": 630,
                            "insight_id": "1e36b12f-2638-46e8-8dec-b9efb994eb6d",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "eligibleroomtype",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "12f8db29-56bc-4d62-a408-24541504ac4e",
                            "value": "1S",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "e70631b6-0abd-4878-ac13-ad94ac9c99b7",
                            "generation_id": 406,
                            "insight_id": "743a2e3c-3b12-46b0-8fad-e169a05c0d3d",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "availedroomtype",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "d17b9895-b3e5-4354-bfe0-736465e01b52",
                            "value": "1S  L.William",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.32
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "ce049c42-239e-4f47-8a4d-e2d92c8356fb",
                            "generation_id": 863,
                            "insight_id": "1eb9c744-4998-4450-b867-38fd72fe4ce6",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "treatmentstatus",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "4f0e9f59-e0e9-4a9e-8373-a4e01b96cec4",
                            "value": "Successful  Todrics Mine",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "ts": "2020-07-15T16:26:19.689044",
                    "solution_id": "turnkeydefault",
                    "id": "2308f66b-5d37-462f-b234-d8a927e88a4e",
                    "name": "revisit_recommendation",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "confidence": 0,
                    "count": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "insight_id": "de6c177b-57ad-4a21-a112-98071ca6ae5e",
                    "confidence_dict": {
                        "insight_id": "760a0a5d-5165-4a65-9fe2-8a99b11c1d60"
                    },
                    "validation": [

                    ],
                    "generation_id": 274,
                    "node_type": "entity",
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "children": [
                        {
                            "name": "model_name",
                            "element_id": "11111111",
                            "value": "Model Class XGB Binary",
                            "confidence": 1,
                            "justification": "",
                            "reason": "",
                            "source": "faas",
                            "rules_applied": [

                            ],
                            "parent_id": "2308f66b-5d37-462f-b234-d8a927e88a4e",
                            "id": "4ce526ca-f504-42a1-88bf-32f4f71c3771",
                            "generation_id": 209,
                            "insight_id": "047b616d-1e00-41fa-9d74-5f322a42fd19",
                            "node_type": "attribute",
                            "count": 1,
                            "is_best": True,
                            "is_deleted": False,
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "solution_id": "turnkeydefault"
                        },
                        {
                            "name": "recommendation",
                            "element_id": "11111111",
                            "value": "Patient will visit",
                            "confidence": 0.537891686,
                            "justification": "",
                            "reason": "",
                            "source": "faas",
                            "rules_applied": [

                            ],
                            "parent_id": "2308f66b-5d37-462f-b234-d8a927e88a4e",
                            "id": "8ec9d45a-a784-4c7c-b4eb-b6d13fbb9640",
                            "generation_id": 378,
                            "insight_id": "de14a166-c9d5-4455-b1e0-8910890900b0",
                            "node_type": "attribute",
                            "count": 1,
                            "is_best": True,
                            "is_deleted": False,
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "solution_id": "turnkeydefault"
                        }
                    ]
                }
            ]
        }
    ]
}
comp_obj = {
    "comparator_type": "domain",
    "comparator_scope": {
        "domain_mapping": "patient",
        "entity_attributes": [
            {
                "entity": "patient_details",
                "attributes": [
                    "provider_id",
                    "patient_name"
                ]
            },
            {
                "entity": "payment_information",
                "attributes": [
                    "insurername",
                    "amountpaid_usd"
                ]
            }
        ]
    }
}
gt = {
    "id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "generation_id": 656,
    "insight_id": "69cd3239-22dc-4509-8ea9-9fc20b5b9285",
    "node_type": "document",
    "confidence": 0,
    "count": 1,
    "is_best": True,
    "justification": "",
    "is_deleted": False,
    "name": "document",
    "regions": [

    ],
    "_XpmsObjectMixin__type": "DocumentDomain",
    "ts": "2020-07-15T16:26:16.113359",
    "solution_id": "turnkeydefault",
    "type": "",
    "version": 0,
    "health": 1.0,
    "node_id": "",
    "cardinality": 1,
    "rules_applied": [

    ],
    "elements": [

    ],
    "confidence_dict": {
        "domain": 0.3551,
        "count": 33,
        "groups": {
            "patient_details": 0.2839,
            "admission": 0.2789,
            "payment_information": 0.5568,
            "customer_experience": 0.25,
            "patient": 0.3551,
            "insight_id": "52e585b3-c2f8-4c31-8940-8bc9366a2906"
        },
        "insight_id": "db0df17b-a914-4a88-85db-a69f36f0e750"
    },
    "validation": [

    ],
    "root_id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "doc_id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "parent_id": "6a5fe519-35e3-47ab-a361-7ae1094d94e5",
    "_name": "document_6a5_0",
    "review_counts": {
        "accepted": 1,
        "needs_review": 32,
        "corrected": 0,
        "insight_id": "e3886db6-efc7-426d-8b3b-83f5ad87d880"
    },
    "confidence_model_enabled": False,
    "children": [
        {
            "id": "fdb961a7-cce7-4303-a91e-415be6c2b760",
            "generation_id": 828,
            "insight_id": "c1478956-b877-42ab-8116-59007be454d3",
            "node_type": "domain",
            "confidence": 0.35506666666666664,
            "count": 33,
            "is_best": True,
            "justification": "",
            "is_deleted": False,
            "name": "patient",
            "regions": [

            ],
            "_XpmsObjectMixin__type": "DomainObject",
            "solution_id": "turnkeydefault",
            "type": "",
            "version": 0,
            "health": 0,
            "node_id": "",
            "cardinality": 1,
            "rules_applied": [

            ],
            "elements": [

            ],
            "ts": "2020-07-15T16:26:16.113463",
            "confidence_dict": {
                "insight_id": "97dc34d6-03a3-49dc-9fe5-15789bff9bff"
            },
            "validation": [

            ],
            "children": [
                {
                    "id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                    "generation_id": 384,
                    "insight_id": "7dffc3af-8642-40f2-8fcc-b0f241984b48",
                    "node_type": "entity",
                    "confidence": 0.28782222222222226,
                    "count": 9,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "patient_details",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.113555",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "4f1c78f6-9a89-4a76-8c54-136d4122db39"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "77fd1289-3ebf-40c6-85f7-6dfc2002a89b",
                            "generation_id": 856,
                            "insight_id": "b6279f18-38ae-40fc-bc20-be8fdbeced6c",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "provider_id",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "8e4ee9cd-6d03-468c-93b2-32d560bf7680",
                            "value": "EX5009",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "086b4601-8e20-49f9-9365-c5a8dc154251",
                            "generation_id": 252,
                            "insight_id": "53d0c750-cc6c-4141-9c0b-fb25c355d345",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "vendor",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "3ebb9e96-28c3-499d-aba7-853e5b00b56f",
                            "value": "423009",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "39582615-75a7-42c7-8d84-2c1cee793e36",
                            "generation_id": 707,
                            "insight_id": "1e66e271-631c-47d9-b424-76bbc35770e4",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "patient_name",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "55c634e4-acc7-43ab-9483-0ceece33392c",
                            "value": "John Larry",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "93e2fa75-5ef6-41aa-85c5-2c09f8987461",
                            "generation_id": 410,
                            "insight_id": "7a07795b-bbcb-4ca5-9cfe-d91081c41102",
                            "node_type": "attribute",
                            "confidence": 0.3264,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "ssn",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "ce0bd2a7-2d15-4436-90d3-1532ec9ac77c",
                            "value": "4893X50",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "37ea7ce1-594e-4d9b-900c-b10c25f6da5d",
                            "generation_id": 103,
                            "insight_id": "8dc46c7e-3e44-4f9c-a86c-1e3c3840320e",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "gender",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "893f07ea-aeec-4864-85fc-604e612b1b9f",
                            "value": "Male",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "19010b8b-b2c8-4988-bb42-96dc8aa7fe9d",
                            "generation_id": 699,
                            "insight_id": "962f11c8-d2a6-4731-ab53-3365d8172a5c",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "phone",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "819a336b-bed4-4110-a51f-4447665af548",
                            "value": "786564322",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "523beeaa-a799-4585-8b1f-d66cba152ad2",
                            "generation_id": 757,
                            "insight_id": "36f0bf50-8f47-4938-9247-9f46f6d07491",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "dateofbirth",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "017804cc-82fc-47cd-b5f7-96e4b04b0471",
                            "value": "05.11.1965",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "50a26557-c0b3-4e40-8d2a-68c0737dfb56",
                            "generation_id": 825,
                            "insight_id": "d9b48479-ce96-47f8-9aa9-e7c667470974",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "member_id",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "838798e6-97bc-448f-b888-5ce97da2c5a0",
                            "value": "56432",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "ca1884df-f945-4738-be73-dcd232a319a7",
                            "generation_id": 174,
                            "insight_id": "288a95f4-7793-4577-a9a9-e80995011fee",
                            "node_type": "attribute",
                            "confidence": 0.33599999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "year",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "d4e41520-5bff-4744-82c5-60f16336ae0c",
                            "value": "2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "500c66fc-8e77-4606-99a9-9a92dfccc0dd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                    "generation_id": 809,
                    "insight_id": "8c132ad9-379a-4fe5-96f8-a0308647f073",
                    "node_type": "entity",
                    "confidence": 0.2778181818181818,
                    "count": 11,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "admission",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.114287",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "bb278cb6-42ef-42cf-aa5d-bfc676edb011"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "1e27fb24-e578-4bc0-b84a-ee1a255e2c25",
                            "generation_id": 368,
                            "insight_id": "3a945872-a385-49b4-a58d-76e34a79837f",
                            "node_type": "attribute",
                            "confidence": 0.33599999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "year",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "d4e41520-5bff-4744-82c5-60f16336ae0c",
                            "value": "2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "3765fea1-6bc4-4889-8456-9439de595bd6",
                            "generation_id": 348,
                            "insight_id": "6c484bd7-2fda-4851-ad04-148778316688",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "date_of_admit",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "814f3f34-0e86-4f20-9315-6a783eef2621",
                            "value": "05.04.2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "402ae701-3dd6-4b54-b319-dfa224ad2ccf",
                            "generation_id": 707,
                            "insight_id": "e9348ece-fa74-4902-93b8-48cdc98b4541",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "procedure_group",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "a7b62b2b-ec8c-4ef0-90d0-df314246c650",
                            "value": "Med",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "3c888be6-81e5-4568-9a76-2a57998c49a5",
                            "generation_id": 369,
                            "insight_id": "46f0cf85-f63f-4050-a899-fdab96f4bb90",
                            "node_type": "attribute",
                            "confidence": 0.264,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "date_of_discharge",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "3672e067-5826-4c7d-9d5e-8019af5ee920",
                            "value": "05.06.2018",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.33
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "f9772381-8ebd-49e8-906a-0dc2cbc94f69",
                            "generation_id": 149,
                            "insight_id": "e23300c9-f605-4f4b-90ab-f3b526d59ca7",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "suplos",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "8ea1a8e0-8a28-4138-be60-6e54ca740564",
                            "value": "0",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "7c6c3bf1-b136-45c1-9f3f-da060e1fa8ee",
                            "generation_id": 216,
                            "insight_id": "66b6b2e4-d52c-45de-a8ff-0cff2992c855",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "speciality",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "18cd44e7-3656-43cd-9704-d6769f497ceb",
                            "value": "Emergency",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "14deb5ea-6312-4eec-a028-59a6cff65849",
                            "generation_id": 470,
                            "insight_id": "72daa011-3838-495b-8b57-55f877983155",
                            "node_type": "attribute",
                            "confidence": 0.264,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "lengthofstay",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "b632d5cc-bc3f-4466-9ca5-837bd68ffd4c",
                            "value": "3 days",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.33
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "6691cbf1-8671-46d7-a6a9-d674bac017d5",
                            "generation_id": 995,
                            "insight_id": "537b13f2-72eb-45ad-8a99-cdfb6a0f322b",
                            "node_type": "attribute",
                            "confidence": 0.27999999999999997,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "placesvc",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "8e925bd6-6d93-4c5c-89b9-214ae4b36572",
                            "value": "Urgent Care",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "381b33d9-1f3f-47e5-8929-12da906898ca",
                            "generation_id": 911,
                            "insight_id": "3b60f5d1-491a-4f02-9d39-774a497a8df6",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "pcp",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "bbac4b24-cfd6-4190-bfb4-4a81b7ce3a72",
                            "value": "27564",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "06c2f7c1-b2d4-43d3-b878-a42377541759",
                            "generation_id": 983,
                            "insight_id": "8907099f-afdf-4ce8-9a06-44eedcd751a0",
                            "node_type": "attribute",
                            "confidence": 0.256,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "primary_condition_group",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "5140280e-0db6-4f9d-b8e3-b2f1775e14fc",
                            "value": "NFEC4",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.32
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "bb934512-fbf8-4417-b652-ae8295e4462e",
                            "generation_id": 984,
                            "insight_id": "a8ab6e1c-4e2f-47d4-ab53-74796bd11a61",
                            "node_type": "attribute",
                            "confidence": 0.272,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "charlsonindex",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "e09eb3de-d349-48c0-a817-3ac81beaab40",
                            "value": "1-2",
                            "reason": "",
                            "source": [
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ef7afe04-139d-426a-a3b0-118f7e4cb965",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "66a170d8-44a1-4835-bc28-ef331b1ed5d8",
                    "generation_id": 317,
                    "insight_id": "4da136f6-cd0b-4cf3-8192-885def3090af",
                    "node_type": "entity",
                    "confidence": 0.25,
                    "count": 1,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "payment_information",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115138",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "379d5a36-0c65-4cb1-82a9-dfbca9b380a4"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "0217e626-b043-4b9c-8754-a0828cb28184",
                            "generation_id": 554,
                            "insight_id": "05bd8331-1807-42ec-acdf-688799cd3e83",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "insurername",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "b50eb56d-0f9f-479f-8270-dcb4d3943323",
                            "value": "Amount Paid (USD)  Co-Payment (USD)  Settlement days",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.96,
                                "element_confidence",
                                0.3
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "66a170d8-44a1-4835-bc28-ef331b1ed5d8",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                    "generation_id": 897,
                    "insight_id": "6198a2aa-861f-4f9e-9de9-86e57c0219e2",
                    "node_type": "entity",
                    "confidence": 0.6986666666666667,
                    "count": 3,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "payment_information",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115236",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "cb0b66d2-e0b5-415f-b9a5-23dda763e024"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "54277621-c4d9-413d-ba9a-af0380885983",
                            "generation_id": 684,
                            "insight_id": "35a3c25c-241a-4c24-8e23-598563992658",
                            "node_type": "attribute",
                            "confidence": 0.816,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "insurername",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "0087aa25-1eff-4b41-af13-b5bf2c7e6830",
                            "value": "cigana health",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.96,
                                "cell_confidence",
                                0.85
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                            "solution_id": "turnkeydefault",
                            "status": "accepted"
                        },
                        {
                            "id": "bb2d1581-1272-4cc9-933a-3956baab03a2",
                            "generation_id": 709,
                            "insight_id": "ad825cbe-f1dd-4135-8fb1-06744fff890e",
                            "node_type": "attribute",
                            "confidence": 0.6080000000000001,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "amountpaid_usd",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "1b8ba550-221f-4db1-b67b-d8c61dc55307",
                            "value": "431",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.76
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "b0f57310-b3a8-4567-82b9-5997743e9a61",
                            "generation_id": 604,
                            "insight_id": "2a800561-05d7-4e74-a662-a39c9d7a7d31",
                            "node_type": "attribute",
                            "confidence": 0.672,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "copayment_usd_123",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "44ed360e-b920-42db-a4b6-fb6ab6020975",
                            "value": "50",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.84
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "9e95e27a-144e-4859-894a-e8773bcb7131",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                    "generation_id": 130,
                    "insight_id": "f1a72174-3a88-471f-b028-4c819043b963",
                    "node_type": "entity",
                    "confidence": 0.7216,
                    "count": 3,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "payment_information",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115446",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "f1e569ec-1041-4b47-8995-39827fd96c52"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "10480b56-7a40-46c1-a787-36c2ddf4c891",
                            "generation_id": 417,
                            "insight_id": "ecd7eeac-25b9-4f02-8094-3026f97cda93",
                            "node_type": "attribute",
                            "confidence": 0.7968000000000001,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "insurername",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "9c2a374c-6865-449d-813e-8f66d832f9da",
                            "value": "cvs",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.96,
                                "cell_confidence",
                                0.8300000000000001
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "fa0a383c-bbba-40b3-8743-02e40012c728",
                            "generation_id": 306,
                            "insight_id": "cd703489-d683-4d9b-bfce-9f3acfb86f6d",
                            "node_type": "attribute",
                            "confidence": 0.6880000000000001,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "amountpaid_usd",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "3a710bd8-d516-4dcd-9480-dea10fce0fd9",
                            "value": "231",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.86
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "349d3ae0-573e-42fc-8b21-178ad9b78ee2",
                            "generation_id": 725,
                            "insight_id": "0e11a66f-7e9b-48a1-b68a-267910bc1383",
                            "node_type": "attribute",
                            "confidence": 0.68,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "copayment_usd_123",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "5586a5e2-2797-40b4-8822-32c7e7334bbb",
                            "value": "30",
                            "reason": "",
                            "source": [
                                "table_header",
                                0.8,
                                "cell_confidence",
                                0.85
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "ec5ee513-523f-4871-8c89-4d011430f5cd",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                    "generation_id": 979,
                    "insight_id": "db120b74-236a-4b7e-8c65-cb7ade421a8a",
                    "node_type": "entity",
                    "confidence": 0.25,
                    "count": 4,
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "name": "customer_experience",
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "ts": "2020-07-15T16:26:16.115655",
                    "solution_id": "turnkeydefault",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "confidence_dict": {
                        "insight_id": "886e4f2c-97f8-4c50-aa68-8b3dad61f323"
                    },
                    "validation": [

                    ],
                    "children": [
                        {
                            "id": "1463c95b-db1c-4c79-be0d-97b34807f058",
                            "generation_id": 630,
                            "insight_id": "1e36b12f-2638-46e8-8dec-b9efb994eb6d",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "eligibleroomtype",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "12f8db29-56bc-4d62-a408-24541504ac4e",
                            "value": "1S",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.34
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "e70631b6-0abd-4878-ac13-ad94ac9c99b7",
                            "generation_id": 406,
                            "insight_id": "743a2e3c-3b12-46b0-8fad-e169a05c0d3d",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "availedroomtype",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "d17b9895-b3e5-4354-bfe0-736465e01b52",
                            "value": "1S  L.William",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.32
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        },
                        {
                            "id": "ce049c42-239e-4f47-8a4d-e2d92c8356fb",
                            "generation_id": 863,
                            "insight_id": "1eb9c744-4998-4450-b867-38fd72fe4ce6",
                            "node_type": "attribute",
                            "confidence": 0.25,
                            "count": 1,
                            "is_best": True,
                            "justification": "",
                            "is_deleted": False,
                            "name": "treatmentstatus",
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "element_id": "4f0e9f59-e0e9-4a9e-8373-a4e01b96cec4",
                            "value": "Successful  Todrics Mine",
                            "reason": "",
                            "source": [
                                "heading",
                                "0.59",
                                "synonym",
                                0.8,
                                "element_confidence",
                                0.35
                            ],
                            "rules_applied": [

                            ],
                            "parent_id": "6cc2cac8-d1eb-42f6-bc65-7e2d92091212",
                            "solution_id": "turnkeydefault",
                            "status": "review_required"
                        }
                    ]
                },
                {
                    "ts": "2020-07-15T16:26:19.689044",
                    "solution_id": "turnkeydefault",
                    "id": "2308f66b-5d37-462f-b234-d8a927e88a4e",
                    "name": "revisit_recommendation",
                    "type": "",
                    "version": 0,
                    "health": 0,
                    "node_id": "",
                    "cardinality": 1,
                    "confidence": 0,
                    "count": 1,
                    "rules_applied": [

                    ],
                    "elements": [

                    ],
                    "insight_id": "de6c177b-57ad-4a21-a112-98071ca6ae5e",
                    "confidence_dict": {
                        "insight_id": "760a0a5d-5165-4a65-9fe2-8a99b11c1d60"
                    },
                    "validation": [

                    ],
                    "generation_id": 274,
                    "node_type": "entity",
                    "is_best": True,
                    "justification": "",
                    "is_deleted": False,
                    "regions": [

                    ],
                    "_XpmsObjectMixin__type": "Entity",
                    "children": [
                        {
                            "name": "model_name",
                            "element_id": "11111111",
                            "value": "Model Class XGB Binary",
                            "confidence": 1,
                            "justification": "",
                            "reason": "",
                            "source": "faas",
                            "rules_applied": [

                            ],
                            "parent_id": "2308f66b-5d37-462f-b234-d8a927e88a4e",
                            "id": "4ce526ca-f504-42a1-88bf-32f4f71c3771",
                            "generation_id": 209,
                            "insight_id": "047b616d-1e00-41fa-9d74-5f322a42fd19",
                            "node_type": "attribute",
                            "count": 1,
                            "is_best": True,
                            "is_deleted": False,
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "solution_id": "turnkeydefault"
                        },
                        {
                            "name": "recommendation",
                            "element_id": "11111111",
                            "value": "Patient will visit",
                            "confidence": 0.537891686,
                            "justification": "",
                            "reason": "",
                            "source": "faas",
                            "rules_applied": [

                            ],
                            "parent_id": "2308f66b-5d37-462f-b234-d8a927e88a4e",
                            "id": "8ec9d45a-a784-4c7c-b4eb-b6d13fbb9640",
                            "generation_id": 378,
                            "insight_id": "de14a166-c9d5-4455-b1e0-8910890900b0",
                            "node_type": "attribute",
                            "count": 1,
                            "is_best": True,
                            "is_deleted": False,
                            "regions": [

                            ],
                            "_XpmsObjectMixin__type": "Attribute",
                            "solution_id": "turnkeydefault"
                        }
                    ]
                }
            ]
        }
    ]
}


def get_all_groups(data_objects, all_entity_groups):
    for data_object in data_objects:
        if data_object["node_type"] == "domain":
            if data_object["children"]:
                get_all_groups(data_object["children"], all_entity_groups)
        elif data_object["node_type"] == "entity":
            entity_name = data_object["name"]
            groups = []
            for attribute in data_object["children"]:
                if attribute["node_type"] == "entity":
                    get_all_groups([attribute], all_entity_groups)
                else:
                    groups.append(attribute)
            if groups:
                if entity_name not in all_entity_groups:
                    all_entity_groups[entity_name] = [groups]
                else:
                    all_entity_groups[entity_name].extend([groups])


def compare_domain(group_index, doc_data, entity, att_name, att_value):
    if entity in doc_data:
        for att in doc_data[entity][group_index]:
            if att["name"] == att_name:
                if att["value"] == att_value:
                    return "matched"
    return "unmatched"


all_doc_entity_groups = {}
all_gt_entity_groups = {}

get_all_groups(inp["children"], all_doc_entity_groups)
get_all_groups(gt["children"], all_gt_entity_groups)

comp_list = []
for ent_att in comp_obj["comparator_scope"]["entity_attributes"]:
    entity_name = ent_att["entity"]
    for attr in ent_att["attributes"]:
        comp_dict = {"entity_name": entity_name, "att_name": attr}
        if entity_name in all_gt_entity_groups:
            group_index = -1
            for index, group in enumerate(all_gt_entity_groups[entity_name]):
                for att in group:
                    if att["name"] == attr:
                        comp_dict.update({"comparator_result": compare_domain(index, all_doc_entity_groups, entity_name,
                                                                              att["name"], att["value"])})
        comp_list.append(comp_dict)
print(comp_list)

