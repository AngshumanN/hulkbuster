from xpms_file_storage.file_handler import XpmsResource, LocalResource
import json


def get_doc_obj(input, doc_obj):
    if not doc_obj:
        if isinstance(input, list):
            get_doc_obj(input[0], doc_obj)
        if isinstance(input, dict):
            if "document" in input:
                doc_obj.append(input["document"][0])
            else:
                get_doc_obj(list(input.values())[0], doc_obj)


def get_file_metadata(doc):
    file_meta = doc["metadata"]["properties"]["file_metadata"]

    return file_meta
def get_format_gt(gt_path):
    ground_truth = {}
    minio_folder = XpmsResource.get(urn=gt_path)
    local_folder = LocalResource(key="/tmp/jsonsss")
    minio_folder.copy(local_folder)
    ground_truth = {}
    with open(local_folder.fullpath) as f:
        data = json.load(f)
    return data

da = get_format_gt("minio://dev1/movecomp/large_data/1b5e3ff4-71c0-4e58-be09-3b67363cb8b1/input_output.json")
json_data = json.loads(da["outputs"][0])["json_data"]
ref_id = ""
doc_obj = []
if not ref_id:
    if "task_input" in json_data:
        for k, v in json_data["task_input"].items():
            if isinstance(v, list):
                get_doc_obj(v, doc_obj)
        if doc_obj:
            doc_obj = doc_obj[0] if isinstance(doc_obj, list) else doc_obj
            meta = get_file_metadata(doc_obj)
            ref_id = meta["ref_id"]
print(ref_id)


https://web.microsoftstream.com/video/5d3d1255-9d7a-45ec-a50d-b9290935c7d6


#after ingest
ref_id:
"$.document.[0].doc_id"
e_d_i:
"$.document.[0].metadata.properties.file_metadata"
e_d_a:
"file_path,ref_id,ingest_ts,dag_execution_id,dag_instance_id"

#after classify
ref_id:
"$.document.[0].doc_id"
e_d_i:
"$.document.[0].metadata.template_info"
e_d_a:
"doc_class_name,domain_mapping"

#after extract json
ref_id:
"$.document.[0].doc_id"
d_d_i:
"$.document.[0].elements[*].children[*].children[*]"
d_d_a:
"node_type,confidence"

#after entity
ref_id:
"$.document.[0].doc_id"
d_d_i:
"$.domain.[0].children[*].children[*].children[*]"
d_d_a:
"name,confidence"



